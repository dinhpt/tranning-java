var checkModle = false;
$(document).ready(
		function() {
			var urlLoadMatchComing = "${f:url('loadTabMatch')}";
			var urlLoadMatchLive = "${f:url('loadTabMatch')}";
			// BTN HOST CLICK
			$('#modal-host').modal();
			$('#close-btn').click(function() {
				$('#modal-host').modal('close')
			})
			$('.btn-host').click(function() {
				// $('#modal-host').modal('open')
			})
			$(".button-collapse").sideNav();
			$('#icon-search').click(function() {
				$(this).hide();
				$('#warp-search #form-search').show();
			})
			$('.action-chat').click(
					function() {
						if ($('#box-chat').hasClass('active')) {
							$('#box-chat').removeClass('active')
							$('#warp-main-content').removeClass('m8')
									.removeClass('l8').addClass('m10')
									.addClass('l10');
							$('.action-chat i').addClass('fa-angle-left')
									.removeClass('fa-angle-right');
							$('#main-content, #tab-schedule').addClass(
									'container');

						} else {
							$('#box-chat').addClass('active');
							$('#warp-main-content').removeClass('m10')
									.removeClass('l10').addClass('m8')
									.addClass('l8');
							$('.action-chat i').addClass('fa-angle-right')
									.removeClass('fa-angle-left');
							$('#main-content, #tab-schedule').removeClass(
									'container');
						}
					});
			setHeightListLive();

			$("#list-other-video").mCustomScrollbar({
				scrollButtons : {
					enable : true
				},
				theme : "light-thick",
				scrollbarPosition : "outside"
			});
			/**
			 * Namlx Countdown stream
			 */
			$('[data-countdown]').each(function() {
				var $this = $(this), finalDate = $(this).data('countdown');
				$this.countdown(finalDate, function(event) {
					$this.html(event.strftime('%I:%M:%S'));
				}).on('finish.countdown', function(event) {
					$(this).html('Live');
					$(this).addClass("live");
				}).countdown('start');
			});

			$('.dropdown-button').dropdown({
				inDuration : 300,
				outDuration : 225,
				constrain_width : false, // Does not change width of dropdown
				// to that of the activator
				hover : false, // Activate on hover
				gutter : 0, // Spacing from edge
				belowOrigin : false, // Displays dropdown below the button
				alignment : 'left' // Displays dropdown with edge aligned to
			// the left of button
			});

			// init show moblie
			showMobile();
			$(window).resize(function() {
				showMobile();
			})

			$('.icon-menu').click(
					function() {
						if ($('#sidebar').hasClass('active')) {
							$('#sidebar').removeClass('active')
							$('#warp-main-content').removeClass('m8')
									.removeClass('l8');

						} else {
							$('#sidebar').addClass('active');
							$('#warp-main-content').removeClass('m8')
									.removeClass('l8');
						}
					});

			/* start search */
			setTabActive();
			searchInTab();
			/* end search */
		});

function setTabActive() {
	$('#wrap-tab ul li a').click(function() {
		// set tab active
		var tab_info = $(this).attr('id');
		localStorage.setItem('tab_active_info', tab_info);
	})
}

function searchInTab() {
	$('#search-math').keypress(function(event) {
		var key = event.which;
		if (key == 13) // the enter key code
		{
			triggerSearch();
			return false;
		}
	})
}

function triggerSearch() {
	var tab_info = localStorage.getItem('tab_active_info');
	if (tab_info != undefined && tab_info.length > 0) {
		$('#' + tab_info).trigger("click");
	}
}

function showMobile() {
	if (isMobile()) {
		$('#sidebar').removeClass('active');
		$('#box-chat').removeClass('active');
		$('.action-chat i').addClass('fa-angle-left').removeClass(
				'fa-angle-right');
		$('#warp-main-content').removeClass('m8').removeClass('l8');

		$('.chat-word').click(function() {
			if ($('#box-chat').hasClass('active')) {
				$('#box-chat').removeClass('active')
				$('#warp-main-content').removeClass('m8').removeClass('l8');

			} else {
				$('#box-chat').addClass('active');
				$('#warp-main-content').removeClass('m10').removeClass('l10');
			}
		})

	}
}
function isMobile() {
	var width = $(window).width();
	return (width < 1024);
}

function setHeightListLive() {
	var contentVideoHeight = $('#content-video').height();
	$('#list-other-video').height(contentVideoHeight)
}

function doHost(matchId) {
	checkModle = true;
	if (matchId.length == 0) {
		console.log('match id empty');
		return;
	}
	$(".empty-link").hide();
	$(".not-link").hide();
	$("#link").val('');
	$.ajax({
		type : "POST",
		url : urlDoHost,
		data : {
			"matchId" : matchId,
		},
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function(response) {
			$("#gametype").text(response.gameName);
			$("#match-name").text(response.matchName);
			$("#team-name1").text(response.team1);
			$("#team-name2").text(response.team2);
			$("#matchId").val(response.matchId);
			appendLinkStreams(response);
			$('#modal-host').modal('open');
		},
		error : function(e) {
			console.log(e)
		}
	});
}

function appendLinkStreams(response) {
	var divSuggest = $("#sussgest-link-stream");
	var size = response.streams.length;
	var html = "";
	if (size == 0) {

	} else {
		html += '<p class="host">Link video gợi ý của trận đấu</p>';
		for (i = 0; i < size; i++) {
			stream = response.streams[i];
			html += '<label>' + (i + 1) + '. [' + stream.lang + ']-'
					+ stream.url + '</label><br/>';
		}
	}
	$('#sussgest-link-stream').html(html);
}

function appendLinkStreamsHasRadio(response) {
	var divSuggest = $("#sussgest-link-stream");
	var size = response.streams.length;
	var html = "";
	if (size == 0) {
		html += '<p class="host">Chèn link video của trận đấu</p>';
		html += '<p class="host"><input style="width: 50%;" type="text" name="link" id="link" onblur="isLinkStream(this.value)" /></p><br/>';
	} else {
		html += '<p class="host">Chọn link video của trận đấu</p>';
		for (i = 0; i < size; i++) {
			stream = response.streams[i];
			html += '<label><input class="link-stream" type="radio" name="streamId" value="';
			html += stream.streamId;
			html += '">[' + stream.lang + ']-' + stream.url
					+ '</input></label><br/>';
		}
		html += '<label><input class="link-stream" type="radio" name="streamId" value="">Other:</input>';
		html += '<input style="width: 50%;" type="text" name="link" id="link" onblur="isLinkStream(this.value)" /></label>';
	}
	$('#sussgest-link-stream').html(html);
}

function doHosted() {
	checkModle = true;
}

/**
 * NamLX Ajax load Tab
 * 
 * @param url
 * @param loadOption
 */
function loadTabMatch(url, loadOption, divId) {
	console.log("START LOADING MATCH OPTION");
	console.log(loadOption);
	if (loadOption.length == 0) {
		console.log('loadOption id empty');
		return;
	}
	var searchMatch = $('#search-math').val();
	var selectedGame = $('#selectedGame').val();
	$.ajax({
		type : "POST",
		url : url,
		data : {
			"loadOption" : loadOption,
			"searchMatch" : searchMatch,
			"gameSelect" : selectedGame
		},
		beforeSend : function() {
			$('#load').show()
		},
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function(response) {
			$('#load').hide()
			generateMatchs(response, divId);
		},
		error : function(e) {
			console.log(e)
		}
	});
}
/**
 * NamLX
 * generate matchs in tab
 * @param response
 * @param divId
 */
function generateMatchs(response, divId) {
	console.log(response.lstComingMatchs);
	var html = "";
	var gameList;
	var matchDtos;
	var match;
	console.log(response.lstComingMatchs.length);
	var notHaveMatch = true;
	for (i = 0; i < response.lstComingMatchs.length; i++) {
		gameList = response.lstComingMatchs[i];

		if (gameList.matchDtos != null && gameList.matchDtos[0] != undefined) {
			notHaveMatch = false;
			html += '<div class="list-match">';
			html += '<h2 class="game-name bg-blue">';
			html += gameList.nameGame;
			html += '</h2>';
			html += '<table class="matchs ">';
			matchDtos = gameList.matchDtos;
			for (j = 0; j < matchDtos.length; j++) {
				if (matchDtos[j].status == 1)
					html += '<tr class="item-match" onclick="viewStreamLive('
							+ matchDtos[j].userHostedsId + ')">';
				else
					html += '<tr class="item-match" onclick="rctmatchToBetHome('
							+ matchDtos[j].matchId
							+ ','
							+ matchDtos[j].uhostedId + ')">';
				// Team 1
				html += '<td class="team1">';
				html += '<div class="live-left">';
				console.log(matchDtos[j].status);
				if (matchDtos[j].status == 1) {
					html += '<span class="live"> Live </span> <span class="viewer">';
					html += matchDtos[j].totalShowing;
					html += '</span>';
				} else {
					html += matchDtos[j].startTime;
				}
				html += '</div>';
				html += '<div class="team-info"><span class="team-name">';
				html += matchDtos[j].team1Name;
				html += '</span>';
				html += '<span class="team-logo">';
				html += '<img alt="team1" src="';
				html += matchDtos[j].team1Avatar;
				html += '"/>';
				html += '</span>';
				html += '<span class="percent">';
				html += matchDtos[j].team1BetPer;
				html += '%</span></div>';
				html += '</td>';
				// Team 2
				html += '<td class="team2">';
				html += '<div class="team-info-1"><span class="percent">';
				html += matchDtos[j].team2BetPer;
				html += '%</span>';
				html += '<span class="team-logo">';
				html += '<img alt="team1" src="';
				html += matchDtos[j].team2Avatar;
				html += '"/>';
				html += '</span>';
				html += '<span class="team-name">';
				html += matchDtos[j].team2Name;
				html += '</span></div>';
				html += '<span class="hosted">';
				html += ' Hosted by <strong>';
				html += ' <a href="#">';
				html += matchDtos[j].userHosted;
				html += '</a> </strong></span>';

				html += '</td>';
				html += '</tr>';
			}

			html += '</table> </div>';
		}

	}
	if (notHaveMatch) {
		html += "Không có trận đấu nào";
	}

	$('#' + divId).html(html);
}

function approveHost() {
	var matchId = $("#matchId").val();
	var link = $("#link").val();
	$.ajax({
		type : "POST",
		url : urlAppHost,
		data : {
			"matchId" : matchId,
			"link" : link,
		},
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function(response) {
			var result = response.result;
			var emptyLinkClass = $(".empty-link");
			var notLinkClass = $(".not-link");
			if (result == saveSuccess) {
				var confirm = alert("Khởi tạo stream thành công");
				$("#uhid" + matchId).val(response.uhostedId);
				$("#hosted" + matchId).show();
				$("#host" + matchId).hide();
				$('#modal-host').modal('close');
				return false;
			}
			if (result == hostOver) {
				var confirm = alert("Đã quá số host tối đa cho phép");
				$('#modal-host').modal('close');
				return false;
			}
			if (result == userHosted) {
				var confirm = alert("Bạn đã host trận thi đấu này");
				$('#modal-host').modal('close');
				return false;
			}
			if (result == emptyLink) {
				emptyLinkClass.show();
				notLinkClass.hide();
				return false;
			}
			if (result == notLink) {
				notLinkClass.show();
				emptyLinkClass.hide();
				return false;
			}
			if (result == saveFailed) {
				alert('save false');
				return false;
			}
		},
		error : function(e) {
			console.log(e)
		}
	});
}

function isLinkStream(link) {
	console.log(link);
	var emptyLink = $(".empty-link");
	var notLink = $(".not-link");
	if (link.length == 0) {
		emptyLink.show();
		notLink.hide();
		return;
	}
	emptyLink.hide();
	var res = link
			.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
	if (res == null) {
		notLink.show();
	} else {
		notLink.hide();
	}
}

function rctmatchToBet(matchId) {
	if (checkModle == true) {
		checkModle = false;
		return;
	}
	if (matchId.length == 0) {
		console.log('match id empty');
		return;
	}
	var uhostedId = $("#uhid" + matchId).val();
	if (uhostedId.length == 0) {
		console.log('uhostedId id empty');
		return;
	}
	var url = urlRctMatchToBet + matchId + '/uhid/' + uhostedId;
	window.location = url;
}

/**
 * Link to bet page
 * @param matchId
 * @param uhostedId
 */
function rctmatchToBetHome(matchId, uhostedId) {
	if (checkModle == true) {
		checkModle = false;
		return;
	}
	if (matchId.length == 0) {
		console.log('match id empty');
		return;
	}

	var url = urlRctMatchToBet + matchId + '/uhid/' + uhostedId;
	window.location = url;
}

function viewStreamLive(userHostedsId) {
	if (userHostedsId.length == 0) {
		console.log('user hosteds id empty');
		return;
	}
	var url = urlViewLiveStream + userHostedsId;
	window.location = url;
}
function doBet(val, teamId) {
	var subMatchId = getIdFromAtrIdTag(val, 'betsmid');
	var betMoney = $('#money' + teamId + subMatchId).val();
	if (subMatchId.length == 0 || teamId.length == 0 || matchId.length == 0)
		return;
	var url = urlDoBet;
	$.ajax({
		type : "POST",
		url : url,
		data : {
			"matchId" : matchId,
			"teamId" : teamId,
			"betMoney" : betMoney,
			"uhostedId" : uhostedId,
			"smatchId" : subMatchId,
			"team1Id" : team1Id,
			"team2Id" : team2Id,
		},
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function(response) {
			alert(response.message);
			var code = response.code;
			if(code == 'success'){
				if(response.firstBet){
					doRereshBetHtml(subMatchId, teamId);
					$('#money'+teamId+subMatchId).val(betMoney);
				}
				$('#totalbet'+subMatchId).text(response.totalBet);
				doRefreshData();
			}
		},
		error : function(e) {
			console.log(e)
		}
	});
}

function doBet1(val) {
	var teamId = team1Id;
	doBet(val, teamId);
}

function doBet2(val) {
	var teamId = team2Id;
	doBet(val, teamId);
}

function doRereshBetHtml(subMatchId, teamId){
	var div1 = $("#cdn" + team1Id + subMatchId);
	var div2 = $("#cdn" + team2Id + subMatchId);
	if(teamId == team1Id){
		appendHtmlTeam1(div1, div2, subMatchId);
	}else if(teamId == team2Id){
		appendHtmlTeam2(div1, div2, subMatchId);
	}
}

function appendHtmlTeam1(div1, div2, subMatchId){
	var html = '<div class="box-order clearfix">';
	html += 		'<input placeholder="Nhập số sao đặt" id="money'+team1Id+subMatchId+'" type="text" class="col s10"/>';
	html += 		'<button class="btn-red col s2" id="betsmid'+subMatchId+'" onclick="doBet1(this)">Đặt</button>';
	html +=    '</div>';
	html += '<p class="expect">Dự kiến thắng <strong>1.000.000</strong> sao</p>';
	html +=	'<p class="ordered">Bạn đã đặt: <strong id="totalbet'+subMatchId+'"></strong> sao</p>';
	div1.html(html);
	html = '<div class="box-order clearfix">';
	html += 	'<button class="btn-gray col s2">Đặt</button>';
	html += 	'<input class="col s10" readonly="readonly" placeholder="Không thể đặt tất cả 2 bên" type="text">';
	html += '</div>';
	html += '<button class="btn-change" id="btncb'+subMatchId+'" onclick="doChangeBet2(this)">Đổi kèo</button>';
	div2.html(html);
}

function appendHtmlTeam2(div1, div2, subMatchId){
	var html = '<div class="box-order clearfix">';
	html += 		'<button class="btn-red col s2" id="betsmid'+subMatchId+'" onclick="doBet2(this)">Đặt</button>';
	html += 		'<input placeholder="Nhập số sao đặt" id="money'+team2Id+subMatchId+'" type="text" class="col s10"/>';
	html +=    '</div>';
	html += '<p class="expect">Dự kiến thắng <strong>1.000.000</strong> sao</p>';
	html +=	'<p class="ordered">Bạn đã đặt: <strong id="totalbet'+subMatchId+'"></strong> sao</p>';
	div2.html(html);
	html =  '<div class="box-order clearfix">';
	html += 	'<input class="col s10" readonly="readonly" placeholder="Không thể đặt tất cả 2 bên" type="text">'
	html += 	'<button class="btn-gray col s2" >Đặt</button>';
	html += '</div>';
	html += '<button class="btn-change" id="btncb'+subMatchId+'" onclick="doChangeBet1(this)">Đổi kèo</button>';
	div1.html(html);
}

function doChangeBet(value, teamId){
	var smatchId = getIdFromAtrIdTag(value, "btncb");
	if(smatchId.length == 0) return;
	var url = urlDoChange;
	$.ajax({
		type : "POST",
		url : url,
		data : {
			"matchId" : matchId,
			"smatchId" : smatchId,
			"teamId" : teamId,
			"uhostedId" : uhostedId,
			"team1Id" : team1Id,
			"team2Id" : team2Id,
		},
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function(response) {
			alert(response.message);
			if(response.code == 'success'){
				doRereshBetHtml(smatchId, teamId);
				$('#btncb'+smatchId).hide();
				$('#totalbet'+smatchId).text(response.totalBet);
				doRefreshData();
			}
		},
		error : function(e) {
			console.log(e)
		}
	});
}

function doRefreshData(){
	$.ajax({
		type : "POST",
		url : urlRefresh,
		cache: false,
		data : {
			"matchId" : matchId,
			"uhostedId" : uhostedId,
			"team1Id" : team1Id,
			"team2Id" : team2Id,
		},
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function(response) {
			if(response.code == 'success'){
				var info = response.infoBet;
				var size = info.length;
				if(size > 0){
					for (i = 0; i < size; i++) {
						var smatchId = info[i].subMatchId;
						$("#rate"+team1Id+smatchId).text(info[i].rateTeam1);
						$("#rate"+team2Id+smatchId).text(info[i].rateTeam2);
						
						$("#moneybo"+team1Id+smatchId).text(info[i].moneyTeam1Bo);
						$("#moneybo"+team2Id+smatchId).text(info[i].moneyTeam2Bo);
						
						$("#profit"+team1Id+smatchId).text(info[i].profitTeam1);
						$("#profit"+team2Id+smatchId).text(info[i].profitTeam2);
					}
					
				}
				var statistic = response.statistic;
				if(statistic != null){
					var money1 = statistic.ubetMoneyTeam1;
					var win1 = statistic.moneyWinTeam1;
					$("#ubet"+team1Id).text(money1 != null ? money1 : 0);
					$("#ubetwin"+team1Id).text(win1 != null ? win1 : 0);
					
					var money2 = statistic.ubetMoneyTeam2;
					var win2 = statistic.moneyWinTeam2;
					$("#ubet"+team2Id).text(money2 != null ? money2 : 0);
					$("#ubetwin"+team2Id).text(win2 != null ? win2 : 0 );
				}
			}
		},
		error : function(e) {
			console.log(e)
		}
	});
}

function doChangeBet1(value){
	var teamId = team1Id;
	doChangeBet(value, teamId);
}

function doChangeBet2(value){
	var teamId = team2Id;
	doChangeBet(value, teamId);
}

function getIdFromAtrIdTag(value, char){
	var id = $(value).attr('id');
	if(id.length == 0 || char.length == 0) return null;
	var split = id.split(char);
	if(!(split.length > 1)) return null;
	return split[1];
}

setInterval(function(){doRefreshData()},5000);






