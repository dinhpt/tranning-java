<div id="main-content">
	<!-- ========= START WRAP VIDEO ========= -->
	<div class="row" id="wrap-video">
		<!-- ========= START CONTENT VIDEO ========= -->
		<div class="col m8 l8" id="content-video">
			<header>
				<h4>Trực tiếp trận chung kết thế giới</h4>
				<span class="btn-like"> </span>
			</header>
			<!-- VIDEO HEAR -->
			<div id="video-content" class="video-container">
				<iframe allowfullscreen="" frameborder="0" height="350"
					src="http://player.twitch.tv/?channel=shagodname&autoplay=true&muted=false"
					width="100%"> </iframe>
			</div>
		</div>
		<!-- ========= START OTHER VIDEO ========= -->
		<div class="col m4 l4" id="list-other-video">
			<ul>
				<c:forEach var="commingMatch" items="${lstMatchComming}">
					<li class="other-video"><a href="#"> <span
							class="logo-game"> <img alt=""
								src="${f:h(commingMatch.gameImgStream)} " />
						</span> <span class="title-video"> ${f:h(commingMatch.name)} </span> <!-- COUTINGDOWN OR LIVE -->
							<c:choose>
								<c:when test="${f:h(commingMatch.status == 1 )}">
									<span class="time-live live"> Live </span>
								</c:when>
								<c:otherwise>
									<span class="time-live"
										data-countdown="${f:h(commingMatch.startTime)}"> </span>
								</c:otherwise>
							</c:choose> <!-- END COUTINGDOWN-->
					</a></li>
				</c:forEach>
			</ul>
		</div>
	</div>
	<!-- =====END MAIN CONTENT====== -->
	<!-- ========= START TAB SCHEDULE ========= -->
	<div id="tab-schedule">
		<div class="row">
			<div id="wrap-tab">
				<!-- ===== HEADER TAB =========== -->
				<ul class="tabs tab-demo z-depth-1">
					<li class="tab"><a class="active" href="#live-tab"> Đang
							trực tiếp </a></li>
					<li class="tab"><a href="#comming-soon-tab"> Sắp diễn ra </a></li>
					<li class="tab"><a class="" href="#24h-tab"> 24h tới </a></li>
					<li class="tab"><a class="" href="#all-tab"> Tất cả </a></li>
					<li class="tab">

						<div id="warp-search">
							<span id="icon-search"> </span>
							<form action="" id="form-search">
								<button class="search" id="btn-search-match"></button>
								<input id="search-math" placeholder="Tìm kiếm..."
									name="search-match-txt" type="text" />
							</form>
						</div>
					</li>
				</ul>
				<!-- ========== START CONTENT TAB =========-->

				<div id="live-tab">

					<!-- =====START LIST MATCH====== -->
					<div class="list-match">
						<h2 class="game-name bg-blue">League of Legend</h2>
						<!-- =====START TABLE MATCHS====== -->
						<table class="responsive-table matchs ">

							<!-- ======START MATCH======= -->
							<tr class="item-match">
								<td class="team1">
									<div class="live-left">
										<span class="live"> Live </span> <span class="viewer">
											2016 </span>
									</div> <span class="team-name"> SKT T1 </span> <span
									class="team-logo"> <img alt="team1"
										src="${f:url('/assests/imgs/team1.png')}" />
								</span> <span class="percent"> 61% </span>
								</td>
								<td class="team2"><span class="percent"> 61% </span> <span
									class="team-logo"> <img alt="team1"
										src="${f:url('/assests/imgs/team2.png')}" />
								</span> <span class="team-name"> Tao Son No1 </span> <span
									class="hosted"> Hosted by <strong> <a href="#">
												Nam LX </a>
									</strong>
								</span></td>
							</tr>
							<!-- ======START MATCH======= -->
							<tr class="item-match">
								<td class="team1">
									<div class="live-left">
										<span class="live"> Live </span> <span class="viewer">
											9830 </span>
									</div> <span class="team-name"> Cloud 9 </span> <span
									class="team-logo"> <img alt="team1"
										src="${f:url('/assests/imgs/team1.png')}" />
								</span> <span class="percent"> 61% </span>
								</td>
								<td class="team2"><span class="percent"> 61% </span> <span
									class="team-logo"> <img alt="team1"
										src="${f:url('/assests/imgs/team2.png')}" />
								</span> <span class="team-name"> Team Solomind </span> <span
									class="hosted"> Hosted by <strong> <a href="#">
												Tukunkun </a>
									</strong>
								</span></td>
							</tr>
						</table>
					</div>
					<!-- =====START LIST MATCH====== -->
					<div class="list-match">
						<c:if test="${f:h(lstLol != null && lstLol.size() > 0 )}">
							<h2 class="game-name bg-blue">League of Legend</h2>
							<!-- =====START TABLE MATCHS====== -->

							<c:forEach var="match" items="${lolList}">
								<table class="responsive-table matchs ">
									<!-- ======START MATCH======= -->
									<tr class="item-match">
										<td class="team1">
											<div class="live-left">
												<span class="live"> Live </span> <span class="viewer">
													${f:h(match.totalShowing)} </span>
											</div> <span class="team-name"> ${f:h(match.team1Name)} </span> <span
											class="team-logo"> <img alt="team1"
												src="${f:h(match.team1Avatar)} " />
										</span> <span class="percent"> ${f:h(match.team1BetPer)}% </span>
										</td>
										<td class="team2"><span class="percent">
												${f:h(match.team2BetPer)}% </span> <span class="team-logo">
												<img alt="team1" src="${f:h(match.team2Avatar)} " />
										</span> <span class="team-name"> ${f:h(match.team2Name)} </span> <span
											class="hosted"> Hosted by <strong> <a
													href="#"> ${f:h(match.userHosted)} </a>
											</strong>
										</span></td>
									</tr>
								</table>

							</c:forEach>


						</c:if>
					</div>
					<!-- =====END LIST MATCH====== -->
					<!-- =====START LIST MATCH====== -->
					<div class="list-match">
						<c:if test="${f:h(dotaList != null && dotaList.size() > 0 )}">
							<h2 class="game-name bg-blue">Dota 2</h2>
							<!-- =====START TABLE MATCHS====== -->
							<c:forEach var="match" items="${dotaList}">
								<table class="responsive-table matchs ">
									<!-- ======START MATCH======= -->
									<tr class="item-match">
										<td class="team1">
											<div class="live-left">
												<span class="live"> Live </span> <span class="viewer">
													${f:h(match.totalShowing)} </span>
											</div> <span class="team-name"> ${f:h(match.team1Name)} </span> <span
											class="team-logo"> <img alt="team1"
												src="${f:h(match.team1Avatar)} " />
										</span> <span class="percent"> ${f:h(match.team1BetPer)}% </span>
										</td>
										<td class="team2"><span class="percent">
												${f:h(match.team2BetPer)}% </span> <span class="team-logo">
												<img alt="team1" src="${f:h(match.team2Avatar)} " />
										</span> <span class="team-name"> ${f:h(match.team2Name)} </span> <span
											class="hosted"> Hosted by <strong> <a
													href="#"> ${f:h(match.userHosted)} </a>
											</strong>
										</span></td>
									</tr>
								</table>
							</c:forEach>
						</c:if>
					</div>
					<!-- =====END LIST MATCH====== -->
					<!-- =====START LIST MATCH====== -->
					<div class="list-match">
						<c:if
							test="${f:h(overwatchList != null && overwatchList.size() > 0 )}">
							<h2 class="game-name bg-blue">Overwatch</h2>
							<!-- =====START TABLE MATCHS====== -->

							<c:forEach var="match" items="${overwatchList}">
								<table class="responsive-table matchs ">
									<!-- ======START MATCH======= -->
									<tr class="item-match">
										<td class="team1">
											<div class="live-left">
												<span class="live"> Live </span> <span class="viewer">
													${f:h(match.totalShowing)} </span>
											</div> <span class="team-name"> ${f:h(match.team1Name)} </span> <span
											class="team-logo"> <img alt="team1"
												src="${f:h(match.team1Avatar)} " />
										</span> <span class="percent"> ${f:h(match.team1BetPer)}% </span>
										</td>
										<td class="team2"><span class="percent">
												${f:h(match.team2BetPer)}% </span> <span class="team-logo">
												<img alt="team1" src="${f:h(match.team2Avatar)} " />
										</span> <span class="team-name"> ${f:h(match.team2Name)} </span> <span
											class="hosted"> Hosted by <strong> <a
													href="#"> ${f:h(match.userHosted)} </a>
											</strong>
										</span></td>
									</tr>
								</table>

							</c:forEach>
						</c:if>
					</div>
					<!-- =====END LIST MATCH====== -->
					<!-- =====START LIST MATCH====== -->
					<div class="list-match">
						<c:if test="${f:h(csList != null && csList.size() > 0 )}">
							<h2 class="game-name bg-blue">CS-GO</h2>
							<!-- =====START TABLE MATCHS====== -->

							<c:forEach var="match" items="${csList}">
								<table class="responsive-table matchs ">
									<!-- ======START MATCH======= -->
									<tr class="item-match">
										<td class="team1">
											<div class="live-left">
												<span class="live"> Live </span> <span class="viewer">
													${f:h(match.totalShowing)} </span>
											</div> <span class="team-name"> ${f:h(match.team1Name)} </span> <span
											class="team-logo"> <img alt="team1"
												src="${f:h(match.team1Avatar)} " />
										</span> <span class="percent"> ${f:h(match.team1BetPer)}% </span>
										</td>
										<td class="team2"><span class="percent">
												${f:h(match.team2BetPer)}% </span> <span class="team-logo">
												<img alt="team1" src="${f:h(match.team2Avatar)} " />
										</span> <span class="team-name"> ${f:h(match.team2Name)} </span> <span
											class="hosted"> Hosted by <strong> <a
													href="#"> ${f:h(match.userHosted)} </a>
											</strong>
										</span></td>
									</tr>
								</table>

							</c:forEach>
						</c:if>
					</div>
					<!-- =====END LIST MATCH====== -->
				</div>
				<div id="comming-soon-tab">Comming soon</div>
				<div id="24h-tab">Comming soon</div>
				<div id="all-tab">Comming soon</div>
			</div>
		</div>
	</div>
</div>
