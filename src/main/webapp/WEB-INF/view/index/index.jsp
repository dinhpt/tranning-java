<%@page import="com.staresport.utils.AppConstants"%>
<tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
	<tiles:put name="content" type="string">
		<script type="text/javascript">
			var urlLoadMatchComing = "${f:url('loadTabMatch')}";
			var urlLoadMatchLive = "${f:url('loadTabMatch')}";
			var urlRctMatchToBet = "${f:url('/bet')}";
			var urlViewLiveStream = "${f:url('/index')}stream/";
			var hourLoadComing = '<%=AppConstants.MATCH_LOAD_OPTION.HOUR_LOAD_COMING_TAB%>';
			var hourLoad24h = '<%=AppConstants.MATCH_LOAD_OPTION.HOUR_LOAD_24H_TAB%>';
			var loadAllMatch = '<%=AppConstants.MATCH_LOAD_OPTION.LOAD_ALL_MATCH%>';
			var loadLiveMatch = '<%=AppConstants.MATCH_LOAD_OPTION.LOAD_LIVE_MATCH%>';
		</script>
		<div id="main-content">
			<input id="hourLoadComing" type="hidden"
				value="${f:h(hourLoadComing)}" /> <input id="hourLoad24h"
				type="hidden" value="${f:h(hourLoad24h)}" /> <input
				id="loadAllMatch" type="hidden" value="${f:h(loadAllMatch)}" /> <input
				id="loadLiveMatch" type="hidden" value="${f:h(loadLiveMatch)}" /> <input
				id="selectedGame" type="hidden" value="${f:h(gameSelect)}" />
			<!-- ========= START WRAP VIDEO ========= -->
			<div class="row" id="wrap-video">
				<!-- ========= START CONTENT VIDEO ========= -->
				<div class="col m8 l8" id="content-video">
					<header>
						<c:choose>
							<c:when test="${f:h(userHostedDto == null )}">
								<h4>STAR ESPORT GAMING</h4>
							</c:when>
							<c:otherwise>
								<h4>${f:h(userHostedDto.matchName)}</h4>
							</c:otherwise>
						</c:choose>
						<span class="btn-like"> </span>
					</header>
					<!-- VIDEO HEAR -->
					<div id="video-content" class="video-container">

						<c:choose>
							<c:when test="${f:h(userHostedDto == null )}">
								<h1 id="stream-countdown">Không có trận đấu nào</h1>
							</c:when>
							<c:when test="${f:h(userHostedDto.matchStatus == 1 )}">
								<iframe allowfullscreen="" frameborder="0" height="350"
									src="${f:h(userHostedDto.stream)}" width="100%"> </iframe>
							</c:when>
							<c:otherwise>
								<h1 id="stream-countdown"
									data-countdown="${f:h(userHostedDto.dateTime)}"></h1>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<!-- ========= START OTHER VIDEO ========= -->
				<div class="col m4 l4 mCustomScrollbar _mCS_2 mCS-autoHide"
					id="list-other-video">
					<div class="mCSB_container" dir="ltr">
						<ul>
							<c:forEach var="commingMatch" items="${lstMatchComming}">
								<li class="other-video"><a
									href="${f:url('stream')}/${f:h(commingMatch.userHostedsId)} ">
										<span class="logo-game"> <img alt=""
											src="${pageContext.request.contextPath}${f:h(commingMatch.gameImgStream)} " />
									</span> <span class="title-video"> ${f:h(commingMatch.name)} </span> <!-- COUTINGDOWN OR LIVE -->
										<c:choose>
											<c:when test="${f:h(commingMatch.status == 1 )}">
												<span class="time-live live"> Live </span>
											</c:when>
											<c:otherwise>
												<span class="time-live"
													data-countdown="${f:h(commingMatch.startTime)}"> </span>
											</c:otherwise>
										</c:choose> <!-- END COUTINGDOWN-->
								</a></li>
							</c:forEach>

						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- =====END MAIN CONTENT====== -->
		<!-- ========= START TAB SCHEDULE ========= -->

		<div id="tab-schedule">
			<div class="row">
				<div id="wrap-tab">
					<div id="load">
						<img src="${f:url('/assests/imgs/loading.gif')}" alt="load" />
					</div>
					<!-- ===== HEADER TAB =========== -->
					<ul class="tabs tab-demo z-depth-1">
						<li class="tab"><a id="link-live" class="active"
							href="#live-tab"
							data-option-tab="urlLoadMatchLive, loadLiveMatch, 'live-tab'"
							onclick="loadTabMatch(urlLoadMatchLive, $('#loadLiveMatch').val(), 'live-tab')">
								Đang trực tiếp </a></li>
						<li class="tab"><a id="link_comming" href="#comming-soon-tab"
							data-option-tab="urlLoadMatchComing, hourLoadComing, 'comming-soon-tab'"
							onclick="loadTabMatch(urlLoadMatchComing, $('#hourLoadComing').val(), 'comming-soon-tab')">
								Sắp diễn ra</a></li>
						<li class="tab"><a id="link-24h" class=""
							href="#next-24h-tab"
							data-option-tab="urlLoadMatchComing, hourLoad24h, 'next-24h-tab'"
							onclick="loadTabMatch(urlLoadMatchComing, $('#hourLoad24h').val(), 'next-24h-tab')">
								24h tới </a></li>
						<li class="tab"><a id="link-all" class="" href="#all-tab"
							data-option-tab="urlLoadMatchComing, loadAllMatch, 'all-tab'"
							onclick="loadTabMatch(urlLoadMatchComing, $('#loadAllMatch').val(), 'all-tab')">
								Tất cả </a></li>
						<li class="tab">

							<div id="warp-search">
								<span id="icon-search"> </span>
								<div id="form-search">
									<!-- <form action="#" id="form-search"> -->
									<button class="search" id="btn-search-match"
										onclick="triggerSearch()"></button>
									<input id="search-math" placeholder="Tìm kiếm..."
										name="search-match-txt" type="text" />
									<!-- </form> -->
								</div>
							</div>
						</li>
					</ul>
					<!-- ========== START CONTENT TAB =========-->

					<div id="live-tab">
						<!-- =====START LIST MATCH====== -->
						<c:choose>
							<c:when
								test="${f:h(lstMatchsDtos != null && lstMatchsDtos.size() > 0 )}">
								<c:forEach var="lstGame" items="${lstMatchsDtos}">
									<c:if
										test="${f:h(lstGame != null && lstGame.matchDtos != null && lstGame.matchDtos.size() > 0 )}">
										<div class="list-match">
											<h2 class="game-name bg-blue">${f:h(lstGame.nameGame)}</h2>
											<!-- =====START TABLE MATCHS====== -->

											<c:forEach var="match" items="${lstGame.matchDtos}">
												<table class="matchs ">
													<!-- ======START MATCH======= -->
													<tr class="item-match"
														onclick="viewStreamLive(${f:h(match.userHostedsId)})">
														<td class="team1">
															<div class="live-left">
																<span class="live"> Live </span> <span class="viewer">
																	${f:h(match.totalShowing)} </span>
															</div>
															<div class="team-info">
																<span class="team-name"> ${f:h(match.team1Name)}
																</span> <span class="team-logo"> <img alt="team1"
																	src="${f:h(match.team1Avatar)} " />
																</span> <span class="percent">
																	${f:h(match.team1BetPer)}% </span>
															</div>
														</td>
														<td class="team2">
															<div class="team-info-1">
																<span class="percent"> ${f:h(match.team2BetPer)}%
																</span> <span class="team-logo"> <img alt="team1"
																	src="${f:h(match.team2Avatar)} " />
																</span> <span class="team-name"> ${f:h(match.team2Name)}
																</span>
															</div> <span class="hosted"> Hosted by <strong>
																	<a href="#"> ${f:h(match.userHosted)} </a>
															</strong>
														</span>
														</td>
													</tr>
												</table>

											</c:forEach>
										</div>
									</c:if>
								</c:forEach>
							</c:when>
							<c:otherwise>
								Không có trận đấu nào
							</c:otherwise>
						</c:choose>
						<!-- =====END LIST MATCH====== -->
					</div>
					<div id="comming-soon-tab"></div>
					<div id="next-24h-tab"></div>
					<div id="all-tab"></div>
				</div>
			</div>
		</div>
	</tiles:put>
</tiles:insert>

