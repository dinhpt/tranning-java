<%@page import="com.staresport.utils.AppConstants"%>
<tiles:insert template="/WEB-INF/view/common/layout-match.jsp" flush="true">
	<tiles:put name="content" type="string">
		<script type="text/javascript">
			var urlAppHost = "${f:url('approveHost')}";
			var urlDoHost = "${f:url('doHost')}";
			var urlRctMatchToBet = "${f:url('/bet')}";
			var emptyLink = '<%=AppConstants.HOST.HOST_NOTIFY.EMPTY_LINK%>';
			var hostOver = '<%=AppConstants.HOST.HOST_NOTIFY.HOST_OVER%>';
			var notLink = '<%=AppConstants.HOST.HOST_NOTIFY.NOT_LINK%>';
			var saveFailed = '<%=AppConstants.HOST.HOST_NOTIFY.SAVE_FAILED%>';
			var saveSuccess = '<%=AppConstants.HOST.HOST_NOTIFY.SAVE_SUCCESS%>';
			var userHosted = '<%=AppConstants.HOST.HOST_NOTIFY.USER_HOSTED%>';
		</script>
		<!-- ========= START WRAP MAIN CONTENT ========= -->
		<div class="container" id="main-content">
			<!-- =====START LIST MATCH====== -->
			<c:forEach var="m" items="${matchsDtos}">
				<div class="list-match">
					<h2 class="game-name bg-blue">${f:h(m.nameGame)}</h2>
					<!-- =====START TABLE MATCHS====== -->
					<table class="responsive-table matchs ">
						<!-- ======START MATCH======= -->
						<c:forEach var="v" items="${m.matchDtos}">
							<input type="hidden" name="uhostedId" value="${f:h(v.uhostedId)}" id="uhid${f:h(v.matchId)}"/>
							<tr class="item-match" onclick="rctmatchToBet(${f:h(v.matchId)})">
								<td class="team1">
									<span class="start-time">${f:h(v.startTime)} </span> 
									<span class="team-name">${f:h(v.team1Name)} </span> 
									<span class="flag ${f:h(v.team1Flag)}" title="${f:h(v.team1Flag)}"></span> 
									<span class="percent">${f:h(v.team1BetPer)}% </span>
								</td>
								<td class="team2">
									<span class="percent">${f:h(v.team2BetPer)}% </span> 
									<span class="flag ${f:h(v.team2Flag)}" title="${f:h(v.team2Flag)}"></span> 
									<span class="team-name">${f:h(v.team2Name)} </span> 
									<c:choose>
										<c:when test="${f:h(v.viewHost)}">
											<a class="hosted" onclick="doHosted()"> Hosted </a>
										</c:when>
										<c:otherwise>
											<a class="hvr-rectangle-out btn-host" id="host${f:h(v.matchId)}"
												onclick="doHost(${f:h(v.matchId)})" href="#model-host"> Host </a>
											<a class="hosted" style="display: none;" onclick="doHosted()" id="hosted${f:h(v.matchId)}"> Hosted </a>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</c:forEach>
					</table>
					
				</div>
			</c:forEach>
			<!-- =====END LIST MATCH====== -->
		</div>
		<!-- =====END MAIN CONTENT====== -->
		<input type="hidden" name="matchId" id="matchId" />
		<input type="hidden" name="userId" id="userId" />
		<!-- Modal Structure -->
		<div class="modal modal-fixed-footer" id="modal-host">
			<div class="modal-content">
				<h2 id="gametype">Language of Legend</h2>
				<button id="close-btn">X</button>
				<p class="host">
					<span>Bạn đã chọn host trận đấu</span>
				</p>
				<p class="host">
					<span id="match-name"></span>
				</p>
				<p class="team">
					<span id="team-name1"></span> vs <span id="team-name2"></span>
				</p>
				<div id="sussgest-link-stream"></div>
				<p class="host">Chèn link video của trận đấu</p>
				<p class="host">
					<input style="width: 50%;" type="text" name="link" id="link" onblur="isLinkStream(this.value)" />
				</p><br/>
				<span class="error-msg empty-link">Bạn cần nhập link stream của trận đấu</span> 
				<span class="error-msg not-link">Bạn cần nhập đúng format link stream của trận đấu</span>
				<a class="btn-win hvr-buzz" data-target="modal-win"
					href="#model-win" onclick="approveHost()"> Đồng ý </a>
			</div>
		</div>
	</tiles:put>
</tiles:insert>
