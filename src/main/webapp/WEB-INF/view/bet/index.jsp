<tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
	<tiles:put name="content" type="string">
		<script type="text/javascript">
			var urlDoBet = "${f:url('dobet')}";
			var urlDoChange = "${f:url('dochangebet')}";
			var urlRefresh = "${f:url('refresh')}";
			var team1Id = '${f:h(team1Id)}';
			var team2Id = '${f:h(team2Id)}';
			var matchId = '${f:h(matchId)}';
			var uhostedId = '${f:h(uhostedId)}';
		</script>
		<div class="" id="main-content">
	        <div class="row" id="wrap-video">
	            <div class="" id="content-video">
	                <header>
	                    <h4>
	                        ${f:h(name)}
	                    </h4>
	                    <span class="hosted">
	                        Hosted by
	                        <strong>
	                            <a href="#">
	                                ${f:h(uhostedName)}
	                            </a>
	                        </strong>
	                    </span>
	                </header>
	                <!-- VIDEO HEAR -->
	                <div class="video-container" id="video-content">
	                    <iframe allowfullscreen="" frameborder="0" height="350" 
	                    	src="${f:h(linkStream)}" width="100%">
	                    </iframe>
	                </div>
	            </div>
	        </div>
	        <div id="notice-match">
	        	<span class="logo-li">
			        <c:choose>
						<c:when test="${f:h(isLive)}">
							LIVE
						</c:when>
						<c:otherwise>
							NOT LIVE
						</c:otherwise>
					</c:choose>
	                
	            </span>
	            <span class="logo-pe">
	                9830
	            </span>
	        </div>
	        <!-- ==================start bet ================ -->
	        <div id="bet">
	            <div class="row" id="list-video">
	                <!-- ==============================Start header live========================== -->
	                <header>
	                    <table class="live-people">
	                        <tr class="item-match">
	                            <td class="team1">
	                                <span class="team-name">
	                                    ${f:h(team1Name)} 
	                                </span>
	                                <span class="team-logo">
	                                    <img alt="" src="${f:h(team1Avatar)}"/>
	                                </span>
	                                <span class="percent">
	                                    ${f:h(team1BetPer)}%
	                                </span>
	                            </td>
	                            <td class="team2">
	                                <span class="percent">
	                                    ${f:h(team2BetPer)}%
	                                </span>
	                                <span class="team-logo">
	                                    <img alt="" src="${f:h(team2Avatar)}"/>
	                                </span>
	                                <span class="team-name">
	                                    ${f:h(team2Name)} 
	                                </span>
	                            </td>
	                        </tr>
	                    </table>
	                </header>
	                <!-- ===========================End header ================================ -->
	                <div class="main-live">
	                    <div class="Betting">
	                       <!-- ==============START GROUP ORDER================= -->
	                       <c:forEach var="m" items="${betDetailList}">
	                       		<div class="group-order">
		                            <div class="group-top">
		                                <span class="per-order-left">
		                                    1 ăn
		                                    <strong id="rate${f:h(team1Id)}${f:h(m.subMatchId)}">
		                                        ${f:h(m.rateTeam1)}
		                                    </strong>
		                                </span>
		                                	<c:choose>
												<c:when test="${f:h(m.noMatch == 0)}">
													<span class="title-order unlock">
														Đặt cược cả trận BO${f:h(bestOf)} 
													</span>
												</c:when>
												<c:otherwise>
													<span class="title-order">
														Đặt cược trận ${f:h(m.noMatch)}
													</span>
												</c:otherwise>
											</c:choose>
		                                    
		                                <span class="per-order-right">
		                                    1 ăn
		                                    <strong id="rate${f:h(team2Id)}${f:h(m.subMatchId)}">
		                                        ${f:h(m.rateTeam2)}
		                                    </strong>
		                                </span>
		                            </div>
		                            <div class="progress">
		                                <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="${f:h(m.pctgTeam1)}" class="progress-bar" style="width: ${f:h(m.pctgTeam1)}%" role="progressbar">
		                                    <span class="line-in">
		                                    </span>
		                                    <span class="pull-left money" id="moneybo${f:h(team1Id)}${f:h(m.subMatchId)}">
		                                       ${f:h(m.moneyTeam1Bo)}
		                                    </span>
		                                    <span class="bulk">
		                                    </span>
		                                </div>
		                                <span class="line-out">
		                                </span>
		                                <span class="pull-right money" id="moneybo${f:h(team2Id)}${f:h(m.subMatchId)}">
		                                    ${f:h(m.moneyTeam2Bo)}
		                                </span>
		                            </div>
		                            <div class="group-bottom">
		                                <div class="col s5 col-left" id="cdn${f:h(team1Id)}${f:h(m.subMatchId)}">
		                                    <div class="box-order clearfix">
		                                        <c:choose>
													<c:when test="${f:h((team1Id == m.betTeamId) || m.betTeamId == 0)}">
														<input placeholder="Nhập số sao đặt" id="money${f:h(team1Id)}${f:h(m.subMatchId)}" 
		                                        			type="text" class="col s10"/>
														<button class="btn-red col s2" id="betsmid${f:h(m.subMatchId)}" 
		                                        			onclick="doBet1(this)">Đặt</button>   
													</c:when>
													<c:otherwise>
														<input class="col s10" readonly="readonly" placeholder="Không thể đặt tất cả 2 bên" type="text">
														<button class="btn-gray col s2" >Đặt</button>   
													</c:otherwise>
												</c:choose>
		                                    </div>
		                                    <c:choose>
												<c:when test="${f:h(team1Id == m.betTeamId)}">
													<p class="expect">Dự kiến thắng <strong id="profit${f:h(team1Id)}${f:h(m.subMatchId)}">${f:h(m.profitTeam1)}</strong> sao</p>
		                                    		<p class="ordered">Bạn đã đặt: <strong id="totalbet${f:h(m.subMatchId)}">${f:h(m.betMoney)}</strong> sao</p>
												</c:when>
												<c:otherwise>
												
												</c:otherwise>
											</c:choose>
											<c:choose>
												<c:when test="${f:h(team2Id == m.betTeamId && m.numChange < 1)}">
													<button class="btn-change" id="btncb${f:h(m.subMatchId)}" onclick="doChangeBet1(this)">Đổi kèo</button>
												</c:when>
												<c:otherwise>
												</c:otherwise>
											</c:choose>

										</div>
		                                <div class="col s5 col-right" id="cdn${f:h(team2Id)}${f:h(m.subMatchId)}">
		                                    <div class="box-order clearfix">
		                                    	<c:choose>
													<c:when test="${f:h((team2Id == m.betTeamId) || m.betTeamId == 0)}">
														<button class="btn-red col s2" id="betsmid${f:h(m.subMatchId)}" 
		                                        			 onclick="doBet2(this)">Đặt</button>   
	                                        			<input placeholder="Nhập số sao đặt" id="money${f:h(team2Id)}${f:h(m.subMatchId)}" 
	                                        				type="text" class="col s10"/>
													</c:when>
													<c:otherwise>
														<button class="btn-gray col s2">Đặt</button>   
														<input class="col s10" readonly="readonly" placeholder="Không thể đặt tất cả 2 bên" type="text">
													</c:otherwise>
												</c:choose>
		                                        
		                                    </div>
		                                    <c:choose>
												<c:when test="${f:h(team2Id == m.betTeamId)}">
													<p class="expect">Dự kiến thắng <strong id="profit${f:h(team2Id)}${f:h(m.subMatchId)}">${f:h(m.profitTeam2)}</strong> sao</p>
		                                    		<p class="ordered">Bạn đã đặt: <strong id="totalbet${f:h(m.subMatchId)}">${f:h(m.betMoney)}</strong> sao</p>
												</c:when>
												<c:otherwise>
												</c:otherwise>
											</c:choose>
											<c:choose>
												<c:when test="${f:h(team1Id == m.betTeamId && m.numChange < 1)}">
													<button class="btn-change" id="btncb${f:h(m.subMatchId)}" onclick="doChangeBet2(this)">Đổi kèo</button>
												</c:when>
												<c:otherwise>
												</c:otherwise>
											</c:choose>
		                                </div>
		                            </div>
		                        </div>
	                       </c:forEach>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- =================end bet main============================= -->
	         <div class="clearfix"></div>
             <footer id="wap-footer">
                 <div class="sum-bet-tea1">
                     <p>Tổng tiền đặt cho ${f:h(team1Name)} <span class="color-tea1" id="ubet${f:h(team1Id)}">${f:h(ubetMoneyTeam1)}</span> sao</p>
                     <p class="Expect-tea1">Dự kiến thắng <span class="color-tea1" id="ubetwin${f:h(team1Id)}">${f:h(moneyWinTeam1)}</span> sao</p>
                 </div>
                 <div class="sum-bet-tea2">
                     <p>Tổng tiền đặt cho ${f:h(team2Name)} <span class="color-tea2" id="ubet${f:h(team2Id)}">${f:h(ubetMoneyTeam2)}</span> sao</p>
                     <p class="Expect-tea2">Dự kiến thắng <span class="color-tea2" id="ubetwin${f:h(team2Id)}">${f:h(moneyWinTeam2)}</span> sao</p>
                 </div>
             </footer><!-- ===========end footer bet main================= -->
	    </div>
	</tiles:put>
</tiles:insert>
