<tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
	<tiles:put name="content" type="string">
		<section class="login">
			<h1>
				STAR ESPORT<br>
			</h1>
			<s:form method="POST" action="/login/authenticate">
				<p>
					UserName<br>
					<input type="text" name="username" value='${f:h(username)}' />
				</p>
				<p>
					Password<br> <input type="password" name="password"
						value='${f:h(password)}' />
				</p>
				<p>
					<button type="submit">Sign in</button>
				</p>
			</s:form>
		</section>
	</tiles:put>
</tiles:insert>
