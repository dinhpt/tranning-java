<%@page pageEncoding="UTF-8"%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="${f:url('/css/sa.css')}" />
<link rel="stylesheet" type="text/css" href="${f:url('/css/style.css')}">
<script type="text/javascript" src="${f:url('/js/jquery-2.2.4.min.js')}"></script>
<script type="text/javascript" src="${f:url('/js/header.min.js')}"></script>
<script type="text/javascript" src="${f:url('/js/ui-mock.min.js')}"></script>
</head>
<body>
	<jsp:include page="common/header.jsp" />

</body>
</html>
