<link rel="stylesheet"
	href="http://fonts.googleapis.com/icon?family=Material+Icons" />
<link rel="stylesheet" href="${f:url('/assests/css/materialize.min.css')}"
	media="screen,projection" type="text/css" />
<link rel="stylesheet" href="${f:url('/assests/css/font-awesome.min.css')}"
	media="screen,projection" type="text/css" />
<link rel="stylesheet" href="${f:url('/assests/css/esport.css')}"
	media="screen,projection" type="text/css" />
<!-- start dinhpt add domain dynamic -->
<style>
span.flag {
    background: rgba(0, 0, 0, 0) url("${f:url('/')}assests/imgs/flags.png") no-repeat scroll 0 0;
    display: inline-block;
    vertical-align: middle;
}
</style>
<!-- end dinhpt add domain dynamic -->
<link rel="stylesheet" href="${f:url('/assests/css/flag.css')}"
	media="screen,projection" type="text/css" />
<link rel="stylesheet" href="${f:url('/assests/css/hover.css')}"
	media="screen,projection" type="text/css" />
<link rel="stylesheet" href="${f:url('/assests/css/layout.css')}"
	media="screen,projection" type="text/css" />
<link rel="stylesheet" href="${f:url('/assests/css/responsive.css')}"
	media="screen,projection" type="text/css" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<link href="${f:url('/assests/css/jquery.mCustomScrollbar.css')}" rel="stylesheet" />
<link href="${f:url('/assests/css/bet.css')}" rel="stylesheet"/>
<link rel="stylesheet" href="${f:url('/assests/css/materialize.css')}" rel="stylesheet" />
<link rel="stylesheet" href="${f:url('/assests/css/type.css')}" rel="stylesheet" />

<!--Import jQuery before materialize.js-->
<script src="${f:url('/assests/js/jquery-2.1.1.min.js')}" type="text/javascript"></script>
<script src="${f:url('/assests/js/materialize.min.js')}" type="text/javascript"></script>
<script src="${f:url('/assests/js/jquery.nicescroll.js')}" type="text/javascript"></script>
<script src="${f:url('/assests/js/jquery.mCustomScrollbar.concat.min.js')}" type="text/javascript"></script>
<script src="${f:url('/assests/js/esport.js')}" type="text/javascript"></script>
<script src="${f:url('/assests/js/jquery.countdown.min.js')}" type="text/javascript"></script>

