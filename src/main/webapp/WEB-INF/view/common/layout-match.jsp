<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1024">
<title>STAR ESPORT</title>

<jsp:include page="../common/common-header.jsp" />
</head>
<body id="esport-home">
	<div class="row">
		<jsp:include page="../common/aside-left.jsp" />
		<section class="col s12 m10 l10" id="warp-main-content">
			<jsp:include page="../common/header-match.jsp" />
			<tiles:insert attribute="content" />
		</section>
		
		<jsp:include page="../common/footer.jsp" />
	</div>
</body>
</html>