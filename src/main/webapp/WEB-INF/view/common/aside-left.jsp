<!-- ====== START SIDEBAR ========= -->
<header class="hea-mobile active">
	<span class="icon-menu active"> <i class="fa fa-bars"
		aria-hidden="true"></i>
	</span> <img class="logo-mobile" alt="" src="${f:url('/assests/imgs/logo.png')}" /> <span
		class="chat-word"> <i class="fa fa-comments" aria-hidden="true"></i>
	</span>
</header>
<aside class="col s12 m2 l2" id="sidebar">
	<div class="row col s12 m2 l2" id="bg-sidebar"></div>
	<div id="logo">
		<a href="${f:url('/index')}"> <img alt="logo"
			class="responsive-img" src="${f:url('/assests/imgs/logo.png')}" />
		</a>
	</div>
	<!-- ========START FORM SEARCH========= -->
	<!-- <form action="post" id="form-search">
		<input id="search-txt" name="search_txt" placeholder="Search"
			type="text" />
		<button class="material-icons" id="btn-search" type="submit">
			search</button>
	</form> -->
	<!-- ========START LIST GAMES========= -->
	<div id="list-games">
		<ul>
			<c:choose>
				<c:when test="${f:h(lstGameType != null && lstGameType.size() > 0)}">
					<c:forEach var="game" items="${lstGameType}">
						<!-- class notice -->
						<c:choose>
							<c:when test="${f:h(game.gameTypeId == gameSelect)}">
								<li class="item-game game-active"><a class=""
									href="${pageContext.request.contextPath}/${f:h(game.gameTypeId)}">
										<img alt="logo game"
										src="${pageContext.request.contextPath}${f:h(game.imgUrl)}" />
										<span class="name-game">${f:h(game.name)} </span>
								</a></li>
							</c:when>
							<c:otherwise>
								<li class="item-game "><a class=""
									href="${pageContext.request.contextPath}/${f:h(game.gameTypeId)}">
										<img alt="logo game"
										src="${pageContext.request.contextPath}${f:h(game.imgUrl)}" />
										<span class="name-game">${f:h(game.name)} </span>
								</a></li>
							</c:otherwise>
						</c:choose>

					</c:forEach>
				</c:when>
			</c:choose>

		</ul>
	</div>

	<!-- ========START LIST GOLIVE========= -->
	<div id="list-golive">
		<h2>Xem nhiều nhất</h2>
		<ul>
			<c:choose>
				<c:when
					test="${f:h(lstMostViewing != null && lstMostViewing.size() > 0)}">
					<c:forEach var="match" items="${lstMostViewing}">
						<li class="item-golive"><a
							href="${f:url('/index')}stream/${f:h(match.userHostedsId)}">
								<span class="avt-streamer"> <img alt=""
									src="${pageContext.request.contextPath}${f:h(match.hostedAvar)} " />
							</span> <span class="streamer"> ${f:h(match.userHosted)} </span> <span
								class="caption"> Trực tiếp ${f:h(match.gameShortName)} </span>
						</a></li>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<li>Không có trận đấu nào</li>
				</c:otherwise>
			</c:choose>
		</ul>
	</div>
</aside>
<!-- ====== END SIDEBAR ========= -->