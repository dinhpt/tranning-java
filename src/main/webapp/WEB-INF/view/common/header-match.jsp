<!-- ========= START WRAP USER ========= -->
<!-- ========= START HEADER ========= -->
<header id="wrap-user" class="page-match">
	<c:choose>
		<c:when test="${f:h(user != null)}">
			<div id="user-infor">
				<div id="wrap-gavatar">
					<img alt="gavatar" id="gravatar" src="${f:url('/assests/imgs/user.png')}"  class="dropdown-button"  data-activates='dropdown1'/>
						<!-- Dropdown Structure -->
                              <ul id='dropdown1' class='dropdown-content'>
                                <li>
                                    <a href="#" class="member">Trang cá nhân</a>
                                </li>
                                <li><a href="${f:url('/history')}" class="his-bet">Lịch sử đặt cược</a></li>
                                <li><a href="${f:url('/index')}doLogout" class="outing"><i class="fa fa-sign-out" aria-hidden="true"></i>Đăng xuất</a></li>
                              </ul>
				</div>
				<span id="user-name"> ${f:h(user.name)} </span>
				<p id="wrap-coins">
					<span class="coins"> 1.234.567.890 </span> <a class="btn hrv-"
						href="#" id="btn-buy-coin"> Nạp </a>
				</p>
			</div>
			<ul id="nav-action">
				<li class="action-golive"><c:choose>
						<c:when test="${f:h(user.permission.viewMatch == true)}">
							<a href="${f:url('/match')}"> golive </a>
						</c:when>
						<c:otherwise>

						</c:otherwise>
					</c:choose></li>
				<li class="action-confirm"><c:choose>
						<c:when test="${f:h(user.permission.viewNotiEndMatch == true)}">
							<a href="#"> confirm </a>
						</c:when>
						<c:otherwise>

						</c:otherwise>
					</c:choose></li>
			</ul>
		</c:when>
		<c:otherwise>
			<div id="user-infor"
				style="display: block !important; text-align: center;">
				<p id="wrap-login">
					<a href="http://14.160.32.2:8081/Portal/Account/Register/?sid=10101" class="btn hvr-pop" id="login">Đăng ký</a> <a
						href="${f:url('/login')}" class="btn hvr-pop" id="logout">Đăng
						nhập</a>
				</p>
			</div>
		</c:otherwise>
	</c:choose>
</header>
<!-- ========= START HEADER ========= -->