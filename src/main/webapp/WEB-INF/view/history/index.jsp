<tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
	<tiles:put name="content" type="string">
		<div class="" id="main-content">
			<!-- =====START LIST MATCH====== -->
			<div class="list-match">
				<h2 class="history-title">Lịch Sử Đặt Cược</h2>
				<!-- =====START TABLE MATCHS====== -->
				<table class="responsive-table matchs">
					<!-- ======START MATCH======= -->
					<tr class="item default">
						<td class="time"><span>Thời Gian</span></td>
						<td class="game"><span class="game-name">Game</span></td>
						<td class="match"><span>Trận</span></td>
						<td class="money-bet"><span>Tiền bet</span></td>
						<td class="status"><span>Trạng Thái</span></td>
					</tr>
					<!-- ======END MATCH======= -->
					<c:if
						test="${f:h(lstUserBetDtos != null && lstUserBetDtos.size()> 0)}">
						<c:forEach var="userBetDtos" items="${lstUserBetDtos}">
							<c:if test="${f:h(userBetDtos != null)}">
								<c:forEach var="userBetJson"
									items="${userBetDtos.lstUserBetJson}">
									<c:if test="${f:h(userBetJson.betTeamId != null)}">
										<tr class="item">
											<c:choose>
												<c:when test="${f:h(userBetJson.matchStatus == 0)}">
													<td class="time yet-started"><span>${f:h(userBetJson.betTime)}</span>
														<span></span></td>
												</c:when>
												<c:otherwise>
													<td class="time"><span>${f:h(userBetJson.betTime)}</span>
														<span></span></td>
												</c:otherwise>
											</c:choose>

											<td class="game"><span>${f:h(userBetDtos.gameName) }</span></td>
											<td class="match">
												<p>
													<span class="note-team-1"> <span
														class="team-name color-1">${f:h(userBetDtos.team1Name) }</span>
														<span class="team-logo"> <img class="logo-history" alt="team1"
															src="${f:h(userBetDtos.team1Avatar) }" />
													</span>
													</span> <span class="VS">VS</span> <span class="note-team-2">
														<span class="team-logo"> <img class="logo-history" alt="team1"
															src="${f:h(userBetDtos.team2Avatar) }" />
													</span> <span class="team-name color-2">${f:h(userBetDtos.team2Name) }</span>
													</span>
												</p>
												<p>
													<c:choose>
														<c:when
															test="${f:h(userBetJson.betTeamId == userBetDtos.team1Id  )}">

															<span class="color-1">đặt cược cho
																${f:h(userBetDtos.team1Name) }</span>
														</c:when>
														<c:otherwise>
															<span class="color-2">đặt cược cho
																${f:h(userBetDtos.team2Name) }</span>
														</c:otherwise>
													</c:choose>
												</p>
											</td>
											<td class="money-bet"><span><fmt:formatNumber
														type="number" maxFractionDigits="3"
														value="${f:h(userBetJson.betMoney)}" /></span></td>
											<td class="status"><c:choose>
													<c:when test="${f:h(userBetJson.matchStatus == 0)}">
														<span class="color-2">Chưa bắt đầu</span>
													</c:when>
													<c:when test="${f:h(userBetJson.matchStatus == 1)}">
														<span class="color-2">Đang diễn ra</span>
													</c:when>
													<c:when test="${f:h(userBetJson.profit > 0)}">
														<span class="color-3">Thắng</span>
														<strong class="color-3"><fmt:formatNumber
																type="number" maxFractionDigits="3"
																value="${f:h(userBetJson.profit)}" /></strong>
													</c:when>
													<c:when test="${f:h(userBetJson.profit < 0)}">
														<span class="color-5">Thua</span>
														<strong class="color-5"><fmt:formatNumber
																type="number" maxFractionDigits="3"
																value="${f:h(userBetJson.profit)}" /></strong>
													</c:when>
													<c:otherwise>
														<span class="color-2">Chưa bắt đầu</span>
													</c:otherwise>
												</c:choose></td>
										</tr>
									</c:if>
								</c:forEach>
							</c:if>
						</c:forEach>
					</c:if>

					<!-- ======END MATCH======= -->
				</table>
				<!--================End table matchs=================  -->
			</div>
			<!-- =====END LIST MATCH====== -->
		</div>
	</tiles:put>
</tiles:insert>