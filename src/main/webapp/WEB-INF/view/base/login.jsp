<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1024">
<title>ログイン | Barrio eye Nurse</title>

<link rel="stylesheet" href="../asset/css/style.css">
</head>
<body>

<section class="login">
	<h1>地域医療連携　訪問看護支援システム<br><b>Barrio eye Nurse</b></h1>
	<form action="../base/home.html">
		<p>
			法人コード<br>
			<input type="text">
		</p>
		<p>
			ログインID<br>
			<input type="text">
		</p>
		<p>
			パスワード<br>
			<input type="password">
		</p>
		
		<p>
			<button type="submit">ログイン</button>
		</p>
	</form>
</section>

</body>
</html>
