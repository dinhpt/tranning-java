<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1024">
<title>Barrio eye  Nurse</title>

<link rel="stylesheet" type="text/css" href="${f:url('/css/style.css')}">
<script type="text/javascript" src="${f:url('/js/jquery-2.2.4.min.js')}"></script>
<script type="text/javascript" src="${f:url('/js/header.min.js')}"></script>
<script type="text/javascript" src="${f:url('/js/ui-mock.min.js')}"></script>

</head>
<body>

<header>
	<div class="logo"><a href="../">Barrio eye Nurse</a></div>
	
	<div class="account">
		<ul>
			<li class="name">ユーザー名</li>
			<li class="loginId">ログインID</li>
			<li class="logout"><a href="../base/login.html">ログアウト</a></li>
		</ul>
	</div>
	
	<nav>
		<ul class="firstNav">
			<li class="home"><a href="../base/home.html">ホーム</a></li>
			<li class="user"><span>利用者</span>
				<ul class="secondNav">
					<li><a href="">利用者文書検索</a></li>
					<li><a href="">利用者基本情報</a></li>
				</ul>
			</li>	
			<li class="document"><span>文書</span>
				<ul class="secondNav">
					<li><a href="">文書検索</a></li>
					<li><a href="">指示書</a></li>
					<li><a href="">記録書Ⅰ</a></li>
					<li><a href="">計画書</a></li>
					<li><a href="">記録書Ⅱ</a></li>
					<li><a href="">報告書</a></li>
					<li><a href="">情報提供書</a></li>
				</ul>
			</li>
			<li class="planning"><span>訪問予定</span>
				<ul class="secondNav">
					<li><a href="">訪問予定一覧</a></li>
					<li><a href="">スタッフ月間予定</a></li>
					<li><a href="">スタッフ週間予定</a></li>
					<li><a href="">当日訪問一覧</a></li>
					<li><a href="">利用者月間予定</a></li>
					<li><a href="">月間訪問数</a></li>
					<li><a href="">月間スタッフ数</a></li>
				</ul>
			</li>
			<li class="billing"><a href="">請求処理</a></li>
			<li class="master"><span>マスタ</span>
				<ul class="secondNav">
					<li><a href="../master/office.html">事業所</a></li>
					<li><a href="">スタッフ</a></li>
					<li><a href="">医療機関等</a></li>
					<li><a href="">医師・ケアマネ</a></li>
					<li><a href="">衛生材料</a></li>
					<li><a href="">保険者</a></li>
					<li><a href="">公費負担者</a></li>
					<li><a href="">エリア</a></li>
					<li><a href="">定型句</a></li>
					<li><a href="">アクセス権限</a></li>
				</ul>
			</li>
			<li class="system"><span>システム</span>
				<ul class="secondNav">
					<li><a href="">法人登録</a></li>
					<li><a href="">介護単位</a></li>
					<li><a href="">医療単位</a></li>
				</ul>
			</li>
		</ul>
	</nav>
</header>

</body>
</html>
