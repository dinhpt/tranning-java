/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : bssd_esport

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2017-11-06 10:27:31
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `bet_configs`
-- ----------------------------
DROP TABLE IF EXISTS `bet_configs`;
CREATE TABLE `bet_configs` (
  `bet_config_id` int(11) NOT NULL,
  `times_per_match` int(11) NOT NULL,
  `max_money_bet` float NOT NULL,
  PRIMARY KEY (`bet_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of bet_configs
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_configs`
-- ----------------------------
DROP TABLE IF EXISTS `sys_configs`;
CREATE TABLE `sys_configs` (
  `scid` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `value` varchar(128) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`scid`,`code`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_configs
-- ----------------------------
INSERT INTO `sys_configs` VALUES ('1', 'bet_fee', '3', 'fee bet ', 'tiền phế (%) cho kèo', null, null, null, null);
INSERT INTO `sys_configs` VALUES ('2', 'host_time_expire', '5', null, 'Thoi gian khong cho host truoc khi tran dau dien ra bao nhieu phut', null, null, null, null);
INSERT INTO `sys_configs` VALUES ('3', 'bet_time_expire', '1', null, 'Thoi gian khong cho bet truoc khi tran dau dien ra bao nhieu phut', null, null, null, null);
INSERT INTO `sys_configs` VALUES ('4', 'bet_turns', '3', null, 'Số lượt tối đa được phép bet cho 1 kèo', null, null, null, null);
INSERT INTO `sys_configs` VALUES ('5', 'bet_turns_change', '1', null, 'Số lượt tối đa được đổi kèo', null, null, null, null);
INSERT INTO `sys_configs` VALUES ('6', 'host_turns_max', '5', null, '1 tran dau duoc toi da bao nhieu lan host', null, null, null, null);
INSERT INTO `sys_configs` VALUES ('7', 'upd_res_hour', '12', null, 'Lay cac tran dau ma khong lay dc ket qua dau x gio', null, null, null, null);
INSERT INTO `sys_configs` VALUES ('8', 'email_notify_from', 'dudoanesportnotify@gmail.com', null, 'tai khoan mail gui notify', null, null, null, null);
INSERT INTO `sys_configs` VALUES ('9', 'email_notify_pass', 'esport@123', null, 'password cua mail notify', null, null, null, null);
INSERT INTO `sys_configs` VALUES ('10', 'email_notify_to', 'esport_notice@scfs-outsource.mailclark.ai', null, 'email slack', null, null, null, null);
INSERT INTO `sys_configs` VALUES ('11', 'email_notify_host', 'smtp.gmail.com', null, 'email host ', null, null, null, null);
INSERT INTO `sys_configs` VALUES ('12', 'notify_url_approved', 'http://www.dudoanesports.vn/approved', null, 'link duyet', null, null, null, null);
INSERT INTO `sys_configs` VALUES ('13', 'gift_code_win_amount', '50000', null, 'so luong tien thang de duoc gift code', null, null, null, null);
INSERT INTO `sys_configs` VALUES ('14', 'approved_fetch_days', '4', null, 'So ngay hien thi lich su duyet', null, null, null, null);
INSERT INTO `sys_configs` VALUES ('15', 'beted_fetch_days', '7', null, 'So ngay hien thi lich su dat cuoc', null, null, null, null);
INSERT INTO `sys_configs` VALUES ('16', 'report_fetch_days', '30', null, 'so ngay default báo cáo phế, khi không chọn thì lấy từ curent ngược lại 30 day', null, null, null, null);
INSERT INTO `sys_configs` VALUES ('17', 'time_match_lose', '15', null, 'sau x phut tran dau khong cap nhat trang thai running', null, null, null, null);

-- ----------------------------
-- Table structure for `user_types`
-- ----------------------------
DROP TABLE IF EXISTS `user_types`;
CREATE TABLE `user_types` (
  `user_type_id` int(11) NOT NULL,
  `code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AVG_ROW_LENGTH=4096;

-- ----------------------------
-- Records of user_types
-- ----------------------------
INSERT INTO `user_types` VALUES ('1', 'admin', null);
INSERT INTO `user_types` VALUES ('2', 'partner', null);
INSERT INTO `user_types` VALUES ('3', 'vip', null);
INSERT INTO `user_types` VALUES ('4', 'normal', null);

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `total_money` float DEFAULT NULL,
  `total_money_beted` float DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `avatar_url` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AVG_ROW_LENGTH=16384;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '1', 'Admin', 'admin', null, 'fEqNCco3Yq9h5ZUglD3CZJT4lBs=', null, null, null, null, null, null, null, '/assests/imgs/user.png');
INSERT INTO `users` VALUES ('2', '2', 'Partner', 'partner', null, 'sNfXNqOfAyECZDLfPvSEJAORGew=', null, null, null, null, null, null, null, null);
INSERT INTO `users` VALUES ('3', '3', 'Vip', 'vip', null, 'sNfXNqOfAyECZDLfPvSEJAORGew=', null, null, null, null, null, null, null, null);
INSERT INTO `users` VALUES ('4', '4', 'Normal', 'normal', null, 'sNfXNqOfAyECZDLfPvSEJAORGew=', null, null, null, null, null, null, null, null);
