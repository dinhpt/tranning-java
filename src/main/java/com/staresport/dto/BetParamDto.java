package com.staresport.dto;

public class BetParamDto {
	private String matchId;
	private String betTeamId;
	private String uhostedId;
	private String smatchId;
	private String team1Id;
	private String team2Id;
	private String betMoney;
	
	public String getMatchId() {
		return matchId;
	}
	public void setMatchId(String matchId) {
		this.matchId = matchId;
	}
	public String getBetTeamId() {
		return betTeamId;
	}
	public void setBetTeamId(String betTeamId) {
		this.betTeamId = betTeamId;
	}
	public String getUhostedId() {
		return uhostedId;
	}
	public void setUhostedId(String uhostedId) {
		this.uhostedId = uhostedId;
	}
	public String getSmatchId() {
		return smatchId;
	}
	public void setSmatchId(String smatchId) {
		this.smatchId = smatchId;
	}
	public String getTeam1Id() {
		return team1Id;
	}
	public void setTeam1Id(String team1Id) {
		this.team1Id = team1Id;
	}
	public String getTeam2Id() {
		return team2Id;
	}
	public void setTeam2Id(String team2Id) {
		this.team2Id = team2Id;
	}
	public String getBetMoney() {
		return betMoney;
	}
	public void setBetMoney(String betMoney) {
		this.betMoney = betMoney;
	}
	
	
}
