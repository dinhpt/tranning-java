package com.staresport.dto;



public class UserBetDetailDto {
	private Integer userId;
	private Integer matchId;
	private Integer uhostedId;
	private Integer subMatchId;
	private Integer betTeamId;
	private Integer betMoney;
	private Double profit;
	private Integer numBet;
	private Integer numChange;
	private Integer noMatch;
	
	private String moneyWinTeam1;
	private String moneyWinTeam2;
	private Double ubetMoneyTeam1;
	private Double ubetMoneyTeam2;
	private Double totalMoneyTeam1;
	private Double totalMoneyTeam2;
	// PERCENTAGE 
	private String pctgTeam1;
	private String pctgTeam2;
	// RATE
	private Double rateTeam1;
	private Double rateTeam2;
	private Double moneyTeam1Bo;
	private Double moneyTeam2Bo;
	private Double ubdMoneyTeam1;
	private Double ubdMoneyTeam2;
	// PROFIT
	private Integer profitTeam1;
	private Integer profitTeam2;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getMatchId() {
		return matchId;
	}
	public void setMatchId(Integer matchId) {
		this.matchId = matchId;
	}
	public Integer getUhostedId() {
		return uhostedId;
	}
	public void setUhostedId(Integer uhostedId) {
		this.uhostedId = uhostedId;
	}
	public Integer getSubMatchId() {
		return subMatchId;
	}
	public void setSubMatchId(Integer subMatchId) {
		this.subMatchId = subMatchId;
	}
	public Integer getBetTeamId() {
		return betTeamId;
	}
	public void setBetTeamId(Integer betTeamId) {
		this.betTeamId = betTeamId;
	}
	public Integer getBetMoney() {
		return betMoney;
	}
	public void setBetMoney(Integer betMoney) {
		this.betMoney = betMoney;
	}
	public Double getProfit() {
		return profit;
	}
	public void setProfit(Double profit) {
		this.profit = profit;
	}
	public Integer getNumBet() {
		return numBet;
	}
	public void setNumBet(Integer numBet) {
		this.numBet = numBet;
	}
	public String getMoneyWinTeam1() {
		return moneyWinTeam1;
	}
	public void setMoneyWinTeam1(String moneyWinTeam1) {
		this.moneyWinTeam1 = moneyWinTeam1;
	}
	public String getMoneyWinTeam2() {
		return moneyWinTeam2;
	}
	public void setMoneyWinTeam2(String moneyWinTeam2) {
		this.moneyWinTeam2 = moneyWinTeam2;
	}
	public Double getUbetMoneyTeam1() {
		return ubetMoneyTeam1;
	}
	public void setUbetMoneyTeam1(Double ubetMoneyTeam1) {
		this.ubetMoneyTeam1 = ubetMoneyTeam1;
	}
	public Double getUbetMoneyTeam2() {
		return ubetMoneyTeam2;
	}
	public void setUbetMoneyTeam2(Double ubetMoneyTeam2) {
		this.ubetMoneyTeam2 = ubetMoneyTeam2;
	}
	public Double getTotalMoneyTeam1() {
		return totalMoneyTeam1;
	}
	public void setTotalMoneyTeam1(Double totalMoneyTeam1) {
		this.totalMoneyTeam1 = totalMoneyTeam1;
	}
	public Double getTotalMoneyTeam2() {
		return totalMoneyTeam2;
	}
	public void setTotalMoneyTeam2(Double totalMoneyTeam2) {
		this.totalMoneyTeam2 = totalMoneyTeam2;
	}
	public String getPctgTeam1() {
		return pctgTeam1;
	}
	public void setPctgTeam1(String pctgTeam1) {
		this.pctgTeam1 = pctgTeam1;
	}
	public String getPctgTeam2() {
		return pctgTeam2;
	}
	public void setPctgTeam2(String pctgTeam2) {
		this.pctgTeam2 = pctgTeam2;
	}
	public Double getRateTeam1() {
		return rateTeam1;
	}
	public void setRateTeam1(Double rateTeam1) {
		this.rateTeam1 = rateTeam1;
	}
	public Double getRateTeam2() {
		return rateTeam2;
	}
	public void setRateTeam2(Double rateTeam2) {
		this.rateTeam2 = rateTeam2;
	}
	public Double getMoneyTeam1Bo() {
		return moneyTeam1Bo;
	}
	public Double getMoneyTeam2Bo() {
		return moneyTeam2Bo;
	}
	public void setMoneyTeam1Bo(Double moneyTeam1Bo) {
		this.moneyTeam1Bo = moneyTeam1Bo;
	}
	public void setMoneyTeam2Bo(Double moneyTeam2Bo) {
		this.moneyTeam2Bo = moneyTeam2Bo;
	}
	public Integer getNumChange() {
		return numChange;
	}
	public Integer getNoMatch() {
		return noMatch;
	}
	public void setNumChange(Integer numChange) {
		this.numChange = numChange;
	}
	public void setNoMatch(Integer noMatch) {
		this.noMatch = noMatch;
	}
	public Double getUbdMoneyTeam1() {
		return ubdMoneyTeam1;
	}
	public Double getUbdMoneyTeam2() {
		return ubdMoneyTeam2;
	}
	public void setUbdMoneyTeam1(Double ubdMoneyTeam1) {
		this.ubdMoneyTeam1 = ubdMoneyTeam1;
	}
	public void setUbdMoneyTeam2(Double ubdMoneyTeam2) {
		this.ubdMoneyTeam2 = ubdMoneyTeam2;
	}
	public Integer getProfitTeam1() {
		return profitTeam1;
	}
	public Integer getProfitTeam2() {
		return profitTeam2;
	}
	public void setProfitTeam1(Integer profitTeam1) {
		this.profitTeam1 = profitTeam1;
	}
	public void setProfitTeam2(Integer profitTeam2) {
		this.profitTeam2 = profitTeam2;
	}
}
