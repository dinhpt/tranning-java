package com.staresport.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.staresport.bean.UserBetInfo;
import com.staresport.bean.UserBetJson;

public class UserBetDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String gameName;
	private String team1Name;
	private String team1Avatar;
	private String team2Name;
	private String team2Avatar;
	private Integer team1Id;
	private Integer team2Id;
	private Long gameId;
	private Integer matchId;
	private String allBets;
	private String allBetsHis;
	private Integer subMatchId;
	private Integer uhostedId;
	private Integer betTeamId;
	private String betMoney;
	private Integer userBetId;
	private String betBestof;
	private String allBetHistories;
	private List<UserBetJson> userBetJsons;
	private List<UserBetJson> groupUbetJsons;
	private List<UserBetJson> ubetHistrJsons;
	private List<UserBetInfo> lstUserBetJson = new ArrayList<>();

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public String getTeam1Name() {
		return team1Name;
	}

	public void setTeam1Name(String team1Name) {
		this.team1Name = team1Name;
	}

	public String getTeam1Avatar() {
		return team1Avatar;
	}

	public void setTeam1Avatar(String team1Avatar) {
		this.team1Avatar = team1Avatar;
	}

	public String getTeam2Name() {
		return team2Name;
	}

	public void setTeam2Name(String team2Name) {
		this.team2Name = team2Name;
	}

	public String getTeam2Avatar() {
		return team2Avatar;
	}

	public void setTeam2Avatar(String team2Avatar) {
		this.team2Avatar = team2Avatar;
	}

	public List<UserBetInfo> getLstUserBetJson() {
		return lstUserBetJson;
	}

	public void setLstUserBetJson(List<UserBetInfo> lstUserBetJson) {
		this.lstUserBetJson = lstUserBetJson;
	}

	public Integer getTeam1Id() {
		return team1Id;
	}

	public void setTeam1Id(Integer team1Id) {
		this.team1Id = team1Id;
	}

	public Integer getTeam2Id() {
		return team2Id;
	}

	public void setTeam2Id(Integer team2Id) {
		this.team2Id = team2Id;
	}

	public Long getGameId() {
		return gameId;
	}

	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}

	public String getAllBets() {
		return allBets;
	}

	public void setAllBets(String allBets) {
		this.allBets = allBets;
	}

	public Integer getMatchId() {
		return matchId;
	}

	public void setMatchId(Integer matchId) {
		this.matchId = matchId;
	}

	public Integer getSubMatchId() {
		return subMatchId;
	}

	public Integer getUhostedId() {
		return uhostedId;
	}

	public Integer getBetTeamId() {
		return betTeamId;
	}

	public void setSubMatchId(Integer subMatchId) {
		this.subMatchId = subMatchId;
	}

	public void setUhostedId(Integer uhostedId) {
		this.uhostedId = uhostedId;
	}

	public void setBetTeamId(Integer betTeamId) {
		this.betTeamId = betTeamId;
	}

	public String getBetMoney() {
		return betMoney;
	}

	public void setBetMoney(String betMoney) {
		this.betMoney = betMoney;
	}

	public Integer getUserBetId() {
		return userBetId;
	}

	public void setUserBetId(Integer userBetId) {
		this.userBetId = userBetId;
	}

	public List<UserBetJson> getUserBetJsons() {
		return userBetJsons;
	}

	public List<UserBetJson> getGroupUbetJsons() {
		return groupUbetJsons;
	}

	public String getBetBestof() {
		return betBestof;
	}

	public String getAllBetHistories() {
		return allBetHistories;
	}

	public void setBetBestof(String betBestof) {
		this.betBestof = betBestof;
	}

	public void setAllBetHistories(String allBetHistories) {
		this.allBetHistories = allBetHistories;
	}

	public List<UserBetJson> getUbetHistrJsons() {
		return ubetHistrJsons;
	}

	public void setUbetHistrJsons(List<UserBetJson> ubetHistrJsons) {
		this.ubetHistrJsons = ubetHistrJsons;
	}

	public void setGroupUbetJsons(List<UserBetJson> groupUbetJsons) {
		this.groupUbetJsons = groupUbetJsons;
	}

	public void setUserBetJsons(List<UserBetJson> userBetJsons) {
		this.userBetJsons = userBetJsons;
	}

	public String getAllBetsHis() {
		return allBetsHis;
	}

	public void setAllBetsHis(String allBetsHis) {
		this.allBetsHis = allBetsHis;
	}

}
