package com.staresport.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.staresport.bean.UserBetJson;
import com.staresport.entity.Streams;

public class MatchDto implements Serializable, Comparable<MatchDto> {
	private static final long serialVersionUID = 1L;
	private Integer matchId;
	private Integer userId;
	private String name;
	// INFO TEAM 1
	private Long team1Id;
	private String team1Name;
	private String team1Avatar;
	private String team1Flag;
	// INFO TEAM 2
	private Long team2Id;
	private String team2Name;
	private String team2Avatar;
	private String team2Flag;
	private Date dateTime;
	private String startTime;
	private Float score1Team;
	private Float score2Team;
	private Integer totalUserBet;
	private Float totalFee;
	private Float totalMoneyBet;
	private String link;
	private Integer totalUserShow;
	private Integer team1BetPer;
	private Integer team2BetPer;
	private Integer winResults;
	private Integer status;
	private Integer bestOf;
	private Integer gameType;
	private Integer matchType;
	private String tournament;
	private Integer numHosted;
	private Date createdAt;
	private Date updateAt;
	private Integer createdBy;
	private Integer updatedBy;
	private String userHosted;
	private String hostedAvar;
	private String gameName;
	private String gameShortName;
	private String gameAvar;
	private String gameImgStream;
	private int totalShowing;
	private int userType;
	private String linkStream;
	private boolean viewHost;
	private boolean isLive;
	private Long userHostedsId;
	private int viewStatus;
	private Double ubetMoneyTeam1;
	private Double ubetMoneyTeam2;
	private Double totalMoneyTeam1;
	private Double totalMoneyTeam2;
	private Double moneyTeam1Bo;
	private Double moneyTeam2Bo;
	private Double ubdMoneyTeam1;
	private Double ubdMoneyTeam2;
	private String moneyWinTeam1;
	private String moneyWinTeam2;
	
	// USER BET JSON
	private String betTime;
	private String betTeamId;
	private String betMoney;
	private String profit;
	// DAT KEO THEO CA BO HOAC DAT CHO MOI BO
	// SO LAN DAT CUOC
	private String numBet;
	// DOI KEO(0: KHONG DOI DEO; 1: DOI KEO)
	private String changed;
	// DAT CUOC LAN THU
	private String noBet;
	private String numChange;
	
	// INFO STREAM
	private Long streamId;
	private String lang;
	private String url;
	private List<Streams> streams = new ArrayList<>();
	// SUB MATCHS
	private Integer subMatchId;
	private Integer noMatch;
	private Integer betType;
	// USER HOSTED
	private Integer uhostedId;
	private String uhostedName;
	// USER BET
	private Integer userBetsId;
	private Integer userBetId;
	private short userBetResult;
	private String betBestof;
	private String allBets;
	private String allBetHistories;
	private List<SubMatchsDto> subMatchs;
	private List<UserBetJson> userBetJsons;
	private List<UserBetDetailDto> betDetailList;

	public Integer getMatchId() {
		return matchId;
	}

	public void setMatchId(Integer matchId) {
		this.matchId = matchId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getTeam1Id() {
		return team1Id;
	}

	public void setTeam1Id(Long team1Id) {
		this.team1Id = team1Id;
	}

	public String getTeam1Name() {
		return team1Name;
	}

	public void setTeam1Name(String team1Name) {
		this.team1Name = team1Name;
	}

	public String getTeam1Avatar() {
		return team1Avatar;
	}

	public void setTeam1Avatar(String team1Avatar) {
		this.team1Avatar = team1Avatar;
	}

	public String getTeam1Flag() {
		return team1Flag;
	}

	public void setTeam1Flag(String team1Flag) {
		this.team1Flag = team1Flag;
	}

	public Long getTeam2Id() {
		return team2Id;
	}

	public void setTeam2Id(Long team2Id) {
		this.team2Id = team2Id;
	}

	public String getTeam2Name() {
		return team2Name;
	}

	public void setTeam2Name(String team2Name) {
		this.team2Name = team2Name;
	}

	public String getTeam2Avatar() {
		return team2Avatar;
	}

	public void setTeam2Avatar(String team2Avatar) {
		this.team2Avatar = team2Avatar;
	}

	public String getTeam2Flag() {
		return team2Flag;
	}

	public void setTeam2Flag(String team2Flag) {
		this.team2Flag = team2Flag;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public Float getScore1Team() {
		return score1Team;
	}

	public void setScore1Team(Float score1Team) {
		this.score1Team = score1Team;
	}

	public Float getScore2Team() {
		return score2Team;
	}

	public void setScore2Team(Float score2Team) {
		this.score2Team = score2Team;
	}

	public Integer getTotalUserBet() {
		return totalUserBet;
	}

	public void setTotalUserBet(Integer totalUserBet) {
		this.totalUserBet = totalUserBet;
	}

	public Float getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(Float totalFee) {
		this.totalFee = totalFee;
	}

	public Float getTotalMoneyBet() {
		return totalMoneyBet;
	}

	public void setTotalMoneyBet(Float totalMoneyBet) {
		this.totalMoneyBet = totalMoneyBet;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Integer getTotalUserShow() {
		return totalUserShow;
	}

	public void setTotalUserShow(Integer totalUserShow) {
		this.totalUserShow = totalUserShow;
	}

	public Integer getTeam1BetPer() {
		return team1BetPer;
	}

	public void setTeam1BetPer(Integer team1BetPer) {
		this.team1BetPer = team1BetPer;
	}

	public Integer getTeam2BetPer() {
		return team2BetPer;
	}

	public void setTeam2BetPer(Integer team2BetPer) {
		this.team2BetPer = team2BetPer;
	}

	public Integer getWinResults() {
		return winResults;
	}

	public void setWinResults(Integer winResults) {
		this.winResults = winResults;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getBestOf() {
		return bestOf;
	}

	public void setBestOf(Integer bestOf) {
		this.bestOf = bestOf;
	}

	public Integer getGameType() {
		return gameType;
	}

	public void setGameType(Integer gameType) {
		this.gameType = gameType;
	}

	public Integer getMatchType() {
		return matchType;
	}

	public void setMatchType(Integer matchType) {
		this.matchType = matchType;
	}

	public String getTournament() {
		return tournament;
	}

	public void setTournament(String tournament) {
		this.tournament = tournament;
	}

	public Integer getNumHosted() {
		return numHosted;
	}

	public void setNumHosted(Integer numHosted) {
		this.numHosted = numHosted;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUserHosted() {
		return userHosted;
	}

	public void setUserHosted(String userHosted) {
		this.userHosted = userHosted;
	}

	public String getHostedAvar() {
		return hostedAvar;
	}

	public void setHostedAvar(String hostedAvar) {
		this.hostedAvar = hostedAvar;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public String getGameShortName() {
		return gameShortName;
	}

	public void setGameShortName(String gameShortName) {
		this.gameShortName = gameShortName;
	}

	public String getGameAvar() {
		return gameAvar;
	}

	public void setGameAvar(String gameAvar) {
		this.gameAvar = gameAvar;
	}

	public String getGameImgStream() {
		return gameImgStream;
	}

	public void setGameImgStream(String gameImgStream) {
		this.gameImgStream = gameImgStream;
	}

	public int getTotalShowing() {
		return totalShowing;
	}

	public void setTotalShowing(int totalShowing) {
		this.totalShowing = totalShowing;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public boolean isViewHost() {
		return viewHost;
	}

	public void setViewHost(boolean viewHost) {
		this.viewHost = viewHost;
	}

	public boolean isLive() {
		return isLive;
	}

	public void setLive(boolean isLive) {
		this.isLive = isLive;
	}

	public String getLinkStream() {
		return linkStream;
	}

	public void setLinkStream(String linkStream) {
		this.linkStream = linkStream;
	}

	public Long getStreamId() {
		return streamId;
	}

	public void setStreamId(Long streamId) {
		this.streamId = streamId;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<Streams> getStreams() {
		return streams;
	}

	public void setStreams(List<Streams> streams) {
		this.streams = streams;
	}

	@Override
	public int compareTo(MatchDto o) {
		if (this.getDateTime() != null && o.getDateTime() != null && this.getDateTime().before(o.getDateTime())) {
			return 1;
		}
		return -1;
	}

	public Integer getSubMatchId() {
		return subMatchId;
	}

	public void setSubMatchId(Integer subMatchId) {
		this.subMatchId = subMatchId;
	}

	public Integer getBetType() {
		return betType;
	}

	public void setBetType(Integer betType) {
		this.betType = betType;
	}

	public Integer getNoMatch() {
		return noMatch;
	}

	public void setNoMatch(Integer noMatch) {
		this.noMatch = noMatch;
	}

	public List<SubMatchsDto> getSubMatchs() {
		return subMatchs;
	}

	public void setSubMatchs(List<SubMatchsDto> subMatchs) {
		this.subMatchs = subMatchs;
	}

	public Long getUserHostedsId() {
		return userHostedsId;
	}

	public void setUserHostedsId(Long userHostedsId) {
		this.userHostedsId = userHostedsId;
	}

	public List<UserBetJson> getUserBetJsons() {
		return userBetJsons;
	}

	public void setUserBetJsons(List<UserBetJson> userBetJsons) {
		this.userBetJsons = userBetJsons;
	}

	public Integer getUserBetsId() {
		return userBetsId;
	}

	public Integer getUserBetId() {
		return userBetId;
	}

	public short getUserBetResult() {
		return userBetResult;
	}

	public String getAllBets() {
		return allBets;
	}
	public void setUserBetsId(Integer userBetsId) {
		this.userBetsId = userBetsId;
	}

	public void setUserBetId(Integer userBetId) {
		this.userBetId = userBetId;
	}

	public void setUserBetResult(short userBetResult) {
		this.userBetResult = userBetResult;
	}

	public void setAllBets(String allBets) {
		this.allBets = allBets;
	}

	public Integer getUhostedId() {
		return uhostedId;
	}

	public String getUhostedName() {
		return uhostedName;
	}

	public void setUhostedId(Integer uhostedId) {
		this.uhostedId = uhostedId;
	}

	public void setUhostedName(String uhostedName) {
		this.uhostedName = uhostedName;
	}

	public int getViewStatus() {
		return viewStatus;
	}

	public void setViewStatus(int viewStatus) {
		this.viewStatus = viewStatus;
	}

	public String getAllBetHistories() {
		return allBetHistories;
	}

	public void setAllBetHistories(String allBetHistories) {
		this.allBetHistories = allBetHistories;
	}

	public String getBetBestof() {
		return betBestof;
	}

	public void setBetBestof(String betBestof) {
		this.betBestof = betBestof;
	}

	public Double getUbetMoneyTeam1() {
		return ubetMoneyTeam1;
	}

	public Double getUbetMoneyTeam2() {
		return ubetMoneyTeam2;
	}

	public Double getTotalMoneyTeam1() {
		return totalMoneyTeam1;
	}

	public Double getTotalMoneyTeam2() {
		return totalMoneyTeam2;
	}

	public void setUbetMoneyTeam1(Double ubetMoneyTeam1) {
		this.ubetMoneyTeam1 = ubetMoneyTeam1;
	}

	public void setUbetMoneyTeam2(Double ubetMoneyTeam2) {
		this.ubetMoneyTeam2 = ubetMoneyTeam2;
	}

	public void setTotalMoneyTeam1(Double totalMoneyTeam1) {
		this.totalMoneyTeam1 = totalMoneyTeam1;
	}

	public void setTotalMoneyTeam2(Double totalMoneyTeam2) {
		this.totalMoneyTeam2 = totalMoneyTeam2;
	}

	public Double getMoneyTeam1Bo() {
		return moneyTeam1Bo;
	}

	public void setMoneyTeam1Bo(Double moneyTeam1Bo) {
		this.moneyTeam1Bo = moneyTeam1Bo;
	}

	public Double getMoneyTeam2Bo() {
		return moneyTeam2Bo;
	}

	public void setMoneyTeam2Bo(Double moneyTeam2Bo) {
		this.moneyTeam2Bo = moneyTeam2Bo;
	}

	public String getBetTime() {
		return betTime;
	}

	public void setBetTime(String betTime) {
		this.betTime = betTime;
	}

	public String getBetTeamId() {
		return betTeamId;
	}

	public void setBetTeamId(String betTeamId) {
		this.betTeamId = betTeamId;
	}

	public String getBetMoney() {
		return betMoney;
	}

	public void setBetMoney(String betMoney) {
		this.betMoney = betMoney;
	}

	public String getProfit() {
		return profit;
	}

	public void setProfit(String profit) {
		this.profit = profit;
	}

	public String getNumBet() {
		return numBet;
	}

	public void setNumBet(String numBet) {
		this.numBet = numBet;
	}

	public String getChanged() {
		return changed;
	}

	public void setChanged(String changed) {
		this.changed = changed;
	}

	public String getNoBet() {
		return noBet;
	}

	public void setNoBet(String noBet) {
		this.noBet = noBet;
	}

	public String getNumChange() {
		return numChange;
	}

	public void setNumChange(String numChange) {
		this.numChange = numChange;
	}

	public Double getUbdMoneyTeam1() {
		return ubdMoneyTeam1;
	}

	public Double getUbdMoneyTeam2() {
		return ubdMoneyTeam2;
	}

	public void setUbdMoneyTeam1(Double ubdMoneyTeam1) {
		this.ubdMoneyTeam1 = ubdMoneyTeam1;
	}

	public void setUbdMoneyTeam2(Double ubdMoneyTeam2) {
		this.ubdMoneyTeam2 = ubdMoneyTeam2;
	}

	public List<UserBetDetailDto> getBetDetailList() {
		return betDetailList;
	}

	public String getMoneyWinTeam1() {
		return moneyWinTeam1;
	}

	public void setMoneyWinTeam1(String moneyWinTeam1) {
		this.moneyWinTeam1 = moneyWinTeam1;
	}

	public String getMoneyWinTeam2() {
		return moneyWinTeam2;
	}

	public void setMoneyWinTeam2(String moneyWinTeam2) {
		this.moneyWinTeam2 = moneyWinTeam2;
	}

	public void setBetDetailList(List<UserBetDetailDto> betDetailList) {
		this.betDetailList = betDetailList;
	}
	
}
