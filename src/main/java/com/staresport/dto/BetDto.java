package com.staresport.dto;

public class BetDto {
	private boolean isBetFirst;
	private String moneyBetBo;
	private String moneyWinTeam1;
	private String moneyWinTeam2;
	private Double ubetMoneyTeam1;
	private Double ubetMoneyTeam2;
	private Double totalMoneyTeam1;
	private Double totalMoneyTeam2;
	// PERCENTAGE 
	private String pctgTeam1;
	private String pctgTeam2;
	// RATE
	private Double rateTeam1;
	private Double rateTeam2;
	// PROFIT
	private Integer profitTeam1;
	private Integer profitTeam2;
	public String getMoneyBetBo() {
		return moneyBetBo;
	}
	public void setMoneyBetBo(String moneyBetBo) {
		this.moneyBetBo = moneyBetBo;
	}
	public String getMoneyWinTeam1() {
		return moneyWinTeam1;
	}
	public void setMoneyWinTeam1(String moneyWinTeam1) {
		this.moneyWinTeam1 = moneyWinTeam1;
	}
	public String getMoneyWinTeam2() {
		return moneyWinTeam2;
	}
	public void setMoneyWinTeam2(String moneyWinTeam2) {
		this.moneyWinTeam2 = moneyWinTeam2;
	}
	public Double getUbetMoneyTeam1() {
		return ubetMoneyTeam1;
	}
	public void setUbetMoneyTeam1(Double ubetMoneyTeam1) {
		this.ubetMoneyTeam1 = ubetMoneyTeam1;
	}
	public Double getUbetMoneyTeam2() {
		return ubetMoneyTeam2;
	}
	public void setUbetMoneyTeam2(Double ubetMoneyTeam2) {
		this.ubetMoneyTeam2 = ubetMoneyTeam2;
	}
	public Double getTotalMoneyTeam1() {
		return totalMoneyTeam1;
	}
	public void setTotalMoneyTeam1(Double totalMoneyTeam1) {
		this.totalMoneyTeam1 = totalMoneyTeam1;
	}
	public Double getTotalMoneyTeam2() {
		return totalMoneyTeam2;
	}
	public void setTotalMoneyTeam2(Double totalMoneyTeam2) {
		this.totalMoneyTeam2 = totalMoneyTeam2;
	}
	public boolean isBetFirst() {
		return isBetFirst;
	}
	public void setBetFirst(boolean isBetFirst) {
		this.isBetFirst = isBetFirst;
	}
	public String getPctgTeam1() {
		return pctgTeam1;
	}
	public String getPctgTeam2() {
		return pctgTeam2;
	}
	public Double getRateTeam1() {
		return rateTeam1;
	}
	public Double getRateTeam2() {
		return rateTeam2;
	}
	public void setPctgTeam1(String pctgTeam1) {
		this.pctgTeam1 = pctgTeam1;
	}
	public void setPctgTeam2(String pctgTeam2) {
		this.pctgTeam2 = pctgTeam2;
	}
	public void setRateTeam1(Double rateTeam1) {
		this.rateTeam1 = rateTeam1;
	}
	public void setRateTeam2(Double rateTeam2) {
		this.rateTeam2 = rateTeam2;
	}
	public Integer getProfitTeam1() {
		return profitTeam1;
	}
	public Integer getProfitTeam2() {
		return profitTeam2;
	}
	public void setProfitTeam1(Integer profitTeam1) {
		this.profitTeam1 = profitTeam1;
	}
	public void setProfitTeam2(Integer profitTeam2) {
		this.profitTeam2 = profitTeam2;
	}
}
