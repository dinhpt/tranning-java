package com.staresport.dto;

import java.util.List;

public class MatchsDto {
	private String nameGame;
	private List<MatchDto> matchDtos;
	private Integer gameType;

	public Integer getGameType() {
		return gameType;
	}

	public void setGameType(Integer gameType) {
		this.gameType = gameType;
	}

	public String getNameGame() {
		return nameGame;
	}

	public void setNameGame(String nameGame) {
		this.nameGame = nameGame;
	}

	public List<MatchDto> getMatchDtos() {
		return matchDtos;
	}

	public void setMatchDtos(List<MatchDto> matchDtos) {
		this.matchDtos = matchDtos;
	}

}
