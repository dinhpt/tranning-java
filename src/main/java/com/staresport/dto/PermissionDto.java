package com.staresport.dto;

import java.io.Serializable;

public class PermissionDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean viewMatch;
	private boolean viewHistory;
	private boolean viewNotiEndMatch;
	
	public boolean isViewMatch() {
		return viewMatch;
	}
	public void setViewMatch(boolean viewMatch) {
		this.viewMatch = viewMatch;
	}
	public boolean isViewHistory() {
		return viewHistory;
	}
	public void setViewHistory(boolean viewHistory) {
		this.viewHistory = viewHistory;
	}
	public boolean isViewNotiEndMatch() {
		return viewNotiEndMatch;
	}
	public void setViewNotiEndMatch(boolean viewNotiEndMatch) {
		this.viewNotiEndMatch = viewNotiEndMatch;
	}
	
	
}
