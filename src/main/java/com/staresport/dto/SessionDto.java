package com.staresport.dto;

import java.io.Serializable;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

/**
 * Session DTO<br>
 *
 * @author dinhpt
 */
@Component(instance = InstanceType.SESSION)
public class SessionDto implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -4550636823432304517L;
	
	public UserDto user;
	
	/**
	 * Clear login session
	 */
	public void clear() {
		user = null;
	}
}