package com.staresport.dto;

import java.io.Serializable;

public class UserHostedDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long userHostedId;
	private Integer userId;
	private Long matchId;
	private String stream;
	private String code;
	private Integer totalUserBet;
	private Float totalFee;
	private Float totalMoneyBet;
	private Integer timeHosted;
	private Integer matchStatus;
	private Integer bestOf;
	private String matchName;
	private String tournament;
	private String lang;
	private Integer totalShowing;
	private String dateTime;

	public Long getUserHostedId() {
		return userHostedId;
	}

	public void setUserHostedId(Long userHostedId) {
		this.userHostedId = userHostedId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public String getStream() {
		return stream;
	}

	public void setStream(String stream) {
		this.stream = stream;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getTotalUserBet() {
		return totalUserBet;
	}

	public void setTotalUserBet(Integer totalUserBet) {
		this.totalUserBet = totalUserBet;
	}

	public Float getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(Float totalFee) {
		this.totalFee = totalFee;
	}

	public Float getTotalMoneyBet() {
		return totalMoneyBet;
	}

	public void setTotalMoneyBet(Float totalMoneyBet) {
		this.totalMoneyBet = totalMoneyBet;
	}

	public Integer getTimeHosted() {
		return timeHosted;
	}

	public void setTimeHosted(Integer timeHosted) {
		this.timeHosted = timeHosted;
	}

	public Integer getMatchStatus() {
		return matchStatus;
	}

	public void setMatchStatus(Integer matchStatus) {
		this.matchStatus = matchStatus;
	}

	public Integer getBestOf() {
		return bestOf;
	}

	public void setBestOf(Integer bestOf) {
		this.bestOf = bestOf;
	}

	public String getMatchName() {
		return matchName;
	}

	public void setMatchName(String matchName) {
		this.matchName = matchName;
	}

	public String getTournament() {
		return tournament;
	}

	public void setTournament(String tournament) {
		this.tournament = tournament;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public Integer getTotalShowing() {
		return totalShowing;
	}

	public void setTotalShowing(Integer totalShowing) {
		this.totalShowing = totalShowing;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

}
