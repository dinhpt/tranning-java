package com.staresport.interceptor;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.aopalliance.intercept.MethodInvocation;
import org.seasar.framework.aop.interceptors.AbstractInterceptor;

import com.staresport.dto.PermissionDto;
import com.staresport.dto.SessionDto;
import com.staresport.dto.UserDto;
import com.staresport.utils.AppConstants;
import com.staresport.utils.SessionUtil;
import com.staresport.utils.SiteUrlUtil;

/**
 * Action Intercepter<br>
 *
 * @author BSSD dinhpt
 */
public class ActionInterceptor extends AbstractInterceptor {
	private static final long serialVersionUID = 5653913792816199119L;
	
	private final String LOGIN = SiteUrlUtil.getIndexUrl() + "/login?redirect=true";
	private final String HOME = SiteUrlUtil.getIndexUrl() + "?redirect=true";
	private Integer countRequest=0;

	@Resource
	protected HttpServletRequest httpServletRequest;

	@Resource
	protected SessionDto sessionDto;
	
	@Resource
	protected HttpSession session;
	
	// PERMISSION
	private boolean viewMatch;
	private boolean viewHistory;
	private boolean viewNotiEndMatch;

	@Override
	public Object invoke(final MethodInvocation invocation) throws Throwable {
		Object result = null;
		String action = invocation.getMethod().getDeclaringClass().getSimpleName();
		UserDto user = sessionDto.user;
		if(user != null){
			PermissionDto permission = user.getPermission();
			viewMatch = permission.isViewMatch();
			viewHistory = permission.isViewHistory();
			viewNotiEndMatch = permission.isViewNotiEndMatch();
		}
		if(AppConstants.ACTION.LOGIN_ACTION.equalsIgnoreCase(action)){
			if(user == null){
				Integer count = SessionUtil.getCountRequest(session);
				if(count == null){
					countRequest++;
					SessionUtil.setCountRequest(session, countRequest);
					return LOGIN;
				}
			}else{
				return HOME;
			}
		}
		else if(AppConstants.ACTION.MATCH_ACTION.equalsIgnoreCase(action)){
			if(user == null || !viewMatch) return LOGIN;
		}
		result = invocation.proceed();
		return result;
	}

}
