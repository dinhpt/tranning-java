package com.staresport.approval;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.log4j.Logger;

public class SendUserBetApproval extends DatabaseProcessor{

	protected Logger logger = Logger.getLogger("SYNC FROM GOSUGAMER");
	private static final int INT_FETCH_ROW = 5000; 
	/**
	 * 
	 * @param intMatchId match Id want to approval
	 * @throws Exception
	 * 
	 */
	protected void ApprovalUserBetForMatch(int  intApprovalMatchId) throws Exception {
		if(intApprovalMatchId<0)
			return;
		Connection objConn = new DatabaseProcessor().getConnectionSQL();
    	try {
    		//get connection
    		
    		if(objConn == null){
    			logger.info("------CONNECT TO DB FAILED-------");
    			return;
    		}
    	
    		Statement objStatement = objConn.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
  	              java.sql.ResultSet.CONCUR_READ_ONLY);
    		objStatement.setFetchSize(Integer.MIN_VALUE);
    		String    strSqlSelect = "select ub.user_id,ub.match_id,ub.uhosted_id,ub.profit from user_bets ub  where ub.matchId = " +intApprovalMatchId;
    		ResultSet objRs=objStatement.executeQuery(strSqlSelect);
    		int intCnt=0;
    		
    		while(objRs.next()) {
    			objConn.setAutoCommit(false);
    			int        intUserId     = objRs.getInt("user_id");
    			int        intMatchId    = objRs.getInt("match_id");
    			int        intHostId     = objRs.getInt("uhosted_id");
    			BigDecimal decProfit     = objRs.getBigDecimal("profit"); 
    			
    			if(intCnt>INT_FETCH_ROW){
    				String  strSqlUpdateRow="Update user_bets ub where set ub.status=1 where user_id=? And match_id=? And uhosted_id=? ";
    				/* update status =1(đã duyệt bên vqs) vào lại database */
    				PreparedStatement objPstmt = objConn.prepareStatement(strSqlUpdateRow);
    				objPstmt.setInt(1, intUserId);
    				objPstmt.setInt(2, intMatchId);
    				objPstmt.setInt(3, intHostId);
    				objPstmt.addBatch();
    				objPstmt.executeBatch();
    			}
    			
    			objConn.setAutoCommit(true);
    			
    			}
		} catch (Exception e) {
			logger.info("----------------- ERROR PROCESS-----------------------");
		}
    	finally{
    		
			closeObject(objConn);
			
		}
    }
	
	
    
   
}
