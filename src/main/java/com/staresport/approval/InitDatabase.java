package com.staresport.approval;

import java.sql.Connection;
import java.sql.DriverManager;

public class InitDatabase {
	private static Connection con = null;
    private static String dbServer = "jdbc:mysql://10.22.10.3:3306/bssd_esport";
    private static String userName = "bssd";
    private static String password = "bssd@123";
    private static InitDatabase instance = null;
    
    private InitDatabase() throws Exception {
    	getConnectionDatabase();
    }

    public static InitDatabase getInstance() throws Exception {
        if (instance == null) {
        	instance = new InitDatabase();
        }
        return instance;
    }
    
    public static Connection getConnection() throws Exception {
        return InitDatabase.getConnectionDatabase();
    }
    
    public synchronized static java.sql.Connection getConnectionDatabase() {
        try {
        	Class.forName("com.mysql.jdbc.Driver").newInstance();
            con = DriverManager.getConnection(dbServer, userName, password);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error Trace in getConnection() : " + e.getMessage());
        }
        return con;
    }
    
    public synchronized static void closeObject(Object obj) {
        try {
            if (obj instanceof java.sql.Connection) {
                if (!((java.sql.Connection) obj).isClosed()) {
                    if (!((java.sql.Connection) obj).getAutoCommit()) {
                        ((java.sql.Connection) obj).rollback();
                    }
                    ((java.sql.Connection) obj).close();
                }
            } else if (obj instanceof java.sql.CallableStatement) {
                ((java.sql.CallableStatement) obj).close();
            } else if (obj instanceof java.sql.PreparedStatement) {
                ((java.sql.PreparedStatement) obj).close();
            } else if (obj instanceof java.sql.Statement) {
                ((java.sql.Statement) obj).close();
            } else if (obj instanceof java.sql.ResultSet) {
                ((java.sql.ResultSet) obj).close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
