package com.staresport.approval;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.log4j.Logger;

public class DatabaseProcessor {
	protected Connection connection;
	protected Logger logger = Logger.getLogger(DatabaseProcessor.class);
	
	private static DatabaseProcessor instance = null;

    public static DatabaseProcessor getInstance() throws Exception {
        if (instance == null) {
        	instance = new DatabaseProcessor();
        }
        return instance;
    }
    
    public synchronized Connection getConnectionSQL() throws Exception {
    	int timeout = 0;
        int maxTimeout = 5;
        Connection conn = null;
        while (timeout < maxTimeout) {
        	 try {
                 if (connection != null && !connection.isClosed()) {
                     conn = connection;
                 } else {
                	 connection = InitDatabase.getConnection();
                     conn = connection;
                 }
             } catch (Exception ex) {
                 logger.error("Error: " + ex.getMessage());
             }
             timeout++;
             if (conn != null) {
                 break;
             } else {
                 logger.info("Connection to DB failed");
             }
        }
        return conn;
    }
    
    public synchronized static void closeObject(Connection obj) throws Exception {
        try {
            if (obj != null) {
                if (!obj.isClosed()) {
                    if (!obj.getAutoCommit()) {
                        obj.rollback();
                    }
                    obj.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void closeObject(CallableStatement obj) {
        try {
            if (obj != null) {
                obj.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void closeObject(Statement obj) {
        try {
            if (obj != null) {
                obj.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void closeObject(ResultSet obj) {
        try {
            if (obj != null) {
                obj.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void closeObject(PreparedStatement obj) {
        try {
            if (obj != null) {
                obj.clearBatch();
                obj.clearParameters();
                obj.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
