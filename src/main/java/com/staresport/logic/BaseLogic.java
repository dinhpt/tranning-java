package com.staresport.logic;

import java.util.List;

public interface BaseLogic {
	
	public <T> T insert(T obj, Integer createdBy);
	
	public <T> List<T> insert(List<T> objList, Integer createdBy);
	
	public <T> boolean insertBatch(List<T> objList);
	
	public <T> T update(T obj, Integer updatedBy);
	
	public <T> List<T> update(List<T> objList, Integer updatedBy);
	
	public <T> boolean updateBatch(List<T> objList);
	
	public <T> int delete(Class<T> c, Object key);
	
	public <T> void delete(List<T> objList);
	
	public <T> int deleteByIds(Class<T> c, String idField, String[] ids);
	
	public <T> T findById(Class<T> c, Object key);
	
	public <T> List<T> findByProperty(Class<T> c, String idField, Object value, String orderBy);
	
	public <T> List<T> findAll(Class<T> c, String... orderByArgs);
}
