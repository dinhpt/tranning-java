package com.staresport.logic;

import com.staresport.dto.UserHostedDto;
import com.staresport.entity.UserHosteds;

public interface UserHostedsLogic {

	public UserHosteds getById(Long userHostedsId);

	public UserHostedDto getUserHostedInfo(Long userHostedId);
}
