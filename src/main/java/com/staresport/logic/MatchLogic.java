package com.staresport.logic;

import java.util.List;

import com.staresport.dto.MatchDto;

public interface MatchLogic {
	public List<MatchDto> getMatchList(int gameType, Integer userId);

	public List<MatchDto> getMatchListLive(int gameType, int matchStatus, String searchKey);

	public List<MatchDto> getMostViewingMatch();

	public List<MatchDto> getMatchComing(int hours, int gameType, String searchMatch);

	public List<MatchDto> getAdminStream();

	public boolean getNumHostedOfMatch(Long matchId);

	public boolean updateNumHostedOfMatch(Long matchId);

	public boolean isUserHosted(Integer userId, Long matchId);

	public MatchDto getInfoMatch(Long matchId);
}
