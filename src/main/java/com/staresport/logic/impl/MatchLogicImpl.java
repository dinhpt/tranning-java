package com.staresport.logic.impl;

import java.util.List;

import javax.annotation.Resource;

import com.staresport.dto.MatchDto;
import com.staresport.logic.MatchLogic;
import com.staresport.service.MatchService;

public class MatchLogicImpl implements MatchLogic {
	@Resource
	private MatchService matchService;

	public List<MatchDto> getMatchList(int gameType, Integer userId) {
		return matchService.getMatchList(gameType, userId);
	}

	@Override
	public List<MatchDto> getMatchListLive(int gameType, int matchStatus, String searchKey) {
		return matchService.getMatchListLive(gameType, matchStatus, searchKey);
	}

	@Override
	public List<MatchDto> getAdminStream() {
		return matchService.getAdminStream();
	}

	public boolean getNumHostedOfMatch(Long matchId) {
		return matchService.getNumHostedOfMatch(matchId);
	}

	public boolean updateNumHostedOfMatch(Long matchId) {
		return matchService.updateNumHostedOfMatch(matchId);
	}

	public boolean isUserHosted(Integer userId, Long matchId) {
		return matchService.isUserHosted(userId, matchId);
	}

	@Override
	public List<MatchDto> getMostViewingMatch() {
		return matchService.getMostViewingMatch();
	}

	@Override
	public List<MatchDto> getMatchComing(int hours, int gameType, String searchMatch) {
		return matchService.getMatchComing(hours, gameType, searchMatch);
	}

	public MatchDto getInfoMatch(Long matchId) {
		return matchService.getInfoMatch(matchId);
	}
}
