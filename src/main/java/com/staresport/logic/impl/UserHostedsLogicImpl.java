package com.staresport.logic.impl;

import javax.annotation.Resource;

import com.staresport.dto.UserHostedDto;
import com.staresport.entity.UserHosteds;
import com.staresport.logic.UserHostedsLogic;
import com.staresport.service.UserHostedsService;

public class UserHostedsLogicImpl implements UserHostedsLogic {

	@Resource
	protected UserHostedsService userHostedsService;

	@Override
	public UserHosteds getById(Long userHostedsId) {
		return userHostedsService.getById(userHostedsId);
	}

	@Override
	public UserHostedDto getUserHostedInfo(Long userHostedId) {
		return userHostedsService.getUserHostedInfo(userHostedId);
	}

}
