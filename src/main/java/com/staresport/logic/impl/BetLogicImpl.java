package com.staresport.logic.impl;

import java.util.List;

import javax.annotation.Resource;

import com.staresport.bean.UserBetJson;
import com.staresport.dto.MatchDto;
import com.staresport.dto.SubMatchsDto;
import com.staresport.dto.UserBetDto;
import com.staresport.entity.UserBetDetail;
import com.staresport.entity.UserBets;
import com.staresport.logic.BetLogic;
import com.staresport.service.BetService;

public class BetLogicImpl implements BetLogic{
	@Resource
	private BetService betService;
	public MatchDto getMatchById(Long matchId){
		return betService.getMatchById(matchId);
	}
	
	public boolean validMatch(Integer matchId, Integer subMatchId){
		return betService.validMatch(matchId, subMatchId);
	}
	
	public List<UserBetJson> getBetUser(Integer userId, Integer matchId, Integer uhostedId){
		return betService.getBetUser(userId, matchId, uhostedId);
	}
	
	public UserBetDto getAllBetUser(Integer userId, Integer matchId, Integer uhostedId){
		return betService.getAllBetUser(userId, matchId, uhostedId);
	}

	@Override
	public List<UserBetDto> getUserBetHistory(Integer userId) {
		return betService.getUserBetHistory(userId);
	}
	
	public MatchDto getMatchUserBet(Integer matchId, Integer userHostedId, Integer userBetId){
		return betService.getMatchUserBet(matchId, userHostedId, userBetId);
	}
	
	public boolean isRemainTimeBet(Integer matchId){
		return betService.isRemainTimeBet(matchId);
	}
	
	public List<SubMatchsDto> getSubMatch(Integer matchId){
		return betService.getSubMatch(matchId);
	}
	
	public boolean isUserBeted(Integer matchId, Integer userHostedId, Integer userBetId){
		return betService.isUserBeted(matchId, userHostedId, userBetId);
	}
	
	public MatchDto loadDataBet(UserBetDto userBet, Integer userId){
		return betService.loadDataBet(userBet, userId);
	}
	
	public boolean insert(UserBets userBets, List<UserBetDetail> userBetDetails) {
		return betService.insert(userBets, userBetDetails);
	}
	
	public boolean update(UserBets userBets, List<UserBetDetail> userBetDetails){
		return betService.update(userBets, userBetDetails);
	}
}
