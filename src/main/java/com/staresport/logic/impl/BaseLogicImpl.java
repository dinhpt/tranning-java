package com.staresport.logic.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.staresport.logic.BaseLogic;
import com.staresport.service.BaseService;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class BaseLogicImpl implements BaseLogic{
	private Logger LOGGER = Logger.getLogger(this.getClass());
	@Resource
	protected BaseService baseService;
	
	public <T> T insert(T obj, Integer createdBy){
		try {
			 return (T) baseService.insert(obj, createdBy);
		} catch (Exception e) {
			LOGGER.info(e);
			return null;
		}
	}
	
	public <T> List<T> insert(List<T> objList, Integer createdBy){
		try {
			return baseService.insert(objList, createdBy);
		} catch (Exception e) {
			LOGGER.info(e);
			return null;
		}
	}
	
	public <T> boolean insertBatch(List<T> objList){
		return baseService.insertBatch(objList);
	}
	
	public <T> T update(T obj, Integer updatedBy){
		try {
			return (T) baseService.update(obj, updatedBy);
		} catch (Exception e) {
			LOGGER.info(e);
			return null;
		}
		
	}
	
	public <T> List<T> update(List<T> objList, Integer updatedBy){
		try {
			return baseService.update(objList, updatedBy);
		} catch (Exception e) {
			LOGGER.info(e);
			return null;
		}
	}
	
	public <T> boolean updateBatch(List<T> objList) {
		return baseService.updateBatch(objList);
	}
	
	public <T> int delete(Class<T> c, Object key){
		try {
			return baseService.delete(c, key);
		} catch (Exception e) {
			LOGGER.info(e);
			return 0;
		}
	}
	
	public <T> void delete(List<T> objList){
		try {
			baseService.delete(objList);
		} catch (Exception e) {
			LOGGER.info(e);
		}
	}
	
	public <T> int deleteByIds(Class<T> c, String idField, String[] ids){
		try {
			return baseService.deleteByIds(c, idField, ids);
		} catch (Exception e) {
			LOGGER.info(e);
		}
		return 0;
	}
	
	public <T> T findById(Class<T> c, Object key){
		return (T) baseService.findById(c, key);
	}
	
	public <T> List<T> findByProperty(Class<T> c, String idField, Object value,
            String orderBy){
		return baseService.findByProperty(c, idField, value, orderBy);
	}
	
	public <T> List<T> findAll(Class<T> c, String... orderByArgs){
		return baseService.findAll(c, orderByArgs);
	}
}
