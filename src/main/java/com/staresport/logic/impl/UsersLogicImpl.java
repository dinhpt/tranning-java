package com.staresport.logic.impl;

import javax.annotation.Resource;

import com.staresport.dto.UserDto;
import com.staresport.logic.UsersLogic;
import com.staresport.service.UsersService;

public class UsersLogicImpl implements UsersLogic{
	@Resource
	private UsersService usersService;
	
	public UserDto authen(String username, String password){
		return usersService.authen(username, password);
	}
}
