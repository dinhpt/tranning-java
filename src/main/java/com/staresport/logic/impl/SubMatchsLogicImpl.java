package com.staresport.logic.impl;

import javax.annotation.Resource;

import com.staresport.dto.SubMatchsDto;
import com.staresport.logic.SubMatchsLogic;
import com.staresport.service.SubMatchsService;

public class SubMatchsLogicImpl implements SubMatchsLogic {

	@Resource
	private SubMatchsService subMatchsService;

	@Override
	public SubMatchsDto getById(Integer subMatchId, Integer matchId) {
		return subMatchsService.getById(subMatchId, matchId);
	}

}
