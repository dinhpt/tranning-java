package com.staresport.logic.impl;

import java.util.List;

import javax.annotation.Resource;

import com.staresport.entity.GameTypes;
import com.staresport.logic.GameTypesLogic;
import com.staresport.service.GameTypesService;

public class GameTypesLogicImpl implements GameTypesLogic {

	@Resource
	protected GameTypesService gameTypesService;

	@Override
	public List<GameTypes> getAllTypes() {
		return gameTypesService.getAllTypes();
	}

	@Override
	public GameTypes getById(int gameId) {
		return gameTypesService.getById(gameId);
	}

}
