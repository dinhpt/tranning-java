package com.staresport.logic;

import java.util.List;

import com.staresport.entity.GameTypes;

public interface GameTypesLogic {
	public List<GameTypes> getAllTypes();

	public GameTypes getById(int gameId);
}
