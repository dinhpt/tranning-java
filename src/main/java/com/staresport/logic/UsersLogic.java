package com.staresport.logic;

import com.staresport.dto.UserDto;

public interface UsersLogic {
	public UserDto authen(String username, String password);
}
