package com.staresport.logic;

import java.util.List;

import com.staresport.bean.UserBetJson;
import com.staresport.dto.MatchDto;
import com.staresport.dto.SubMatchsDto;
import com.staresport.dto.UserBetDto;
import com.staresport.entity.UserBetDetail;
import com.staresport.entity.UserBets;

public interface BetLogic {
	public MatchDto getMatchById(Long matchId);

	public boolean validMatch(Integer matchId, Integer subMatchId);

	public List<UserBetJson> getBetUser(Integer userId, Integer matchId, Integer uhostedId);
	
	public UserBetDto getAllBetUser(Integer userId, Integer matchId, Integer uhostedId);

	public List<UserBetDto> getUserBetHistory(Integer userId);
	
	public MatchDto getMatchUserBet(Integer matchId, Integer userHostedId, Integer userBetId);
	
	public boolean isRemainTimeBet(Integer matchId);
	
	public List<SubMatchsDto> getSubMatch(Integer matchId);
	
	public boolean isUserBeted(Integer matchId, Integer userHostedId, Integer userBetId);
	
	public MatchDto loadDataBet(UserBetDto userBet, Integer userId);
	
	public boolean insert(UserBets userBets, List<UserBetDetail> userBetDetails);
	
	public boolean update(UserBets userBets, List<UserBetDetail> userBetDetails);
}
