package com.staresport.logic;

import com.staresport.dto.SubMatchsDto;

public interface SubMatchsLogic {
	public SubMatchsDto getById(Integer subMatchId, Integer matchId);
}
