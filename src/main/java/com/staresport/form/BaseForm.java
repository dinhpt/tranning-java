package com.staresport.form;

import java.util.ArrayList;
import java.util.List;

import com.staresport.dto.MatchDto;
import com.staresport.entity.GameTypes;

public class BaseForm {
	protected List<GameTypes> lstGameType = new ArrayList<>();
	protected List<MatchDto> lstMostViewing = new ArrayList<>();

	public List<GameTypes> getLstGameType() {
		return lstGameType;
	}

	public void setLstGameType(List<GameTypes> lstGameType) {
		this.lstGameType = lstGameType;
	}

	public List<MatchDto> getLstMostViewing() {
		return lstMostViewing;
	}

	public void setLstMostViewing(List<MatchDto> lstMostViewing) {
		this.lstMostViewing = lstMostViewing;
	}

}
