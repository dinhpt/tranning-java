package com.staresport.form;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.staresport.dto.MatchsDto;

public class MatchForm extends BaseForm {
	/**
	 * 
	 */
	private Long matchId;
	private Long userId;
	private String name;
	private Integer team1Id;
	private Integer team2Id;
	private Date dateTime;
	private Float score1Team;
	private Float score2Team;
	private Integer totalUserBet;
	private Float totalFee;
	private Float totalMoneyBet;
	private String link;
	private Integer totalUserShow;
	private Integer team1BetPer;
	private Integer team2BetPer;
	private Integer winResults;
	private Integer status;
	private Integer bestOf;
	private Integer gameType;
	private Integer matchType;
	private String tournament;
	private Integer numHosted;
	private Date createdAt;
	private Date updateAt;
	private Integer createdBy;
	private Integer updatedBy;
	public List<MatchsDto> matchsDtos = new ArrayList<MatchsDto>();

	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getTeam1Id() {
		return team1Id;
	}

	public void setTeam1Id(Integer team1Id) {
		this.team1Id = team1Id;
	}

	public Integer getTeam2Id() {
		return team2Id;
	}

	public void setTeam2Id(Integer team2Id) {
		this.team2Id = team2Id;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public Float getScore1Team() {
		return score1Team;
	}

	public void setScore1Team(Float score1Team) {
		this.score1Team = score1Team;
	}

	public Float getScore2Team() {
		return score2Team;
	}

	public void setScore2Team(Float score2Team) {
		this.score2Team = score2Team;
	}

	public Integer getTotalUserBet() {
		return totalUserBet;
	}

	public void setTotalUserBet(Integer totalUserBet) {
		this.totalUserBet = totalUserBet;
	}

	public Float getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(Float totalFee) {
		this.totalFee = totalFee;
	}

	public Float getTotalMoneyBet() {
		return totalMoneyBet;
	}

	public void setTotalMoneyBet(Float totalMoneyBet) {
		this.totalMoneyBet = totalMoneyBet;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Integer getTotalUserShow() {
		return totalUserShow;
	}

	public void setTotalUserShow(Integer totalUserShow) {
		this.totalUserShow = totalUserShow;
	}

	public Integer getTeam1BetPer() {
		return team1BetPer;
	}

	public void setTeam1BetPer(Integer team1BetPer) {
		this.team1BetPer = team1BetPer;
	}

	public Integer getTeam2BetPer() {
		return team2BetPer;
	}

	public void setTeam2BetPer(Integer team2BetPer) {
		this.team2BetPer = team2BetPer;
	}

	public Integer getWinResults() {
		return winResults;
	}

	public void setWinResults(Integer winResults) {
		this.winResults = winResults;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getBestOf() {
		return bestOf;
	}

	public void setBestOf(Integer bestOf) {
		this.bestOf = bestOf;
	}

	public Integer getGameType() {
		return gameType;
	}

	public void setGameType(Integer gameType) {
		this.gameType = gameType;
	}

	public Integer getMatchType() {
		return matchType;
	}

	public void setMatchType(Integer matchType) {
		this.matchType = matchType;
	}

	public String getTournament() {
		return tournament;
	}

	public void setTournament(String tournament) {
		this.tournament = tournament;
	}

	public Integer getNumHosted() {
		return numHosted;
	}

	public void setNumHosted(Integer numHosted) {
		this.numHosted = numHosted;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public List<MatchsDto> getMatchsDtos() {
		return matchsDtos;
	}

	public void setMatchsDtos(List<MatchsDto> matchsDtos) {
		this.matchsDtos = matchsDtos;
	}

}
