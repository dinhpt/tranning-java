package com.staresport.form;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.staresport.dto.MatchDto;
import com.staresport.dto.MatchsDto;
import com.staresport.dto.UserHostedDto;
import com.staresport.entity.GameTypes;
import com.staresport.utils.AppConstants;

public class HomeForm extends BaseForm implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public List<MatchDto> lstAdminStreams = new ArrayList<>();
	public List<MatchDto> lstMatchComming = new ArrayList<>();
	public List<MatchsDto> lstMatchsDtos = new ArrayList<>();
	public int hourLoadComing = AppConstants.MATCH_LOAD_OPTION.HOUR_LOAD_COMING_TAB;
	public int hourLoad24h = AppConstants.MATCH_LOAD_OPTION.HOUR_LOAD_24H_TAB;
	public int loadAllMatch = AppConstants.MATCH_LOAD_OPTION.LOAD_ALL_MATCH;
	public int loadLiveMatch = AppConstants.MATCH_LOAD_OPTION.LOAD_LIVE_MATCH;
	public Integer loadOption;
	public String searchMatch;
	public Integer gameSelect;
	public UserHostedDto userHostedDto;

	public Integer getGameSelect() {
		return gameSelect;
	}

	public void setGameSelect(Integer gameSelect) {
		this.gameSelect = gameSelect;
	}

	public Integer getLoadOption() {
		return loadOption;
	}

	public void setLoadOption(Integer loadOption) {
		this.loadOption = loadOption;
	}

	public List<GameTypes> getLstGameType() {
		return lstGameType;
	}

	public void setLstGameType(List<GameTypes> lstGameType) {
		this.lstGameType = lstGameType;
	}

	public List<MatchDto> getLstAdminStreams() {
		return lstAdminStreams;
	}

	public void setLstAdminStreams(List<MatchDto> lstAdminStreams) {
		this.lstAdminStreams = lstAdminStreams;
	}

	public List<MatchDto> getLstMatchComming() {
		return lstMatchComming;
	}

	public void setLstMatchComming(List<MatchDto> lstMatchComming) {
		this.lstMatchComming = lstMatchComming;
	}

	public String getSearchMatch() {
		return searchMatch;
	}

	public void setSearchMatch(String searchMatch) {
		this.searchMatch = searchMatch;
	}

	public int getHourLoadComing() {
		return hourLoadComing;
	}

	public void setHourLoadComing(int hourLoadComing) {
		this.hourLoadComing = hourLoadComing;
	}

	public int getHourLoad24h() {
		return hourLoad24h;
	}

	public void setHourLoad24h(int hourLoad24h) {
		this.hourLoad24h = hourLoad24h;
	}

	public int getLoadAllMatch() {
		return loadAllMatch;
	}

	public void setLoadAllMatch(int loadAllMatch) {
		this.loadAllMatch = loadAllMatch;
	}

	public int getLoadLiveMatch() {
		return loadLiveMatch;
	}

	public void setLoadLiveMatch(int loadLiveMatch) {
		this.loadLiveMatch = loadLiveMatch;
	}

	public UserHostedDto getUserHostedDto() {
		return userHostedDto;
	}

	public void setUserHostedDto(UserHostedDto userHostedDto) {
		this.userHostedDto = userHostedDto;
	}

	public List<MatchsDto> getLstMatchsDtos() {
		return lstMatchsDtos;
	}

	public void setLstMatchsDtos(List<MatchsDto> lstMatchsDtos) {
		this.lstMatchsDtos = lstMatchsDtos;
	}

}
