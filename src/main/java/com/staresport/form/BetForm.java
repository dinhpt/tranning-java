package com.staresport.form;

import java.util.Date;
import java.util.List;

import com.staresport.bean.UserBetJson;
import com.staresport.dto.SubMatchsDto;
import com.staresport.dto.UserBetDetailDto;

public class BetForm extends BaseForm {
	private Integer matchId;
	private Long userId;
	private String name;
	private Integer teamId;
	// INFO TEAM 1
	private Integer team1Id;
	private String team1Name;
	private String team1Avatar;
	private String team1Flag;
	// INFO TEAM 2
	private Integer team2Id;
	private String team2Name;
	private String team2Avatar;
	private String team2Flag;

	private Date dateTime;
	private String startTime;
	private Float score1Team;
	private Float score2Team;
	private Integer totalUserBet;
	private Float totalFee;
	private Float totalMoneyBet;
	private Float moneyBet;
	private String link;
	private Integer totalUserShow;
	private Integer team1BetPer;
	private Integer team2BetPer;
	private Integer winResults;
	private Integer status;
	private Integer bestOf;
	private String tournament;
	private String linkStream;
	private boolean isLive;
	private Integer subMatchId;
	private Integer uhostedId;
	private String allBets;
	private String allBetHistories;
	private Integer betTeamId;
	private int viewStatus;
	private String uhostedName;
	private Double ubetMoneyTeam1;
	private Double ubetMoneyTeam2;
	private Double totalMoneyTeam1;
	private Double totalMoneyTeam2;
	private String moneyWinTeam1;
	private String moneyWinTeam2;
	private Double moneyTeam1;
	private Double moneyTeam2;
	private List<SubMatchsDto> subMatchs;
	private List<UserBetJson> userBetJsons;
	private List<UserBetDetailDto> betDetailList;

	public Integer getMatchId() {
		return matchId;
	}

	public void setMatchId(Integer matchId) {
		this.matchId = matchId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getTeam1Id() {
		return team1Id;
	}

	public void setTeam1Id(Integer team1Id) {
		this.team1Id = team1Id;
	}

	public String getTeam1Name() {
		return team1Name;
	}

	public void setTeam1Name(String team1Name) {
		this.team1Name = team1Name;
	}

	public String getTeam1Avatar() {
		return team1Avatar;
	}

	public void setTeam1Avatar(String team1Avatar) {
		this.team1Avatar = team1Avatar;
	}

	public String getTeam1Flag() {
		return team1Flag;
	}

	public void setTeam1Flag(String team1Flag) {
		this.team1Flag = team1Flag;
	}

	public Integer getTeam2Id() {
		return team2Id;
	}

	public void setTeam2Id(Integer team2Id) {
		this.team2Id = team2Id;
	}

	public String getTeam2Name() {
		return team2Name;
	}

	public void setTeam2Name(String team2Name) {
		this.team2Name = team2Name;
	}

	public String getTeam2Avatar() {
		return team2Avatar;
	}

	public void setTeam2Avatar(String team2Avatar) {
		this.team2Avatar = team2Avatar;
	}

	public String getTeam2Flag() {
		return team2Flag;
	}

	public void setTeam2Flag(String team2Flag) {
		this.team2Flag = team2Flag;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public Float getScore1Team() {
		return score1Team;
	}

	public void setScore1Team(Float score1Team) {
		this.score1Team = score1Team;
	}

	public Float getScore2Team() {
		return score2Team;
	}

	public void setScore2Team(Float score2Team) {
		this.score2Team = score2Team;
	}

	public Integer getTotalUserBet() {
		return totalUserBet;
	}

	public void setTotalUserBet(Integer totalUserBet) {
		this.totalUserBet = totalUserBet;
	}

	public Float getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(Float totalFee) {
		this.totalFee = totalFee;
	}

	public Float getTotalMoneyBet() {
		return totalMoneyBet;
	}

	public void setTotalMoneyBet(Float totalMoneyBet) {
		this.totalMoneyBet = totalMoneyBet;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Integer getTotalUserShow() {
		return totalUserShow;
	}

	public void setTotalUserShow(Integer totalUserShow) {
		this.totalUserShow = totalUserShow;
	}

	public Integer getTeam1BetPer() {
		return team1BetPer;
	}

	public void setTeam1BetPer(Integer team1BetPer) {
		this.team1BetPer = team1BetPer;
	}

	public Integer getTeam2BetPer() {
		return team2BetPer;
	}

	public void setTeam2BetPer(Integer team2BetPer) {
		this.team2BetPer = team2BetPer;
	}

	public Integer getWinResults() {
		return winResults;
	}

	public void setWinResults(Integer winResults) {
		this.winResults = winResults;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getBestOf() {
		return bestOf;
	}

	public void setBestOf(Integer bestOf) {
		this.bestOf = bestOf;
	}

	public String getTournament() {
		return tournament;
	}

	public void setTournament(String tournament) {
		this.tournament = tournament;
	}

	public String getLinkStream() {
		return linkStream;
	}

	public void setLinkStream(String linkStream) {
		this.linkStream = linkStream;
	}

	public boolean isLive() {
		return isLive;
	}

	public void setLive(boolean isLive) {
		this.isLive = isLive;
	}

	public List<SubMatchsDto> getSubMatchs() {
		return subMatchs;
	}

	public Integer getSubMatchId() {
		return subMatchId;
	}

	public void setSubMatchId(Integer subMatchId) {
		this.subMatchId = subMatchId;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public Float getMoneyBet() {
		return moneyBet;
	}

	public void setMoneyBet(Float moneyBet) {
		this.moneyBet = moneyBet;
	}

	public List<UserBetJson> getUserBetJsons() {
		return userBetJsons;
	}

	public Integer getUhostedId() {
		return uhostedId;
	}

	public void setUhostedId(Integer uhostedId) {
		this.uhostedId = uhostedId;
	}

	public String getAllBets() {
		return allBets;
	}

	public void setAllBets(String allBets) {
		this.allBets = allBets;
	}

	public Integer getBetTeamId() {
		return betTeamId;
	}

	public void setBetTeamId(Integer betTeamId) {
		this.betTeamId = betTeamId;
	}

	public int getViewStatus() {
		return viewStatus;
	}

	public void setViewStatus(int viewStatus) {
		this.viewStatus = viewStatus;
	}

	public String getAllBetHistories() {
		return allBetHistories;
	}

	public void setAllBetHistories(String allBetHistories) {
		this.allBetHistories = allBetHistories;
	}

	public String getUhostedName() {
		return uhostedName;
	}

	public void setUhostedName(String uhostedName) {
		this.uhostedName = uhostedName;
	}

	public Double getUbetMoneyTeam1() {
		return ubetMoneyTeam1;
	}

	public Double getUbetMoneyTeam2() {
		return ubetMoneyTeam2;
	}

	public Double getTotalMoneyTeam1() {
		return totalMoneyTeam1;
	}

	public Double getTotalMoneyTeam2() {
		return totalMoneyTeam2;
	}

	public void setUbetMoneyTeam1(Double ubetMoneyTeam1) {
		this.ubetMoneyTeam1 = ubetMoneyTeam1;
	}

	public void setUbetMoneyTeam2(Double ubetMoneyTeam2) {
		this.ubetMoneyTeam2 = ubetMoneyTeam2;
	}

	public void setTotalMoneyTeam1(Double totalMoneyTeam1) {
		this.totalMoneyTeam1 = totalMoneyTeam1;
	}

	public void setTotalMoneyTeam2(Double totalMoneyTeam2) {
		this.totalMoneyTeam2 = totalMoneyTeam2;
	}

	public String getMoneyWinTeam1() {
		return moneyWinTeam1;
	}

	public String getMoneyWinTeam2() {
		return moneyWinTeam2;
	}

	public void setMoneyWinTeam1(String moneyWinTeam1) {
		this.moneyWinTeam1 = moneyWinTeam1;
	}

	public void setMoneyWinTeam2(String moneyWinTeam2) {
		this.moneyWinTeam2 = moneyWinTeam2;
	}

	public Double getMoneyTeam1() {
		return moneyTeam1;
	}

	public void setMoneyTeam1(Double moneyTeam1) {
		this.moneyTeam1 = moneyTeam1;
	}

	public Double getMoneyTeam2() {
		return moneyTeam2;
	}

	public void setMoneyTeam2(Double moneyTeam2) {
		this.moneyTeam2 = moneyTeam2;
	}

	public void setUserBetJsons(List<UserBetJson> userBetJsons) {
		this.userBetJsons = userBetJsons;
	}

	public void setSubMatchs(List<SubMatchsDto> subMatchs) {
		this.subMatchs = subMatchs;
	}

	public List<UserBetDetailDto> getBetDetailList() {
		return betDetailList;
	}

	public void setBetDetailList(List<UserBetDetailDto> betDetailList) {
		this.betDetailList = betDetailList;
	}
}
