package com.staresport.form;

import java.io.Serializable;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

@Component(instance = InstanceType.SESSION)
public class SessionForm implements Serializable {

	public static final long serialVersionUID = 1L;


	/**
	 * Clear login session
	 */
	public void clear() {
	}

}