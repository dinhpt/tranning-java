package com.staresport.form;

import java.util.ArrayList;
import java.util.List;

import com.staresport.dto.UserBetDto;

public class HistoryForm extends BaseForm {

	private Long userId;
	private List<UserBetDto> lstUserBetDtos = new ArrayList<>();

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public List<UserBetDto> getLstUserBetDtos() {
		return lstUserBetDtos;
	}

	public void setLstUserBetDtos(List<UserBetDto> lstUserBetDtos) {
		this.lstUserBetDtos = lstUserBetDtos;
	}

}
