package com.staresport.bean;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Matchs {
	private Long matchId;
	private Long team1Id;
	private Long team2Id;
	private Integer team1BetPer;
	private Integer team2BetPer;
	private int score1Team;
	private int score2Team;
	private String name;
	private String link;
	private Date startTime;
	private Date dateTime;
	private String liveIn;
	private Integer bestOf;
	private String tournament;
	// 1:DOTA2; 2:COUNTERSTRIKE; 3:HEROESOFTHESTORM; 4:....
	private Integer gameType;
	private Integer matchType;
	// TEAMS
	private Teams team1;
	private Teams team2;
	private Set<Long> teamIds = new HashSet<>();
	// URL LIVE STREAMING
	private List<Streams> streams;

	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public Long getTeam1Id() {
		return team1Id;
	}

	public void setTeam1Id(Long team1Id) {
		this.team1Id = team1Id;
	}

	public Long getTeam2Id() {
		return team2Id;
	}

	public void setTeam2Id(Long team2Id) {
		this.team2Id = team2Id;
	}

	public Integer getTeam1BetPer() {
		return team1BetPer;
	}

	public void setTeam1BetPer(Integer team1BetPer) {
		this.team1BetPer = team1BetPer;
	}

	public Integer getTeam2BetPer() {
		return team2BetPer;
	}

	public void setTeam2BetPer(Integer team2BetPer) {
		this.team2BetPer = team2BetPer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getLiveIn() {
		return liveIn;
	}

	public void setLiveIn(String liveIn) {
		this.liveIn = liveIn;
	}

	public Teams getTeam1() {
		return team1;
	}

	public void setTeam1(Teams team1) {
		this.team1 = team1;
	}

	public Teams getTeam2() {
		return team2;
	}

	public void setTeam2(Teams team2) {
		this.team2 = team2;
	}

	public Integer getBestOf() {
		return bestOf;
	}

	public void setBestOf(Integer bestOf) {
		this.bestOf = bestOf;
	}

	public String getTournament() {
		return tournament;
	}

	public void setTournament(String tournament) {
		this.tournament = tournament;
	}

	public List<Streams> getStreams() {
		return streams;
	}

	public void setStreams(List<Streams> streams) {
		this.streams = streams;
	}

	public Integer getMatchType() {
		return matchType;
	}

	public void setMatchType(Integer matchType) {
		this.matchType = matchType;
	}

	public Integer getGameType() {
		return gameType;
	}

	public void setGameType(Integer gameType) {
		this.gameType = gameType;
	}

	public Set<Long> getTeamIds() {
		return teamIds;
	}

	public void setTeamIds(Set<Long> teamIds) {
		this.teamIds = teamIds;
	}

	public int getScore1Team() {
		return score1Team;
	}

	public void setScore1Team(int score1Team) {
		this.score1Team = score1Team;
	}

	public int getScore2Team() {
		return score2Team;
	}

	public void setScore2Team(int score2Team) {
		this.score2Team = score2Team;
	}
}
