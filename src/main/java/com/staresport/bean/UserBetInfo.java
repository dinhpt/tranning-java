package com.staresport.bean;

public class UserBetInfo extends UserBetJson {
	private static final long serialVersionUID = 1L;
	private Integer matchStatus;
	private Integer winResult;

	public Integer getMatchStatus() {
		return matchStatus;
	}

	public void setMatchStatus(Integer matchStatus) {
		this.matchStatus = matchStatus;
	}

	public Integer getWinResult() {
		return winResult;
	}

	public void setWinResult(Integer winResult) {
		this.winResult = winResult;
	}

}
