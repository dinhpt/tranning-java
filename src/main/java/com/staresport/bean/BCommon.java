package com.staresport.bean;

import java.io.Serializable;

import javax.persistence.Transient;

import org.apache.commons.beanutils.BeanComparator;

import com.staresport.utils.EntityUtil;

public abstract class BCommon<T extends Object> implements Serializable,
		Cloneable {
	private static final long serialVersionUID = 1L;
	protected boolean compareById = true;

	/**
 * 
 */
	public BCommon() {
	}

	/**
 * 
 */
	@SuppressWarnings("unchecked")
	public T clone() throws CloneNotSupportedException {
		return (T) super.clone();
	}

	/**
	 * 
	 * @param obj
	 * @param properties
	 * @return
	 */
	public int compare(T obj, String... properties) {
		BeanComparator beanComparator = new BeanComparator();
		for (String property : properties) {
			beanComparator.setProperty(property);
		}
		return beanComparator.compare(this, obj);
	}

	/**
	 * 
	 * @return
	 */
	@Transient
	public Object getComparedObjectId() {
		return null;
	}
	
	/**
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		boolean result = false;

		if (obj != null) {
			if (isCompareById()
					&& obj.getClass().getName()
							.equals(this.getClass().getName())) {
				Object objId = ((BCommon<?>) obj).getComparedObjectId();
				if (objId == null) {
					objId = EntityUtil.getIdValue(obj);
				}
				Object selfId = this.getComparedObjectId();
				if (selfId == null) {
					selfId = EntityUtil.getIdValue(this);
				}
				if (selfId != null && objId != null) {
					result = selfId.equals(objId);
				}
			} else {
				result = super.equals(obj);
			}
		}
		return result;
	}

	@Transient
	public boolean isCompareById() {
		return compareById;
	}

	public void setCompareById(boolean compareById) {
		this.compareById = compareById;
	}
}
