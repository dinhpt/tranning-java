package com.staresport.bean;


public class UserBetJson extends BCommon<UserBetJson>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String userId;
	private String matchId;
	private String subMatchId;
	private String betTime;
	private String betTeamId;
	private String betMoney;
	private String profit;
	private String noMatch;
	// DAT KEO THEO CA BO HOAC DAT CHO MOI BO
	private String betType;
	// SO LAN DAT CUOC
	private String numBet;
	// DOI KEO(0: KHONG DOI DEO; 1: DOI KEO)
	private String changed;
	// DAT CUOC LAN THU
	private String noBet;
	//
	private String status;
	private String uhostedId;
	private String numChange;
	private Double moneyTeam1;
	private Double moneyTeam2;
	
	public String getUserId() {
		return userId;
	}
	public String getMatchId() {
		return matchId;
	}
	public String getSubMatchId() {
		return subMatchId;
	}
	public String getBetTime() {
		return betTime;
	}
	public String getBetTeamId() {
		return betTeamId;
	}
	public String getBetMoney() {
		return betMoney;
	}
	public String getProfit() {
		return profit;
	}
	public String getNoMatch() {
		return noMatch;
	}
	public String getBetType() {
		return betType;
	}
	public String getNumBet() {
		return numBet;
	}
	public String getChanged() {
		return changed;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public void setMatchId(String matchId) {
		this.matchId = matchId;
	}
	public void setSubMatchId(String subMatchId) {
		this.subMatchId = subMatchId;
	}
	public void setBetTime(String betTime) {
		this.betTime = betTime;
	}
	public void setBetTeamId(String betTeamId) {
		this.betTeamId = betTeamId;
	}
	public void setBetMoney(String betMoney) {
		this.betMoney = betMoney;
	}
	public void setProfit(String profit) {
		this.profit = profit;
	}
	public void setNoMatch(String noMatch) {
		this.noMatch = noMatch;
	}
	public void setBetType(String betType) {
		this.betType = betType;
	}
	public void setNumBet(String numBet) {
		this.numBet = numBet;
	}
	public void setChanged(String changed) {
		this.changed = changed;
	}
	public String getNoBet() {
		return noBet;
	}
	public void setNoBet(String noBet) {
		this.noBet = noBet;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUhostedId() {
		return uhostedId;
	}
	public void setUhostedId(String uhostedId) {
		this.uhostedId = uhostedId;
	}
	public String getNumChange() {
		return numChange;
	}
	public void setNumChange(String numChange) {
		this.numChange = numChange;
	}
	public Double getMoneyTeam1() {
		return moneyTeam1;
	}
	public void setMoneyTeam1(Double moneyTeam1) {
		this.moneyTeam1 = moneyTeam1;
	}
	public Double getMoneyTeam2() {
		return moneyTeam2;
	}
	public void setMoneyTeam2(Double moneyTeam2) {
		this.moneyTeam2 = moneyTeam2;
	}
	@Override
	public boolean equals(Object object) {
		if (!(object instanceof UserBetJson)) {
			return false;
		}
		UserBetJson other = (UserBetJson) object;
		if ((this.subMatchId == null && other.subMatchId != null)
				|| (this.subMatchId != null && !this.subMatchId.equals(other.subMatchId))) {
			return false;
		}
		return true;
	}

}
