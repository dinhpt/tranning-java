package com.staresport.bean;

import java.io.Serializable;

public class Teams implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long teamId;
	private String name;
	private String avatar;
	private String flag;
	private Double betPercent;
	private Integer ranked;
	private String link;
	// BET PERCENT DEFAULT
	private Double betPerDef;
	
	public Long getTeamId() {
		return teamId;
	}
	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public Double getBetPercent() {
		return betPercent;
	}
	public void setBetPercent(Double betPercent) {
		this.betPercent = betPercent;
	}
	public Integer getRanked() {
		return ranked;
	}
	public void setRanked(Integer ranked) {
		this.ranked = ranked;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public Double getBetPerDef() {
		return betPerDef;
	}
	public void setBetPerDef(Double betPerDef) {
		this.betPerDef = betPerDef;
	}
	
	@Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Teams))
            return false;
        if (obj == this)
            return true;
        return (teamId != null) && this.teamId.equals(((Teams) obj).teamId);
    }
}
