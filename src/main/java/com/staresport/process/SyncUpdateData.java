package com.staresport.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.staresport.utils.AppConstants;
import com.staresport.utils.CommonUtil;

/**
 * UPDATE RESULT OF MATCH FROM http://www.gosugamers.net/ 
 * @author ChuQuangVi
 *
 */
public class SyncUpdateData {
	protected static Logger logger = Logger.getLogger(SyncUpdateData.class);
	
	public static void main(String[] args) {
		String URL = "http://api.pandascore.co/unicorn_query?token=IyQyTc8j_lNaKXgu3EWm6aApnhVMgP5G2hWih6FT6TGEDykTTkA";
		try {
		      URL url = new URL(URL);
		      InputStream inputStream = url.openStream();
		      BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		      String line = bufferedReader.readLine();
		      boolean flag = false;
		      String opentag = "<iframe";
		      String endtag = "</iframe>";
		      String frame = "";
		      while (line != null) {
		    	  if(line.contains(opentag)){
		    		  System.out.println("yeaaaaaaaaaaaaa");
		    		  flag=true;
		    	  }
		    	  if(flag){
		    		  frame += line;
		    	  }
		    	  if(line.contains(endtag)){
		    		  flag=false;
		    	  }
		        System.out.println(line);
		        line = bufferedReader.readLine();
		      }
		      frame.length();
		      bufferedReader.close();
		    } catch (MalformedURLException e) {
		      e.printStackTrace();
		    } catch (IOException e) {
		      e.printStackTrace();
		    }
	}
	
	public static int getScore(Element element, String team){
		if(element == null) return 0;
		Elements elements = element.select("span.score");
		if(CommonUtil.isEmpty(elements)) return 0;
		String score = "";
		if(elements.size() > 1){
			if(team.equalsIgnoreCase(AppConstants.GOSUGAMER.TEAM1)){
				score = elements.first().text();
			}
			else{
				score = elements.last().text();
			}
		}
		if(!CommonUtil.isEmpty(score) && CommonUtil.isNumber(score)){
			return Integer.valueOf(score).intValue();
		}
		return 0;
	}
	
	
}
























