package com.staresport.process;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.staresport.bean.Matchs;
import com.staresport.utils.AppConstants;
import com.staresport.utils.CommonUtil;

public class UpdateProcessManager extends DatabaseProcessor{
	private boolean success = true;
	protected Logger logger = Logger.getLogger("SYNC FROM GOSUGAMER");
	
	protected void process() throws Exception {
		try {
    		Connection connection = new DatabaseProcessor().getConnectionSQL();
    		if(connection == null){
    			logger.info("------CONNECT TO DB FAILED-------");
    			return;
    		}
			logger.info("----------------- START PROCESS-----------------------");
			logger.info("START UPDATE DATA GAME DOTA AT " + SyncGosugamerUtil.getDateNow());
			doUpdateData(AppConstants.GOSUGAMER.GAME_TYPE.DOTA, connection);
			logger.info("END UPDATE DATA GAME DOTA AT " + SyncGosugamerUtil.getDateNow());
			
			logger.info("-------------------------------------------------------------");
			
			logger.info("START UPDATE DATA GAME COUNTERSTRIKE AT " + SyncGosugamerUtil.getDateNow());
			doUpdateData(AppConstants.GOSUGAMER.GAME_TYPE.COUNTERSTRIKE, connection);
			logger.info("END UPDATE DATA GAME COUNTERSTRIKE AT " + SyncGosugamerUtil.getDateNow());
			
			logger.info("-------------------------------------------------------------");
			
			logger.info("START UPDATE DATA GAME HEROESOFTHESTORM AT " + SyncGosugamerUtil.getDateNow());
			doUpdateData(AppConstants.GOSUGAMER.GAME_TYPE.HEROESOFTHESTORM, connection);
			logger.info("END UPDATE DATA GAME HEROESOFTHESTORM AT " + SyncGosugamerUtil.getDateNow());
			
			logger.info("-------------------------------------------------------------");
			
			logger.info("START UPDATE DATA GAME LOL AT " + SyncGosugamerUtil.getDateNow());
			doUpdateData(AppConstants.GOSUGAMER.GAME_TYPE.LOL, connection);
			logger.info("END UPDATE DATA GAME LOL AT " + SyncGosugamerUtil.getDateNow());
			
			logger.info("-------------------------------------------------------------");
			
			logger.info("START UPDATE DATA GAME STARCRAFT2 AT " + SyncGosugamerUtil.getDateNow());
			doUpdateData(AppConstants.GOSUGAMER.GAME_TYPE.STARCRAFT2, connection);
			logger.info("END UPDATE DATA GAME STARCRAFT2 AT " + SyncGosugamerUtil.getDateNow());
			
			logger.info("-------------------------------------------------------------");
			
			logger.info("START UPDATE DATA GAME OVERWATCH AT " + SyncGosugamerUtil.getDateNow());
			doUpdateData(AppConstants.GOSUGAMER.GAME_TYPE.OVERWATCH, connection);
			logger.info("END UPDATE DATA GAME OVERWATCH AT " + SyncGosugamerUtil.getDateNow());
			
			logger.info("----------------- THE END PROCESS-----------------------");
		} catch (Exception e) {
			logger.info("----------------- ERROR PROCESS-----------------------");
		}
    	finally{
			closeObject(connection);
		}
	}
	
	private void doUpdateData(int gameType, Connection connection) throws Exception{
		String game = AppConstants.GOSUGAMER.GAME_TYPE.LINK_GAME_MAP.get(gameType);
		String url = AppConstants.GOSUGAMER.BASEURL + "/" + game + "/" + AppConstants.GOSUGAMER.GOSUBET; 
		int paging = SyncGosugamerUtil.getPaging(url, AppConstants.GOSUGAMER.RESULT_RECORD_LIMIT, 
				AppConstants.GOSUGAMER.STATUS_MATCH.RECENT_RESULTS);
		if(paging==0){
			logger.info("WARNING: NO PAGE TO GET DATA");
			return;
		}
		int inPage=0;
		try {
			logger.info("------ START UPDATE SYNC TOTAL PAGE: " + paging + " AT " + SyncGosugamerUtil.getDateNow() + "------");
			paging = paging > AppConstants.GOSUGAMER.RESULT_NUM_PAGE_SYNC ? AppConstants.GOSUGAMER.RESULT_NUM_PAGE_SYNC : paging;
			for(int i = paging; i > 0; i--){
				inPage = i;
				logger.info("--- START SYNC PAGE " + i + " AT " + SyncGosugamerUtil.getDateNow());
				url = AppConstants.GOSUGAMER.BASEURL + "/" + game + "/" 
						+ AppConstants.GOSUGAMER.GOSUBET + AppConstants.GOSUGAMER.RESULT_PAGE + i;
				List<Matchs> matchs  = getMatchsOnPage(gameType, url);
				doUpdateMatches(matchs, connection);
				if(success){
					logger.info("--- END UPDATE SYNC SUCCESS PAGE " + i + " AT " + SyncGosugamerUtil.getDateNow());
				}
				else{
					logger.info("--- END UPDATE SYNC FAILED PAGE " + i + " AT " + SyncGosugamerUtil.getDateNow());
				}
			}
		} catch (Exception e) {
			logger.info("ERROR UPDATE SYNC DATA GAME "+game+" IN PAGE " +inPage+ "AT " + SyncGosugamerUtil.getDateNow());
		}
		
	}
	
	private List<Matchs> getMatchsOnPage(int gameType, String url) throws Exception{
		String game = AppConstants.GOSUGAMER.GAME_TYPE.LINK_GAME_MAP.get(gameType);
		logger.info("START SYNC UPDATE DATA GAME " + game.toUpperCase() + "(ID:" + gameType + ")" + " AT " + SyncGosugamerUtil.getDateNow());
		Element element = SyncGosugamerUtil.getElementStatusMatch(url, AppConstants.GOSUGAMER.STATUS_MATCH.RECENT_RESULTS);
		if(element == null) return null;
		element = element.select(AppConstants.GOSUGAMER.CLASS_TAG.MATCHES).get(0);
		Elements elements = element.getElementsByTag(AppConstants.GOSUGAMER.HTML_TAG.TR);
		int size = elements.size();
		List<Long> matchIds = SyncGosugamerUtil.getMatchIds(elements);
		List<Long> idsUpdate = getMatchIdsUpdate(matchIds);
		List<Matchs> matchs = new ArrayList<>();
		for(int i=0; i<size; i++){
			Matchs match = new Matchs();
			Element el = elements.get(i);
			String link = SyncGosugamerUtil.findLink(el);
			Long matchId = SyncGosugamerUtil.getId(link);
			if(!idsUpdate.contains(matchId)) continue;
			match.setMatchId(matchId);
			int score = SyncUpdateData.getScore(el, AppConstants.GOSUGAMER.TEAM1);
			match.setScore1Team(score);
			score = SyncUpdateData.getScore(el, AppConstants.GOSUGAMER.TEAM2);
			match.setScore2Team(score);
			matchs.add(match);
		}
		return matchs;
	}
	
	private void doUpdateMatches(List<Matchs> matchs, Connection connection) throws SQLException{
		if(CommonUtil.isEmpty(matchs)){
			success = true;
			logger.info("----- NO MATCH TO UPDATE-----");
			return;
		}
		logger.info("4.START INSERT DATA AT " + SyncGosugamerUtil.getDateNow());
        PreparedStatement preStatement = null;
        success = true;
		if (connection != null) {
			connection.setAutoCommit(false);
			try {
				String query = "UPDATE MATCH SET SCORE1_TEAM = ?, SCORE2_TEAM = ?, STATUS_UPD_SCORE = ? "
						+ " WHERE MATCH_ID = ? ";
				preStatement = connection.prepareStatement(query);
				for(Matchs match : matchs){
					preStatement.setInt(1, match.getScore1Team());
					preStatement.setInt(2, match.getScore2Team());
					preStatement.setInt(3, AppConstants.GOSUGAMER.STATUS_UPDATE_SCORE.YES);
					preStatement.setLong(4, match.getMatchId());
					preStatement.addBatch();
				}
				int[] numRecInserted = preStatement.executeBatch();
	        	logger.info("EXECUTEBATCH INSERT MATCH SUCCESS. THE RECORD INSERTED " + numRecInserted.length);
			} catch (Exception e) {
				success = false;
				logger.info(" WARNING: INSERT STREAM FAILED");
			}
            // COMMIT ALL
            if(success){
            	connection.commit();
            }else{
            	connection.rollback();
            }
        }
	}
	
	/**
	 * GET ID LIST MATCH NEED UPDATE
	 * @param matchIds
	 * @return
	 */
	private List<Long> getMatchIdsUpdate(List<Long> matchIds){
		if(CommonUtil.isEmpty(matchIds)) return null;
		try {
			List<Long> idsUpdated = getMatchIdsUpdated(matchIds);
			List<Long> idsUpdate = new ArrayList<>();
	        if(CommonUtil.isEmpty(idsUpdated)) return matchIds;
	        int size = matchIds.size();
	        for(int i=0; i < size; i++){
	        	Long id = matchIds.get(i);
	        	if(!idsUpdated.contains(id)){
	        		idsUpdate.add(id);
	        	}
	        }
	        return idsUpdate;
		} catch (Exception e) {
			logger.info("ERROR FUNCTION getMatchIdsUpdate()");
		}
		return null;
	}
	
	private List<Long> getMatchIdsUpdated(List<Long> matchIds) throws Exception{
		if(CommonUtil.isEmpty(matchIds)) return null;
		Connection connection = null;
        ResultSet rs = null;
        String ids = matchIds.toString();
		ids = ids.replace("[", "(").replace("]", ")");
		String query = "SELECT m.MATCH_ID FROM MATCH m WHERE m.MATCH_ID IN " + ids;
		query += " AND m.STATUS_UPD_SCORE = " + AppConstants.GOSUGAMER.STATUS_UPDATE_SCORE.YES;
		Set<Long> idsUpdated= new HashSet<>();
		try {
			connection = new DatabaseProcessor().getConnectionSQL();
			if (connection != null) {
	            Statement select = connection.createStatement();
	            rs = select.executeQuery(query);
	            if(rs != null){
	            	while (rs.next()) {
	            		Long id = rs.getLong(1);
	            		idsUpdated.add(id);
	            	}
	            }
	        }
		} catch (Exception e) {
			success = false;
			e.getMessage();
		}
		finally{
			closeObject(connection);
			closeObject(rs);
		}
		return new ArrayList<>(idsUpdated);
	}
}
