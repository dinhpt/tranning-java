package com.staresport.process;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.staresport.bean.Matchs;
import com.staresport.bean.Streams;
import com.staresport.bean.Teams;
import com.staresport.utils.AppConstants;
import com.staresport.utils.CommonUtil;
import com.staresport.utils.ConverterUtil;

/**
 * GET DATA FROM http://www.gosugamers.net/
 * @author ChuQuangVi
 *
 */
public class SyncInsertData {
	protected static Logger logger = Logger.getLogger(SyncInsertData.class);
	
	/**
	 * GET BESTOF OF EACH MATCH
	 * @param matchDetail
	 * @return
	 */
	public static Integer getBestOf(Element element){
		if(element == null) return null;
		Elements elements = element.select(AppConstants.GOSUGAMER.CLASS_TAG.BESTOF);
		if(CommonUtil.isEmpty(elements)) return null;
		String bestof = elements.get(0).text();
		if(CommonUtil.isEmpty(bestof)) return null;
		String[] split = bestof.split(" ");
		if(CommonUtil.isEmpty(split)) return null;
		bestof = split[split.length-1];
		return (!CommonUtil.isEmpty(bestof) && 
				 CommonUtil.isInteger(bestof)) ? Integer.valueOf(bestof) : null;
	}
	/**
	 * GET BET PERCENTAGE
	 * @param element
	 * @param team
	 * @return
	 */
	public static int getBetPercentage(Element element, String team){
		if(element == null) return 0;
		return getBetPercent(element, team);
	}
	
	/**
	 * GET BET PERCENT
	 * @param element
	 * @param betTeam
	 * @return
	 */
	public static int getBetPercent(Element element, String betTeam){
		if(element == null || CommonUtil.isEmpty(betTeam)) return 0;
		Elements elements = element.select("span.bet-percentage" + betTeam);
		if(CommonUtil.isEmpty(elements)) return 0;
		String bet = null;
		try {
			bet = elements.get(0).text();
			if(!CommonUtil.isEmpty(bet)){
				bet = bet.substring(1, bet.length()-1).replace("%", "");
				if(!CommonUtil.isEmpty(bet) && CommonUtil.isNumber(bet))
					return Integer.valueOf(bet).intValue();
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return 0;
	}
	
	/**
	 * GET NAME OF TOURNAMENT
	 * @param matchDetail
	 * @return
	 */
	public static String getTournament(Document matchDetail){
		if(matchDetail == null) return null;
		try {
			Elements elements = matchDetail.select("h1 > fieldset > legend > a");
			if(CommonUtil.isEmpty(elements)) return null;
			return elements.first().text();
		} catch (Exception e) {
			e.getMessage();
		}
		return null;
	}
	
	/**
	 * GET EVENT NAME OF MATCH
	 * @param matchDetail
	 * @return
	 */
	public static String getEventName(Element element){
		if(element == null) return null;
		try {
			Elements elements = element.select("h1 > fieldset > legend > a");
			if(CommonUtil.isEmpty(elements)) return null;
			return elements.first().text();
		} catch (Exception e) {
			e.getMessage();
		}
		return null;
	}
	
	/**
	 * GET STAGE NAME OF EACH MATCH
	 * @param matchDetail
	 * @return
	 */
	public static String getStageName(Element element){
		if(element == null) return null;
		try {
			Elements elements = element.select("h1 > fieldset > legend > label.stage-name");
			if(CommonUtil.isEmpty(elements)) return null;
			return elements.first().text();
		} catch (Exception e) {
			e.getMessage();
		}
		return null;
	}
	
	public static Date getDateTime(Element element){
		Elements lives = element.select(AppConstants.GOSUGAMER.CLASS_TAG.INFO_DATETIME);
		String datetime = null;
		if(lives != null && lives.size() > 0){
			datetime = lives.get(0).text();
		}
		String year = SyncInsertData.getYearOfMatch(element);
		if(CommonUtil.isEmpty(datetime) || CommonUtil.isEmpty(year))
			return null;
		datetime = year + " " + datetime;
		return SyncInsertData.getDateTime(datetime);
	}
	
	/**
	 * GET YEAR OF MATCH THROUGH STAGE NAME
	 * @param matchDetail
	 * @return
	 */
	public static String getYearOfMatch(Element element){
		String stageName = getStageName(element);
		if(CommonUtil.isEmpty(stageName)) return null;
		int length = stageName.length();
		try {
			String year = stageName.substring(length-5, length);
			if(!CommonUtil.isEmpty(year) && CommonUtil.isValidYear(year)) return year;
		} catch (Exception e) {
			e.getMessage();
		}
		return null;
	}
	
	public static Date getDateTime(String datetime){
		if(datetime == null) return null;
		String format = AppConstants.DATE_FORMAT.SLASH_FORMAT.YYYYMMDD_TIME;
		try {
			Date convert = ConverterUtil.toDate(datetime, AppConstants.DATE_FORMAT.OTHER_FORMAT.YYYYMMDD_TIMEZONE);
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			String newdate = sdf.format(convert);
			return ConverterUtil.toDate(newdate, format);
		} catch (Exception e) {
			e.getMessage();
		}
		return null;
	}
	
	public static Teams getInfoTeam(Element element, String cssClass){
		if(element == null) return null;
		Teams team = new Teams();
		Element opponent = element.select(cssClass).get(0);
		// 1. GET LINK AND NAME OF TEAM
		Element elName =  opponent.select(AppConstants.GOSUGAMER.HTML_TAG.H3toA).get(0);
		String link = elName.attr(AppConstants.GOSUGAMER.HTML_ATTRIBUTE.HREF);
		team.setLink(link);
		String name = elName.text();
		team.setName(name);
		Long teamId = SyncGosugamerUtil.getId(link);
		if(teamId == null) return null;
		team.setTeamId(teamId);
		// END 1
		// 2. GET RANKED
		Integer ranked = getRanked(opponent);
		team.setRanked(ranked);
		// END 2
		// 3. FLAG
		String flag = opponent.select(AppConstants.GOSUGAMER.CLASS_TAG.FLAG).get(0).className().substring(5,7);
		team.setFlag(!CommonUtil.isEmpty(flag) ? flag : AppConstants.GOSUGAMER.FLAG.NO_FLAG);
		// END 3
		// 4. GET IMAGE OF TEAM
		String avatar = getAvatar(opponent);
		team.setAvatar(avatar);
		// END 4
		return team;
	}
	
	public static String getAvatar(Element element){
		String avatar = "";
		String[] imgArr = element.select(AppConstants.GOSUGAMER.CLASS_TAG.TEAM_PLAYER)
				.get(0).attr(AppConstants.GOSUGAMER.HTML_ATTRIBUTE.STYLE).split(AppConstants.SPECIAL_CHARACTERS.COLON);
		if(imgArr != null && imgArr.length > 1 && imgArr[0].equalsIgnoreCase("background-image")){
			String urlStyle = imgArr[1];
			avatar = AppConstants.GOSUGAMER.BASEURL + 
					urlStyle.substring(urlStyle.indexOf(AppConstants.SPECIAL_CHARACTERS.COMMA)+1, 
							urlStyle.lastIndexOf(AppConstants.SPECIAL_CHARACTERS.COMMA));
		}
		return avatar;
	}
	
	private static Integer getRanked(Element opponent){
		String ranked = opponent.select(AppConstants.GOSUGAMER.CLASS_TAG.RANKED).get(0).text();
		if(CommonUtil.isEmpty(ranked)) return null;
		String[] split = ranked.split(AppConstants.SPECIAL_CHARACTERS.POUND);
		if(!CommonUtil.isEmpty(split) && split.length > 1){
			ranked = split[1];
			Integer ranking = (!CommonUtil.isEmpty(ranked) && CommonUtil.isNumber(ranked) ? Integer.valueOf(ranked) : null);
			return ranking;
		}
		return null;
	}
	
	/**
	 * FIND ELEMENT HTML UPCOMING MATCH
	 * @param url
	 * @return
	 */
	public static Element getElementUpcoming(String url){
		try {
			Document document = SyncGosugamerUtil.getDocument(url);
			Elements elements = document.select(AppConstants.GOSUGAMER.CLASS_TAG.DIV_BOX);
			if(CommonUtil.isEmpty(elements)) return null;
			int n=0;
			for(Element element : elements){
				String upcoming = element.getElementsByTag(AppConstants.GOSUGAMER.HTML_TAG.H2).text();
				if(!CommonUtil.isEmpty(upcoming) && 
						upcoming.contains(AppConstants.GOSUGAMER.STATUS_MATCH.UPCOMING_MATCHES)){
					return elements.get(n);
				}
				n++;
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return null;
	}
	
	/**
	 * GET STREAM URLS FOR EACH MATCH
	 * @param matchDetail
	 * @return
	 */
	public static List<Streams> getStreamUrlsMatch(Document matchDetail, Matchs match)
    {
        Elements streamTabs = getStreamTabsFromMatchPage(matchDetail);
        if(CommonUtil.isEmpty(streamTabs)) return null;
        List<Streams> streams = new ArrayList<>();
        int size = streamTabs.size();
        for(int i=0; i< size; i++)
        {
        	Element element = streamTabs.get(i);
            Streams stream = makeStreamURL(element);
            if(stream != null){
            	stream.setMatchId(match.getMatchId());
            	streams.add(stream);
            }
        }
        return streams;
    }

	public static Elements getStreamTabsFromMatchPage(Document matchDetail) {
		Elements streamTabs;
		try {
			streamTabs = matchDetail.select(".matches-streams .match-stream-tab");
		} catch (Exception e) {
			e.printStackTrace();
			streamTabs = new Elements();
		}
		return streamTabs;
	}

	public static Streams makeStreamURL(Element streamTab) {
		if(streamTab == null) return null;
		Streams stream = new Streams();
		String streamTabText = streamTab.text();
		String lang = streamTab.select(".button").text();
		String streamURL = findStreamURL(streamTabText);
		if(CommonUtil.isEmpty(streamURL)) return null;
		stream.setLang(lang);
		stream.setUrl(streamURL);
		return stream;
	}

	public static String findStreamURL(String text) {
		String prefix = "";
		Pattern pattern;
		if(!(text.contains("<iframe") && text.contains("</iframe>"))) return null;
		if (text.contains("twitch")) {
			pattern = Pattern.compile(".*\\?channel=(.*?)\".*", Pattern.DOTALL);
			prefix = "http://player.twitch.tv/?channel=";
		} else {
			pattern = Pattern.compile(".*src=\"(.*?)\".*");
		}
		Matcher m = pattern.matcher(text);
		if (m.matches()) {
			String url = prefix + m.group(1);
			return url;
		}
		return "";
	}
}
