package com.staresport.process;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.staresport.bean.Matchs;
import com.staresport.bean.Streams;
import com.staresport.bean.Teams;
import com.staresport.utils.AppConstants;
import com.staresport.utils.CommonUtil;
import com.staresport.utils.ConverterUtil;

public class InsertProcessManager extends DatabaseProcessor{
	private boolean success = true;
	private int resultState;
	protected Logger logger = Logger.getLogger("SYNC FROM GOSUGAMER");
	
	protected void process() throws Exception {
    	try {
    		Connection connection = new DatabaseProcessor().getConnectionSQL();
    		if(connection == null){
    			logger.info("------CONNECT TO DB FAILED-------");
    			return;
    		}
			logger.info("----------------- START PROCESS-----------------------");
			logger.info("START INSERT DATA GAME DOTA AT " + SyncGosugamerUtil.getDateNow());
			doSyncData(AppConstants.GOSUGAMER.GAME_TYPE.DOTA, connection);
			logger.info("END INSERT DATA GAME DOTA AT " + SyncGosugamerUtil.getDateNow());
			
			logger.info("-------------------------------------------------------------");
			
			logger.info("START INSERT DATA GAME COUNTERSTRIKE AT " + SyncGosugamerUtil.getDateNow());
			doSyncData(AppConstants.GOSUGAMER.GAME_TYPE.COUNTERSTRIKE, connection);
			logger.info("END INSERT DATA GAME COUNTERSTRIKE AT " + SyncGosugamerUtil.getDateNow());
			
			logger.info("-------------------------------------------------------------");
			
			/*logger.info("START INSERT DATA GAME HEROESOFTHESTORM AT " + SyncGosugamerUtil.getDateNow());
			doSyncData(AppConstants.GOSUGAMER.GAME_TYPE.HEROESOFTHESTORM, connection);
			logger.info("END INSERT DATA GAME HEROESOFTHESTORM AT " + SyncGosugamerUtil.getDateNow());*/
			
			logger.info("-------------------------------------------------------------");
			
			logger.info("START INSERT DATA GAME LOL AT " + SyncGosugamerUtil.getDateNow());
			doSyncData(AppConstants.GOSUGAMER.GAME_TYPE.LOL, connection);
			logger.info("END INSERT DATA GAME LOL AT " + SyncGosugamerUtil.getDateNow());
			
			logger.info("-------------------------------------------------------------");
			
			/*logger.info("START INSERT DATA GAME STARCRAFT2 AT " + SyncGosugamerUtil.getDateNow());
			doSyncData(AppConstants.GOSUGAMER.GAME_TYPE.STARCRAFT2, connection);
			logger.info("END INSERT DATA GAME STARCRAFT2 AT " + SyncGosugamerUtil.getDateNow());
			
			logger.info("-------------------------------------------------------------");*/
			
			logger.info("START INSERT DATA GAME OVERWATCH AT " + SyncGosugamerUtil.getDateNow());
			doSyncData(AppConstants.GOSUGAMER.GAME_TYPE.OVERWATCH, connection);
			logger.info("END INSERT DATA GAME OVERWATCH AT " + SyncGosugamerUtil.getDateNow());
			
			logger.info("-------------------------------------------------------------");
			
			logger.info("----------------- THE END PROCESS-----------------------");
		} catch (Exception e) {
			logger.info("----------------- ERROR PROCESS-----------------------");
		}
    	finally{
			closeObject(connection);
		}
    }
	
	private void doSyncData(int gameType, Connection connection) throws Exception{
		String game = AppConstants.GOSUGAMER.GAME_TYPE.LINK_GAME_MAP.get(gameType);
		String url = AppConstants.GOSUGAMER.BASEURL + "/" + game + "/" + AppConstants.GOSUGAMER.GOSUBET; 
		int paging = SyncGosugamerUtil.getPaging(url, AppConstants.GOSUGAMER.UPCOMING_RECORD_LIMIT, 
				AppConstants.GOSUGAMER.STATUS_MATCH.UPCOMING_MATCHES);
		int inPage=0;
		try {
			logger.info("------ START SYNC TOTAL PAGE: " + paging + " AT " + SyncGosugamerUtil.getDateNow() + "------");
			for(int i = paging; i > 0; i--){
				inPage = i;
				logger.info("--- START SYNC PAGE " + i + " AT " + SyncGosugamerUtil.getDateNow());
				url = AppConstants.GOSUGAMER.BASEURL + "/" + game + "/" 
						+ AppConstants.GOSUGAMER.GOSUBET + AppConstants.GOSUGAMER.UPCOMING_PAGE + i;
				List<Matchs> matchs  = getMatchsOnPage(gameType, url);
				doInsertData(matchs, connection);
				if(success){
					logger.info("--- END SYNC SUCCESS PAGE " + i + " AT " + SyncGosugamerUtil.getDateNow());
				}
				else{
					logger.info("--- END SYNC FAILED PAGE " + i + " AT " + SyncGosugamerUtil.getDateNow());
				}
			}
		} catch (Exception e) {
			logger.info("ERROR SYNC DATA GAME "+game+" IN PAGE " +inPage+ "AT " + SyncGosugamerUtil.getDateNow());
		}
		
	}
	
	/**
	 * GET INFO OF MATCHES
	 * @param typeGame
	 * @return
	 * @throws Exception
	 */
	private List<Matchs> getMatchsOnPage(int gameType, String url) throws Exception{
		Element element = SyncGosugamerUtil.getElementStatusMatch(url, AppConstants.GOSUGAMER.STATUS_MATCH.UPCOMING_MATCHES);
		if(element == null) return null;
		Elements elements = element.select(AppConstants.GOSUGAMER.CLASS_TAG.MATCHES);
		if(CommonUtil.isEmpty(elements)) return null;
		element = elements.first();
		Elements tagstr = element.getElementsByTag(AppConstants.GOSUGAMER.HTML_TAG.TR);
		int size = tagstr.size();
		List<Matchs> matchs = new ArrayList<>();
		Teams team = null;
		List<Long> matchIds = SyncGosugamerUtil.getMatchIds(tagstr);
		List<Long> ids = getMatchIdsNew(matchIds);
		for(int i=0; i < size; i++){
			Matchs match = new Matchs();
			match.setGameType(gameType);
			Element el = tagstr.get(i);
			String link = SyncGosugamerUtil.findLink(el);
			match.setLink(link);
			Long matchId = SyncGosugamerUtil.getId(link);
			if(!ids.contains(matchId)) continue;
			match.setMatchId(matchId);
			// GET INFO TEAM
			//link = FileUtil.getSafePath(AppConstants.GOSUGAMER.BASEURL + link);
			Document matchDetail = SyncGosugamerUtil.getDocument(AppConstants.GOSUGAMER.BASEURL + link);
			if(matchDetail == null) continue;
			Elements elMatchs = matchDetail.select(AppConstants.GOSUGAMER.CLASS_TAG.MATCH_DETAIL);
			Element elMatch = elMatchs.get(0);
			// GET BEST OF MATCH
			Integer bestOf = SyncInsertData.getBestOf(elMatch);
			match.setBestOf(bestOf);
			// END GET BEST OF MATCH
			
			// GET EVENT NAME
			String eventName = SyncInsertData.getEventName(elMatch);
			match.setName(eventName);
			// END GET EVENT NAME
			
			// GET BET OF TEAM 1
			team = SyncInsertData.getInfoTeam(elMatch, AppConstants.GOSUGAMER.CLASS_TAG.INFO_TEAM1);
			if(team == null) continue;
			Long teamId = team.getTeamId();
			match.setTeam1Id(teamId);
			match.setTeam1(team);
			match.getTeamIds().add(teamId);
			
			// GET BET OF TEAM 2
			team = SyncInsertData.getInfoTeam(elMatch, AppConstants.GOSUGAMER.CLASS_TAG.INFO_TEAM2);
			if(team == null) continue;
			teamId = team.getTeamId();
			match.setTeam2Id(teamId);
			match.setTeam2(team);
			match.getTeamIds().add(teamId);
			// END GET INFO TEAM
			
			// DATETIME
			Date datetime = SyncInsertData.getDateTime(elMatch);
			if(datetime == null) continue;
			match.setDateTime(datetime);
			// END DATETIME
			
			// GET URL LIVE STREAMING
			List<Streams> streams = SyncInsertData.getStreamUrlsMatch(matchDetail, match);
			if(!CommonUtil.isEmpty(streams))
				match.setStreams(streams);
			// END URL LIVE STREAMING
			
			// GET TOURNAMENT
			String tournament = SyncInsertData.getTournament(matchDetail);
			match.setTournament(tournament);
			// END TOURNAMENT
			
			// GET BET PERCENTAGE
			int betPer = SyncInsertData.getBetPercentage(el, AppConstants.GOSUGAMER.CLASS_TAG.BET_TEAM1);
			match.setTeam1BetPer(betPer);
			betPer = SyncInsertData.getBetPercentage(el, AppConstants.GOSUGAMER.CLASS_TAG.BET_TEAM2);
			match.setTeam2BetPer(betPer);
			// END GET PERCENTAGE
			matchs.add(match);
		}
		return matchs;
	}
    
	/**
	 * GET ID LIST NOT INSERT INTO DB
	 * @param matchIds
	 * @return
	 * @throws Exception
	 */
	public List<Long> getMatchIdsNew(List<Long> matchIds) throws Exception{
        List<Long> idsInserted = getMatchIdsInserted(matchIds);
        List<Long> idsNew = new ArrayList<>();
        if(CommonUtil.isEmpty(idsInserted)) return matchIds;
        int size = matchIds.size();
        for(int i=0; i < size; i++){
        	Long id = matchIds.get(i);
        	if(!idsInserted.contains(id)){
        		idsNew.add(id);
        	}
        }
        return idsNew;
    }
    
	/**
	 * GET THE ID LIST OF MATCHES WAS INSERTED 
	 * @param matchIds
	 * @return
	 * @throws Exception
	 */
    private List<Long> getMatchIdsInserted(List<Long> matchIds) throws Exception{
    	Connection connection = null;
        ResultSet rs = null;
        PreparedStatement preStatement = null;
        
        String ids = matchIds.toString();
		ids = ids.replace("[", "(").replace("]", ")");
		String query = "SELECT m.MATCH_ID FROM MATCHS m WHERE m.MATCH_ID IN " + ids;
		Set<Long> idsInserted = new HashSet<>();
		try {
			connection = new DatabaseProcessor().getConnectionSQL();
			if (connection != null) {
	            Statement select = connection.createStatement();
	            rs = select.executeQuery(query);
	            if(rs != null){
	            	while (rs.next()) {
	            		Long id = rs.getLong(1);
	            		idsInserted.add(id);
	            	}
	            }
	        }
		} catch (Exception e) {
			success = false;
			e.getMessage();
		}
		finally{
			closeObject(connection);
			closeObject(rs);
			closeObject(preStatement);
		}
		return new ArrayList<>(idsInserted);
    }
    
    /**
     * GET ID LIST OF THE TEAMS WAS INSERTED 
     * @param teamIds
     * @param connection
     * @return
     * @throws Exception
     */
    private Set<Long> getTeamIdsInserted(Set<Long> teamIds, Connection connection) throws Exception{
    	if(CommonUtil.isEmpty(teamIds) || connection == null) return null;
    	ResultSet rs = null;
        String ids = teamIds.toString();
		ids = ids.replace("[", "(").replace("]", ")");
		String query = "SELECT t.TEAM_ID FROM TEAMS t WHERE t.TEAM_ID IN " + ids;
		Set<Long> idsInserted = new HashSet<>();
		try {
            Statement select = connection.createStatement();
            rs = select.executeQuery(query);
            if(rs != null){
            	while (rs.next()) {
            		Long id = rs.getLong(1);
            		idsInserted.add(id);
            	}
            }
		} catch (Exception e) {
			closeObject(connection);
			closeObject(rs);
		}
		return idsInserted;
    }
    
    private List<Teams> getTeamIdsNew(List<Teams> teams, Set<Long> teamIds, Connection connection){
    	if(CommonUtil.isEmpty(teamIds) || 
    			CommonUtil.isEmpty(teams) || connection == null) return null;
    	try {
    		Set<Long> idsInserted = getTeamIdsInserted(teamIds, connection);
    		List<Teams> teamsNew = new ArrayList<>();
    		if(CommonUtil.isEmpty(idsInserted)) return teams;
    		int size = teams.size();
    		for(int i=0; i < size; i++){
    			Teams team = teams.get(i);
    			Long id = team.getTeamId();
    			if(!idsInserted.contains(id)){
    				teamsNew.add(team);
    			}
    		}
    		return teamsNew;
		} catch (Exception e) {
			e.getMessage();
		}
    	return null;
    }
    
    private int doInsertData(List<Matchs> matchs, Connection connection) throws Exception{
		if(CommonUtil.isEmpty(matchs)) {
			logger.info("---NO DATA TO SYNC----");
			return AppConstants.RESULT_STATE.COMPLETED;
		}
		logger.info("4.START INSERT DATA AT " + SyncGosugamerUtil.getDateNow());
        PreparedStatement preStatement = null;
        success = true;
		try {
			if (connection != null) {
				connection.setAutoCommit(false);
				// INSERT MATCHS
				doInsertMatch(connection, preStatement, matchs);
	            // INSERT STREAMS
	            doInsertStreams(connection, preStatement, matchs);
	            // INSERT TEAM
	            doInsertTeams(connection, preStatement, matchs);
	            
	            // COMMIT ALL
	            if(success){
	            	connection.commit();
	            	resultState = AppConstants.RESULT_STATE.NO_ERROR;
	            }else{
	            	connection.rollback();
	            	resultState = AppConstants.RESULT_STATE.ERROR;
	            }
	            
	        }
		} catch (Exception e) {
			success = true;
			if(connection != null) connection.rollback();
			e.getMessage();
		}
		finally{
			closeObject(preStatement);
		}
		return resultState;
	}
    
    /**
     * INSERT MATCHS
     * @param connection
     * @param preStatement
     * @param matchs
     * @param streamList
     * @param teams
     * @param teamIds
     * @return
     * @throws Exception
     */
    private boolean doInsertMatch(Connection connection, PreparedStatement preStatement, List<Matchs> matchs) throws Exception{
    	if(!success) return false;
    	try {
    		String query = "INSERT INTO MATCHS(MATCH_ID,TEAM1_ID, TEAM2_ID,"
					+ " NAME, LINK, DATE_TIME, TEAM1_BET_PER, TEAM2_BET_PER, "
					+ " BEST_OF, TOURNAMENT, GAME_TYPE)"           
					+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    		preStatement = connection.prepareStatement(query);
    		for(Matchs match : matchs){
    			preStatement.setLong(1, match.getMatchId());
    			preStatement.setLong(2, match.getTeam1Id());
    			preStatement.setLong(3, match.getTeam2Id());
    			preStatement.setString(4, match.getName());
    			preStatement.setString(5, match.getLink());
    			preStatement.setTimestamp(6, ConverterUtil.toTimestamp(match.getDateTime()));
    			preStatement.setDouble(7, match.getTeam1BetPer());
    			preStatement.setDouble(8, match.getTeam2BetPer());
    			preStatement.setInt(9, match.getBestOf());
    			preStatement.setString(10, match.getTournament());
    			preStatement.setInt(11, match.getGameType());
    			preStatement.addBatch();
    		}
    		int[] numRecInserted = preStatement.executeBatch();
        	logger.info("EXECUTEBATCH INSERT MATCH SUCCESS. THE RECORD INSERTED " + numRecInserted.length);
		} catch (Exception e) {
			success = false;
			resultState = AppConstants.RESULT_STATE.ERROR;
			logger.info("WARNING: INSERT MATCH FAILED");
			closeObject(connection);
		}
        return success;
    }
    
    /**
     * INSERT STREAM LIST
     * @param connection
     * @param preStatement
     * @param matchs
     * @return
     * @throws Exception
     */
    private boolean doInsertStreams(Connection connection, PreparedStatement preStatement, List<Matchs> matchs) throws Exception{
    	if(!success) return false;
    	List<Streams> streamList = new ArrayList<>();
    	for(Matchs match : matchs){ 
    		List<Streams> streams = match.getStreams();
    		if(!CommonUtil.isEmpty(streams)){
    			streamList.addAll(streams);
    		}
    	}
    	if(CommonUtil.isEmpty(streamList)){
    		logger.info("---NO STREAMS TO INSERT---");
    		success = true;
    		return success;
    	}
    	try {
    		String query = "INSERT INTO STREAMS(MATCH_ID, LANG, URL) VALUES(?, ?, ?)";
            preStatement = connection.prepareStatement(query);
            for(Streams stream : streamList){
            	preStatement.setLong(1, stream.getMatchId());
            	preStatement.setString(2, stream.getLang());
            	preStatement.setString(3, stream.getUrl());
            	preStatement.addBatch();
            }
            int[] numRecInserted = preStatement.executeBatch();
        	logger.info("EXECUTEBATCH INSERT STREAMS SUCCESS. THE RECORD INSERTED " + numRecInserted.length);
		} catch (Exception e) {
			success = false;
			logger.info(" WARNING: INSERT STREAM FAILED");
			closeObject(connection);
		}
        return success;
    }
    
    /**
     * INSERT TEAM LIST
     * @param connection
     * @param preStatement
     * @param matchs
     * @return
     * @throws Exception
     */
    private boolean doInsertTeams(Connection connection, PreparedStatement preStatement, List<Matchs> matchs) throws Exception{
    	if(!success) return false;
    	List<Teams> teams = new ArrayList<>();
    	Set<Long> teamIds = new HashSet<>();
    	for(Matchs match : matchs){
    		teamIds.addAll(match.getTeamIds());
    		Teams team = match.getTeam1();
    		if(!teams.contains(team)){
    			teams.add(team);
    		}
    		team = match.getTeam2();
    		if(!teams.contains(team)){
    			teams.add(team);
    		}
    	}
    	if(CommonUtil.isEmpty(teams)){
    		logger.info("---NO TEAMS TO INSERT---");
    		success = true;
    		return success;
    	}
    	try {
    		String query = "INSERT INTO TEAMS(TEAM_ID, NAME, RANKED, AVATAR, FLAG, LINK)"
        			+ " VALUES(?, ?, ?, ?, ?, ?)";
    		preStatement = connection.prepareStatement(query);
    		List<Teams> teamsInsert = getTeamIdsNew(teams, teamIds, connection);
    		for(Teams team : teamsInsert){
    			preStatement.setLong(1, team.getTeamId());
    			preStatement.setString(2, team.getName());
    			preStatement.setInt(3, team.getRanked()!=null?team.getRanked():0);
    			preStatement.setString(4, team.getAvatar());
    			preStatement.setString(5, team.getFlag());
    			preStatement.setString(6, team.getLink());
    			preStatement.addBatch();
    		}
    		int[] numRecInserted = preStatement.executeBatch();
			logger.info("EXECUTEBATCH INSERT TEAMS SUCCESS. THE RECORD INSERTED "
					+ numRecInserted.length);
		} catch (Exception e) {
			success = false;
			logger.info("WARNING: INSERT TEAMS FAILED");
			closeObject(connection);
		}
        return success;
    }
    
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
    
   
}
