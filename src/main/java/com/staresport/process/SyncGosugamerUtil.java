package com.staresport.process;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.staresport.utils.AppConstants;
import com.staresport.utils.CommonUtil;
import com.staresport.utils.DateUtil;

public class SyncGosugamerUtil {
	protected static Logger logger = Logger.getLogger(SyncGosugamerUtil.class);
	
	/**
	 * GET ID LIST OF MATCHES ON A PAGE
	 * @param elements
	 * @return
	 */
	public static List<Long> getMatchIds(Elements elements){
		if(CommonUtil.isEmpty(elements)) return null;
		int size = elements.size();
		Set<Long> ids = new HashSet<>();
		for(int i=0; i < size; i++){
			Element el = elements.get(i);
			String link = findLink(el);
			Long matchId = getId(link);
			if(matchId != null){
				ids.add(matchId);
			}
		}
		return new ArrayList<Long>(ids);
	}
	
	public static String findLink(Element element){
		if(element == null) return "";
		String href = element.getElementsByTag(AppConstants.GOSUGAMER.HTML_TAG.A).
				get(0).attr(AppConstants.GOSUGAMER.HTML_ATTRIBUTE.HREF);
		return href;
	}
	
	/**
	 * GET ID OF MATCH OR TEAM
	 * @param link
	 * @return
	 */
	public static Long getId(String link){
		if(CommonUtil.isEmpty(link)) return null;
		String[] split = link.split(AppConstants.SPECIAL_CHARACTERS.SLASH);
		Long returnId=null;
		if(!CommonUtil.isEmpty(split)){
			String value = split[split.length-1];
			if(!CommonUtil.isEmpty(value)){
				split = value.split(AppConstants.SPECIAL_CHARACTERS.HYPHEN);
				if(!CommonUtil.isEmpty(split)){
					String id = split[0];
					returnId = !CommonUtil.isEmpty(id) && CommonUtil.isNumber(id) ? Long.valueOf(id) : null;
				}
			}
		}
		return returnId;
	}
	
	/**
	 * FIND ELEMENT HTML UPCOMING MATCH OR RECENT RESULTS 
	 * @param url
	 * @return
	 */
	public static Element getElementStatusMatch(String url, String statusMatch){
		try {
			Document document = SyncGosugamerUtil.getDocument(url);
			Elements elements = document.select(AppConstants.GOSUGAMER.CLASS_TAG.DIV_BOX);
			if(CommonUtil.isEmpty(elements)) return null;
			int n=0;
			for(Element element : elements){
				String status = element.getElementsByTag(AppConstants.GOSUGAMER.HTML_TAG.H2).text();
				if(!CommonUtil.isEmpty(status) && 
						status.contains(statusMatch)){
					return elements.get(n);
				}
				n++;
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return null;
	}
	
	public static int getPaging(String url, int limit, String statusMatch){
		Integer total = getTotalRecord(url, statusMatch);
		if(total == null) return 1;
		return (int)(Math.ceil(Double.valueOf(total)/Double.valueOf(limit)));
	}
	
	/**
	 * GET TOTAL RECORD OF UPCOMING
	 * @param url
	 * @return
	 */
	public static Integer getTotalRecord(String url, String statusMatch){
		Element element = getElementStatusMatch(url, statusMatch);
		if(element == null) return null;
		Elements elements = element.select(AppConstants.GOSUGAMER.CLASS_TAG.PAGINATOR);
		if(CommonUtil.isEmpty(elements)) return null;
		String paging = elements.first().text();
		if(CommonUtil.isEmpty(paging)) return null;
		try {
			Pattern p = Pattern.compile("Showing (\\d+) to (\\d+) of (\\d+|\\d+,\\d+) Matches");
		    Matcher m = p.matcher(paging);
		    if(m.matches()){
		    	String group = m.group(3);
		    	String page = group.replace(",", "");
		    	if(!CommonUtil.isEmpty(page) && CommonUtil.isNumber(page)){
		    		return Integer.valueOf(page);
		    	}
		    }
		} catch (Exception e) {
			e.getMessage();
		}
		return null;
	}
	
	public static Document getDocument(String url)
    { 
        Connection c =  Jsoup.connect(url); 
        c.userAgent("Mozilla/5.0 (X11; Linux x86_64; rv:35.0) Gecko/20100101 Firefox/35.0"); 
        c.timeout(AppConstants.GOSUGAMER.CONNECT_TIMOUT); 
        Document doc = null; 
        try 
        { 
            doc = c.get(); 
        } 
        catch(IOException e) 
        { 
        	logger.info("---ERROR READ DOCUMENT " + e.getMessage() + "--");
        } 
        return doc; 
    } 
	
	public static String getDateNow(){
    	return DateUtil.formatDate(new Date(), AppConstants.DATE_FORMAT.SLASH_FORMAT.DDMMYYYY_TIME);
    }
}
