package com.staresport.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.WordUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;


/**
 * 
 * @author ChuQuangVi
 *
 */
public class ConverterUtil {
	private static final Gson gson = new GsonBuilder().serializeNulls().create();
    private static final SimpleDateFormat calendarDatetimeSdf = new SimpleDateFormat(
            "yyyyMMdd'T'HHmm'00'");
    private static final DecimalFormat numberDf = new DecimalFormat("######");
    private static final DecimalFormat doubleDf = new DecimalFormat(
            "######.#########");
    private static final DecimalFormat numberCommaDf = new DecimalFormat(
            "###,###");
    private static final DecimalFormat doubleCommaDf = new DecimalFormat(
            "###,###.#########");
    private static final DecimalFormat percentDf = new DecimalFormat("#%");

    /**
     * Convert format abcDefGhi to abc_def_ghi
     * 
     * @param entityName
     * @return
     */
    public static String entityNameToTableName(String entityName) {
        if (entityName == null || entityName.length() <= 1) {
            return entityName;
        }
        StringBuffer tmp = new StringBuffer(entityName);
        StringBuffer rs = new StringBuffer();
        rs.append(tmp.charAt(0));
        for (int i = 1; i < tmp.length(); i++) {
            char c = tmp.charAt(i);
            if (Character.isUpperCase(c)) {
                rs.append("_");
            }
            rs.append(c);
        }
        return rs.toString().toLowerCase();
    }

    /**
     * Convert Object to ByteArray
     * 
     * @param o
     * @return
     */
    public static byte[] toByteArray(Object o) {
        if (o == null)
            return null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(o);
            byte[] buf = baos.toByteArray();
            return buf;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Convert ByteArray to Object
     * 
     * @param b
     * @return
     */
    public static Object toObject(byte[] b) {
        if (b == null) {
            return null;
        }
        try {
            ObjectInputStream ois = new ObjectInputStream(
                    new ByteArrayInputStream(b));
            Object object = ois.readObject();
            ois.close();
            return object;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Convert first letter to lower case
     * 
     * @param s
     * @return
     */
    public static String toLowerCaseFirstLetter(String s) {
        if (s == null) {
            return null;
        }
        String firstLetter = s.substring(0, 1).toLowerCase();
        return firstLetter + s.substring(1, s.length());
    }

    /**
     * Convert first letter to upper case
     * 
     * @param s
     * @return
     */
    public static String toUppperCaseFirstLetter(String s) {
        if (s == null) {
            return null;
        }
        String firstLetter = s.substring(0, 1).toUpperCase();
        return firstLetter + s.substring(1, s.length());
    }
    
    public static String toCapitalize(String s) {
        if (s == null) {
            return null;
        }
        return WordUtils.capitalize(s);
    }

    /**
     * Convert String to Date manual format
     * 
     * @param s
     * @param format
     * @return
     */
    public static Date toDate(String s, String format) {
        if (CommonUtil.isEmpty(s)) {
            return null;
        }
        try {
            SimpleDateFormat df = new SimpleDateFormat(format);
            return df.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Convert string of milisecond to date
     * 
     * @param s
     * @return
     */
    public static Date toDateMilisecond(String s) {
        try {
            Date date = new Date();
            date.setTime(Long.valueOf(s));
            return date;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Convert date to string manual format
     * 
     * @param date
     * @param format
     * @return
     */
    public static String toString(Date date, String format) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    /**
     * Convert date time to string for calendar date time
     * 
     * @param date
     * @return
     */
    public static String toStringCalendarDatetime(Date date) {
        if (date == null) {
            return "";
        }
        return calendarDatetimeSdf.format(date);
    }

    /**
     * Convert date time to string of milisecond
     * 
     * @param date
     * @return
     */
    public static String toStringMilisecond(Date date) {
        if (date == null) {
            return "";
        }
        return String.valueOf(date.getTime());
    }

    /**
     * Convert number to string with prefix zero
     * 
     * @param n
     * @param length
     * @return
     */
    public static String numberToString(Number n, int length) {
        String s = new String();
        if (n == null) {
            return s;
        }
        for (int i = 0; i < length; i++) {
            if (n.longValue() < Math.pow(10, i)) {
                s += "0";
            }
        }
        if (n.longValue() > 0) {
            s += n.toString();
        }
        return s;
    }

    /**
     * Convert java.util.Date to java.sql.Date
     * 
     * @param date
     * @return
     */
    public static java.sql.Date toSqlDate(Date date) {
        if (date == null) {
            return null;
        }
        return new java.sql.Date(date.getTime());
    }
    /**
     * Utility method to convert Date to Timestamp in Java 
     * @param date * @return Timestamp 
     */
    public static Timestamp toTimestamp(Date date) { 
    	return date == null ? null : new java.sql.Timestamp(date.getTime()); 
    }


    /**
     * Convert Array to ArrayList
     * 
     * @param a
     * @return
     */
    public static <T> List<T> toArrayList(T[] a) {
        return Arrays.asList(a);
    }

    /**
     * 
     * @param d
     * @return
     */
    public static String formatNumber(Long d) {
        if (d == null) {
            return "";
        } else {
            return numberDf.format(d);
        }
    }

    /**
     * 
     * @param d
     * @return
     */
    public static String formatNumber(Double d) {
        if (d == null) {
            return "";
        } else {
            return doubleDf.format(d);
        }
    }

    /**
     * 
     * @param d
     * @return
     */
    public static String formatCommaNumber(Long d) {
        if (d == null) {
            return "";
        } else {
            return numberCommaDf.format(d);
        }
    }

    /**
     * 
     * @param d
     * @return
     */
    public static String formatCommaNumber(Double d) {
        if (d == null) {
            return "";
        } else {
            return doubleCommaDf.format(d);
        }
    }

    /**
     * 
     * @param d
     * @param format
     * @return
     */
    public static String formatCommaNumber(Double d, String format) {
        if (d == null) {
            return "";
        } else {
            DecimalFormat decimalFormat = new DecimalFormat(format);
            return decimalFormat.format(d);
        }
    }

    /**
     * 
     * @param d
     * @return
     */
    public static String formatPercent(Double d) {
        if (d == null) {
            return "";
        } else {
            return percentDf.format(d / 100);
        }
    }

    /**
     * 
     * @param s
     * @param l
     * @return
     * @throws Exception
     */
    public static String formatMaxLength(String s, int l) throws Exception {
        if (s == null)
            return null;
        if (s.length() <= l)
            return s;
        return s.substring(0, l) + "...";
    }

    /**
     * Convert Exception to String
     * 
     * @param ex
     * @return
     * @throws Exception
     */
    public static String toString(Exception ex) throws Exception {
        try {
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            return errors.toString();
        } catch (Exception e) {
        }
        return null;
    }

    /**
     * Convert BigDecimal to Long
     * 
     * @param num
     * @return
     */
    public static Long toLongValue(Object num) {
        if (num == null)
            return null;
        return ((BigDecimal) num).longValue();
    }

    /**
     * Convert BigDecimal to Integer
     * 
     * @param num
     * @return
     */
    public static Integer toIntValue(Object num) {
        if (num == null)
            return null;
        return ((BigDecimal) num).intValue();
    }

    /**
     * Convert BigDecimal to Double
     * 
     * @param num
     * @return
     */
    public static Double toDoubleValue(Object num) {
        if (num == null)
            return null;
        return ((BigDecimal) num).doubleValue();
    }


    /**
     * Convert pathString to List<long>
     * 
     * @param stringPath
     * @return
     */
    public static List<Long> convertStringPathToLong(String stringPath) {
        if (CommonUtil.isEmpty(stringPath)) {
            return new ArrayList<Long>();
        }
        List<Long> result = new ArrayList<Long>();
        String[] stringPathSplit = stringPath.split("/");
        int size = stringPathSplit.length;
        for (int i = 0; i < size; i++) {
            if (!CommonUtil.isEmpty(stringPathSplit[i])) {
                result.add(Long.valueOf(stringPathSplit[i]));
            }
        }
        return result;
    }

    /**
     * 
     * @param value
     * @param accuracy
     * @return
     */
    public static Double round(Double value, int scale) {
        Double result = value;
        if (value != null) {
            result = new BigDecimal(value)
                    .setScale(scale, RoundingMode.HALF_UP).doubleValue();
        }
        return result;
    }
    
    public static Integer roundUp(Double value) {
        if (value != null) {
            return value.intValue();
        }
        return 0;
    }
    
    public static String convertNullToEmpty(String str){
		return str != null ? str : "";
	}
    
    public static String formatPhoneNumber(String phone){
    	if(CommonUtil.isEmpty(phone)) return "";
    	String result = "";
    	String[] split = phone.split("-");
    	int length = split.length;
    	if(length == 2){
    		result = "()" + phone;
    	}else if(length == 3){
    		result = "(" + split[0] + ")" + split[1] + "-" + split[2];
    	}
    	return result;
    }
    
    /**
     * Convert Map to JSON
     * 
     * @param map
     * @return
     */
    public static String toJsonString(Map<String, String> map) {
        String json = "";
        if (map == null)
            return json;
        try {
            json = new ObjectMapper().writeValueAsString(map);
        } catch (Exception e) {
        }
        return json;
    }

    /**
     * Convert JSON to Map
     * 
     * @param json
     * @return
     */
    public static Map<String, String> toMap(String json) {
        Map<String, String> map = new HashMap<String, String>();
        if (json == null)
            return map;
        try {
            map = new ObjectMapper().readValue(json,
                    new TypeReference<HashMap<String, String>>() {
                    });
        } catch (Exception e) {
        }
        return map;
    }

    /**
     * Convert Object to JSON
     * 
     * @param map
     * @return
     */
    public static String toJsonString(Object obj) {
        String json = "";
        if (obj == null)
            return json;
        return gson.toJson(obj);
    }

    /**
     * Convert List to JSON
     * 
     * @param map
     * @return
     */
    public static String toJsonString(List<Object> objs) {
        String json = "";
        if (objs == null)
            return json;
        return gson.toJson(objs);
    }

    /**
     * Convert string to object
     * 
     * @param c
     * @param json
     * @return
     */
    public static <T> T fromJSON(Class<T> c, String json) {
        JsonReader reader = new JsonReader(new StringReader(json));
        reader.setLenient(true);
        return gson.fromJson(reader, c);
    }
    
    public static JsonArray toStringJsonArr(String json){
    	if(json == null) return null;
    	JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(json);
        return element.getAsJsonArray();
    }
}
