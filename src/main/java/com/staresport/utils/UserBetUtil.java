package com.staresport.utils;

import com.staresport.dto.BetDto;

public class UserBetUtil {
	
	public static BetDto caculateRate(Double ubet1, Double ubet2, Double totUbet1, Double totUbet2){
		BetDto betDto = new BetDto();
		Double rate1 = 0.0;
		Double rate2 = 0.0;
		if(ubet1 == null){
			ubet1 = 0.0;
		}
		if(ubet2 == null){
			ubet2 = 0.0;
		}
		if(totUbet1 == null || totUbet1 == 0.0){
			totUbet1 = 1.0;
		}
		
		if(totUbet2 == null || totUbet2 == 0.0){
			totUbet2 = 1.0;
		}
		rate1 = ConverterUtil.round(totUbet2*0.98/totUbet1, 4);
		rate2 = ConverterUtil.round(totUbet1*0.98/totUbet2, 4);
		Double rate = rate1 + rate2;
		betDto.setPctgTeam1(String.valueOf(ConverterUtil.round(rate1/rate, 4)*100));
		betDto.setPctgTeam2(String.valueOf(ConverterUtil.round(rate2/rate, 4)*100));
		betDto.setRateTeam1(rate1);
		betDto.setRateTeam2(rate2);
		betDto.setProfitTeam1(ConverterUtil.roundUp(ubet1*rate1));
		betDto.setProfitTeam2(ConverterUtil.roundUp(ubet2*rate2));
		return betDto;
	}
}
