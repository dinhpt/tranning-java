package com.staresport.utils;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;

/**
 * 
 * @author ChuQuangVi
 *
 */
@SuppressWarnings("rawtypes")
public class LoggerUtil {

    private static final Logger LOGGER = Logger.getLogger(LoggerUtil.class);
    private static final Logger LOGGER_BEN = Logger.getLogger("LOG.BEN");
    private static final String FILE_PATH = ResourcesUtil.getResource(ResourcesUtil.APP_CONFIG, "folder.log");

    public static void createPerformanceLog(Class c, String methodName,
            Date startTime, Date endTime) {
        createPerformanceLog(c, methodName, null, startTime, endTime);
    }

    public static void createPerformanceLog(Class c, String methodName,
            long startTime, long endTime) {
        createPerformanceLog(c, methodName, null, startTime, endTime);
    }

    public static void createPerformanceLog(Class c, String methodName,
            String functionName, Date startTime, Date endTime) {
        createPerformanceLog(c, methodName, functionName, startTime.getTime(),
                endTime.getTime());
    }

    public static void createPerformanceLog(Class c, String methodName,
            String functionName, long startTime, long endTime) {
        StringBuilder log = new StringBuilder();
        log.append(c.getName());
        log.append(" ! ");
        log.append(CommonUtil.NVL(methodName));
        log.append(" ! ");
        log.append(CommonUtil.NVL(functionName));
        log.append(" $ ");
        log.append(startTime);
        log.append(" $ ");
        log.append(endTime);
        log.append(" $ ");
        log.append(endTime - startTime);
        Appender fa = null;
        try {
			fa = new FileAppender(new SimpleLayout(), "D:/WORK/PROJECTS/BEN/documents/25_Schedule/MyLogFile.log");
			LOGGER_BEN.addAppender(fa);
		} catch (IOException e) {
			e.printStackTrace();
		}
        LOGGER_BEN.info(log.toString());
    }

    public static void createPageLog(String page) {
        StringBuilder log = new StringBuilder();
        log.append("page = ");
        log.append(page);
        LOGGER.info(log.toString());
    }
    
    public static void createLog (String result) {
        StringBuilder log = new StringBuilder();
        log.append("result = ");
        log.append(result);
        LOGGER.info(log.toString());
    }
    
    /**
	 * 
	 * @param logger
	 * @param log
	 * @param fileName
	 */
	public static void writeLogInFile(Logger logger, StringBuilder log, String fileName) {
		Date now = new Date();
		fileName = fileName + "_" + DateUtil.date2StringNoSlash(now) + ".log";
		String filePath = FILE_PATH + File.separator + DateUtil.date2yyyyMMStringNoSlash(now);
		String fileUrl = FileUtils.getSafePath(filePath + File.separator + fileName);
		Appender fa = null;
		try {
			fa = new FileAppender(new SimpleLayout(), fileUrl);
			logger.addAppender(fa);
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.info(log.toString());
	}

	public static void writeDatabaseLog(String status, String fileName) {
		StringBuilder sb = new StringBuilder();
		Date now = new Date();

		sb.append(DateUtil.dateTime2String(now) + " :  ");
		sb.append(status);
		writeLogInFile(LOGGER, sb, fileName);
	}
}
