package com.staresport.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.validator.UrlValidator;
import org.jsoup.select.Elements;

/**
 * 
 * @author ChuQuangVi
 *
 */
public class CommonUtil {

	/**
	 * Check email
	 * 
	 * @param s
	 * @return
	 */
	public static boolean isEmail(String s) {
		if (CommonUtil.isEmpty(s)) {
			return false;
		}
		try {
			Pattern pattern = Pattern.compile(
					"(^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$)");
			Matcher matcher = pattern.matcher(s.trim());
			if (matcher.find()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean isInteger(String str) {
		if (str == null || !str.matches("[0-9]+$")) {
			return false;
		}
		return true;
	}

	/**
	 * @param phoneNumber
	 * @return
	 */
	public static boolean validatePhoneNumber(String phoneNumber) {
		if (phoneNumber != null && !phoneNumber.isEmpty()) {
			boolean isValid = false;
			String expression = "([+]?[-]?[0-9\\-]?){9,11}[0-9]$";
			CharSequence inputStr = phoneNumber;
			Pattern pattern = Pattern.compile(expression);
			Matcher matcher = pattern.matcher(inputStr);
			if (phoneNumber.length() >= 9 || phoneNumber.length() <= 11) {
				if (matcher.matches()) {
					isValid = true;
				}
			} else {
				isValid = false;
			}
			return isValid;
		}
		return false;
	}

	public static boolean isPhoneNumber(String phone) {
		if (CommonUtil.isEmpty(phone))
			return false;
		// NNNN-NNNN || 0NNNN-NNNN-NNNN
		String regex = "^[0-9]{1,4}-[0-9]{4}$|^[0][0-9]{1,4}-[0-9]{1,4}-[0-9]{4}$";

		CharSequence inputStr = phone;
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches())
			return true;
		return false;
	}

	public static boolean isPhoneNumberNo2(String phone) {
		if (CommonUtil.isEmpty(phone))
			return false;
		// 0N0-NNNN-NNNN
		String regex = "^[0][0-9]{1}[0]-[0-9]{4}-[0-9]{4}$";
		CharSequence inputStr = phone;
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches())
			return true;
		return false;
	}

	public static boolean isYear(String year) {
		if (isEmpty(year))
			return false;
		String regex = "^[12][0-9]{3}$";
		CharSequence inputStr = year;
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches())
			return true;
		return false;
	}
	
	public static boolean isValidYear(String year) {
		if(isEmpty(year)) return false;
	    SimpleDateFormat df = new SimpleDateFormat("yyyy");
	    try {
	        df.parse(year);
	        return true;
	    } catch (ParseException e) {
	        return false;
	    }
	}
	
	public static boolean isYearJapanes(String year){
		if (isEmpty(year))
			return false;
		String regex = "^[0-9]{1,2}$";
		CharSequence inputStr = year;
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches())
			return true;
		return false;
	}

	public static boolean isMonth(String month) {
		if (isEmpty(month))
			return false;
		if (!isInteger(month))
			return false;
		Integer m = Integer.valueOf(month);
		if (m > 0 && m < 13)
			return true;
		return false;
	}

	public static boolean isDay(String day) {
		if (isEmpty(day))
			return false;
		if (!isInteger(day))
			return false;
		Integer d = Integer.valueOf(day);
		if (d > 0 && d < 32)
			return true;
		return false;
	}

	public static boolean isDate(String dateToValidate, String dateFromat) {
		if (dateToValidate == null) {
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);
		try {
			sdf.parse(dateToValidate);
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public static boolean isDate(String y, String m, String d) {
		if (!isYear(y) || !isMonth(m) || !isDay(d))
			return false;
		boolean validDate = false;
		Integer year = Integer.valueOf(y);
		Integer month = Integer.valueOf(m);
		Integer day = Integer.valueOf(d);
		// For months with 30 days
		if ((month == 4 || month == 6 || month == 9 || month == 11) && (day <= 30)) {
			validDate = true;
		}
		// For months with 31 days
		if ((month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
				&& (day <= 31)) {
			validDate = true;
		}
		// For February
		if ((month == 2) && (day < 30)) {
			// Boolean for valid leap year
			boolean validLeapYear = false;

			// A leap year is any year that is divisible by 4 but not divisible
			// by 100 unless it is also divisible by 400
			if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
				validLeapYear = true;
			}
			if (validLeapYear == true && day <= 29) {
				validDate = true;
			} else if (validLeapYear == false && day <= 28) {
				validDate = true;
			}
		}
		return validDate;
	}
	
	public static boolean isHour(String str){
		if(CommonUtil.isEmpty(str) || !isInteger(str)) return false;
		int hour = Integer.valueOf(str);
		return (hour>=0 && hour < 24);
	}
	
	public static boolean isMinute(String str){
		if(CommonUtil.isEmpty(str) || !isInteger(str)) return false;
		int minute = Integer.valueOf(str);
		return (minute>=0 && minute < 60);
	}

	public static boolean isWhiteSpace(String str) {
		Pattern pattern = Pattern.compile("\\s");
		Matcher matcher = pattern.matcher(str);
		return matcher.find();
	}

	/**
	 * 
	 * @param value
	 * @param format
	 * @return
	 * @author vicq
	 */
	public static boolean isValidFormatDate(String value, String format) {
		Date date = null;
		try {
			if (value == null || !value.matches("(\\d{4})/(\\d{1,2})/(\\d{1,2})"))
				return false;
			String split[] = value.split("/");
			String month = Integer.valueOf(split[1]) < 10 && split[1].length() < 2 ? "0" + split[1] : split[1];
			String day = Integer.valueOf(split[2]) < 10 && split[2].length() < 2 ? "0" + split[2] : split[2];
			String newValue = split[0] + "/" + month + "/" + day;
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			sdf.setLenient(false);
			date = sdf.parse(newValue);
			if (!newValue.equals(sdf.format(date))) {
				date = null;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return date != null;
	}

	/**
	 * Check empty
	 * 
	 * @param h
	 * @return
	 */
	public static boolean isEmpty(Hashtable<?, ?> h) {
		if (h == null || h.size() == 0) {
			return true;
		}
		return false;
	}

	public static boolean isEmpty(Object[] o) {
		if (o == null || o.length == 0) {
			return true;
		}
		return false;
	}

	public static boolean isEmpty(Vector<?> v) {
		if (v == null || v.size() == 0) {
			return true;
		}
		return false;
	}

	public static boolean isEmpty(String s) {
		if (s == null || s.trim().equalsIgnoreCase("")) {
			return true;
		}
		return false;
	}

	public static boolean isEmpty(List<?> l) {
		if (l == null || l.size() == 0) {
			return true;
		}
		return false;
	}

	public static boolean isEmpty(Set<?> s) {
		if (s == null || s.size() == 0) {
			return true;
		}
		return false;
	}

	public static boolean isEmpty(Map<?, ?> m) {
		if (m == null || m.size() == 0) {
			return true;
		}
		return false;
	}
	
	public static boolean isEmpty(Elements elements) {
		if (elements == null || elements.size() == 0) {
			return true;
		}
		return false;
	}

	/**
	 * Check equal
	 * 
	 * @param str1
	 * @param str2
	 * @return
	 */
	public static boolean isEqual(String str1, String str2) {
		boolean b1 = str1 == null;
		boolean b2 = str2 == null;
		if (b1 && b2) {
			return true;
		}
		if ((b1 & !b2) || (!b1 & b2)) {
			return false;
		}
		return str1.equals(str2);
	}

	/**
	 * Check digit string
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isDigitString(String str) {
		if (CommonUtil.isEmpty(str)) {
			return false;
		}
		char[] arrChar = str.toCharArray();
		for (int i = 0; i < arrChar.length; i++) {
			if (!Character.isDigit(arrChar[i])) {
				return false;
			}
		}
		return true;
	}

	public static boolean isNumber(String str) {
		if (CommonUtil.isEmpty(str))
			return false;
		return NumberUtils.isNumber(str);
	}
	
	public static boolean isLinkStream(String link){
		if(isEmail(link)) return false;
		Pattern pattern;
		if (link.contains("twitch")) {
			pattern = Pattern.compile(".*\\?channel=(.*?)\".*", Pattern.DOTALL);
		} else {
			pattern = Pattern.compile(".*src=\"(.*?)\".*");
		}
		Matcher m = pattern.matcher(link);
		if (m.matches()) {
			return true;
		}
		return false;
	}
	
	public static boolean isUrl(String url){
		if(isEmpty(url)) return false;
		try {
			UrlValidator urlValid = new UrlValidator();
			return urlValid.isValid(url);
		} catch (Exception e) {
		}
		return false;
	}
	
	public static boolean isURL(String url)
	  {
	    if (url == null) {
	      return false;
	    }
	    String urlPattern = "^http(s{0,1})://[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*";
	    return url.matches(urlPattern);
	  }

	/**
	 * Create random long
	 * 
	 * @return
	 */
	public static Long randomLongNumber() {
		Long rs = 0L;
		Double d = Math.random();
		rs = Math.round(d * 1000000000000000L);
		return rs;
	}

	/**
	 * Convert So sang string
	 * 
	 * @param bigDecimal
	 * @param format
	 * @return
	 */
	public static String formatNumber(BigDecimal bigDecimal, String format) {
		NumberFormat numberFormat = new DecimalFormat(format);
		return numberFormat.format(bigDecimal);
	}

	/**
	 * Replace blank spaces
	 * 
	 * @param s
	 * @return
	 */
	public static String trimBlankSpaces(String s) {
		if (s == null) {
			return null;
		}
		s = s.trim();
		s = s.replaceAll("\\s+", " ");
		return s;
	}

	/**
	 * Return default value if object is null
	 * 
	 * @param value
	 * @param nullValue
	 * @param notNullValue
	 * @return
	 */
	public static Long NVL(Long value, Long nullValue, Long notNullValue) {
		return value == null ? nullValue : notNullValue;
	}

	public static Long NVL(Long value, Long defaultValue) {
		return NVL(value, defaultValue, value);
	}

	public static Long NVL(Long value) {
		return NVL(value, 0L);
	}

	public static Number NVL(Number value, Number nullValue, Number notNullValue) {
		return value == null ? nullValue : notNullValue;
	}

	public static Number NVL(Number value, Number defaultValue) {
		return NVL(value, defaultValue, value);
	}

	public static Number NVL(Number value) {
		if (value instanceof Long) {
			return NVL(value, 0L);
		} else if (value instanceof Double) {
			return NVL(value, 0D);
		}
		return NVL(value, 0);
	}

	public static Double NVL(Double value, Double nullValue, Double notNullValue) {
		return value == null ? nullValue : notNullValue;
	}

	public static Double NVL(Double value, Double defaultValue) {
		return NVL(value, defaultValue, value);
	}

	public static Double NVL(Double value) {
		return NVL(value, new Double(0));
	}

	public static String NVL(String value, String nullValue, String notNullValue) {
		return value == null ? nullValue : notNullValue;
	}

	public static String NVL(String value, String defaultValue) {
		return NVL(value, defaultValue, value);
	}

	public static String NVL(String value) {
		return NVL(value, "");
	}

	public static Object NVL(Object value, Object nullValue, Object notNullValue) {
		return value == null ? nullValue : notNullValue;
	}

	public static Object NVL(Object value, Object defaultValue) {
		return NVL(value, defaultValue, value);
	}

	public static Integer getKeyFromMap(Map<Integer, Object[]> map, Object[] values) throws Exception {
		Set<Integer> keySet = map.keySet();
		for (Integer key : keySet) {
			Object[] vs = map.get(key);
			if (vs.length != values.length)
				continue;
			int length = vs.length;
			boolean valid = true;
			for (int i = 0; i < length; i++) {
				if ((vs[i] != null && values[i] == null) || (vs[i] == null && values[i] != null)
						|| (vs[i] != null && !vs[i].equals(values[i])))
					valid = false;
			}
			if (valid)
				return key;
		}
		throw new Exception("Key not found!");
	}

}
