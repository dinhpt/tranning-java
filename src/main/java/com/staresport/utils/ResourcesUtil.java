package com.staresport.utils;

import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;

import org.seasar.framework.util.ResourceUtil;

import com.staresport.utils.CommonUtil;

/**
 * 
 * @author ChuQuangVi
 *
 */
public class ResourcesUtil {
	public static final String APP_CONFIG = "/properties/appConfig.properties";
	public static final String MASSAGE = "/properties/message.properties";
	public static final String SYS = "/properties/systemConfig.properties";
	public static final String SCREEN_ID = "/properties/screenId.properties";
	public static String getResource(String properties, String key, Object... args){
		String msg = ResourceUtil.getProperties(properties).getProperty(key);
		try {
			if(CommonUtil.isEmpty(msg)) return null;
			msg = new String(msg.getBytes("ISO-8859-1"), "UTF-8");
			return MessageFormat.format(msg, args);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
}
