package com.staresport.utils;

import java.util.Random;

/**
 * 
 * @author ChuQuangVi
 *
 */
public class RandomStringGenerator {
	private static final String CAPTCHA_CHARACTER = "abcdefghijklmnopqrstuvwxyz0123456789";
	private static final int DEFAULT_CAPTCHA_LENGTH = 6;
	private final Random rn = new Random();
	private int length = 0;

	public RandomStringGenerator() {
	}

	public RandomStringGenerator(int length) {
		if (length <= 0)
			throw new IllegalArgumentException(
					"Length cannot be less than or equal to 0");
		this.length = length;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	/**
	 * Get random CAPTCHA
	 * 
	 * @return
	 */
	public String getRandomCaptcha() {
		int defaultCaptchaLength = this.length > 0 ? this.length
				: DEFAULT_CAPTCHA_LENGTH;
		int captchaLength = defaultCaptchaLength;
		StringBuilder sb = new StringBuilder(captchaLength);
		for (int i = 0; i < captchaLength; i++) {
			sb.append(CAPTCHA_CHARACTER.charAt(rn.nextInt(CAPTCHA_CHARACTER
					.length())));
		}
		return sb.toString();
	}
}
