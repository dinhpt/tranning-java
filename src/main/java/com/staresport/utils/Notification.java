/*
 * Copyright 2004-2009 the Seasar Foundation and the Others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
package com.staresport.utils;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.seasar.struts.enums.SaveType;
import org.seasar.struts.util.ActionMessagesUtil;

import com.staresport.utils.AppConstants;
import com.staresport.utils.ResourcesUtil;

/**
 * アクションメッセージ用の例外です。
 * 
 * @author higa
 * 
 */
public class Notification extends RuntimeException {

	// PROPERTY ERROR SHOW ON HTML
	
    private static final long serialVersionUID = 1L;

    /**
     * アクションメッセージの集合です。
     */
    protected static ActionMessages messages = new ActionMessages();
    protected static ActionMessages errors = new ActionMessages();

    /**
     * エラーメッセージをどこに保存するかです。
     */
    protected SaveType saveErrors = SaveType.REQUEST;

    /**
     * インスタンスを構築します。
     * 
     * @param key
     *            キー
     * @param values
     *            値の配列
     */
    public Notification(String key, Object... values) {
        addMessage(key, values);
    }

    /**
     * インスタンスを構築します。
     * 
     * @param key
     *            キー
     * @param resource
     *            リソースかどうか
     */
    public Notification(String key, boolean resource) {
        addMessage(key, resource);
    }

    /**
     * アクションメッセージの集合を返します。
     * 
     * @return アクションメッセージの集合
     */
    public ActionMessages getMessages() {
        return messages;
    }

    /**
     * メッセージを追加します。
     * 
     * @param property
     *            プロパティ
     * @param message
     *            メッセージ
     */
    public void addMessage(String property, ActionMessage message) {
        messages.add(property, message);
    }

    /**
     * メッセージを追加します。
     * 
     * @param key
     *            キー
     * @param values
     *            値の配列
     */
    public void addMessage(String key, Object... values) {
        messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage(key,
                values));
    }

    /**
     * メッセージを追加します。
     * 
     * @param key
     *            キー
     * @param resource
     *            リソースかどうか
     */
    public void addMessage(String key, boolean resource) {
        messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage(key,
                resource));
    }
    
    public static void addMessage(HttpServletRequest request, String property, String key, Object... args) {
    	messages.add(property, new ActionMessage(key, args));
	    ActionMessagesUtil.addMessages(request, messages);
    }
    
    public static void addMsgSuccessDialog(HttpServletRequest request, String key, Object... args) {
    	String msg = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, key, args);
    }

    /**
     * エラーメッセージの保存場所を返します。
     * 
     * @return エラーメッセージの保存場所
     */
    public SaveType getSaveErrors() {
        return saveErrors;
    }

    /**
     * エラーメッセージの保存場所を設定します。
     * 
     * @param saveErrors
     *            エラーメッセージの保存場所
     */
    public void setSaveErrors(SaveType saveErrors) {
        this.saveErrors = saveErrors;
    }

    @Override
    public String getMessage() {
        return messages.toString();
    }
    
    public static void addErrors(HttpServletRequest request, String property, String key, Object... args){
    	errors.add(property, new ActionMessage(key, args));
	    ActionMessagesUtil.addErrors(request, errors);
    }
    
    public static void addErrorsDialog(HttpServletRequest request, String key, Object... args){
    	String msg = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, key, args);
    }
    
    public static void addDeleteDialog(HttpServletRequest request, String key, Object... args){
    	String msg = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, key, args);
    }
    /**
     * 
     * @param request
     * @param property (EX:<html:errors property="error"/> The property is error)
     * @param keyMsg (This key was declared in message.properties)
     * @param args
     */
    public static void addErrorsContent(HttpServletRequest request, String property, String keyMsg, Object... args){
    	String msg = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, keyMsg, args);
    }
    
    public static void showMessageBoxWarning(HttpServletRequest request, String property, String key, Object... args){
    	
    }
    
    public static void showMessageBoxInfo(HttpServletRequest request, String property, String key, Object... args) {
    	
    }
    
    public static void showMessageBoxError(HttpServletRequest request, String property, String key, Object... args) {
    	
    }
    
    public static void showNotificationInfo(HttpServletRequest request, String key, Object... args) {
    	
    }
    
    public static void showNotificationError(HttpServletRequest request, String property, String key, Object... args) {
    	
    }
    
    public static void showNotificationWarning(HttpServletRequest request, String property, String key, Object... args) {
    	
    }
}