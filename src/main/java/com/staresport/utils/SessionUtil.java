package com.staresport.utils;

import javax.servlet.http.HttpSession;

import com.staresport.entity.Users;

public class SessionUtil {

	/**
     * Set SysUser into Session
     * 
     * @param httpSession
     * @param sysUser
     */
    public static void setCountRequest(HttpSession httpSession, int countRequest) {
        httpSession.setAttribute(AppConstants.SESSION_ATTRIBUTE.COUNT_REQUEST, countRequest);
    }

    /**
     * Get SysUser from Session
     * 
     * @param httpSession
     * @return
     */
    public static Integer getCountRequest(HttpSession httpSession) {
        return (Integer) httpSession.getAttribute(AppConstants.SESSION_ATTRIBUTE.COUNT_REQUEST);
    }
    
	/**
     * Set SysUser into Session
     * 
     * @param httpSession
     * @param sysUser
     */
    public static void setSysUser(HttpSession httpSession, Users user) {
        httpSession.setAttribute(AppConstants.SESSION_ATTRIBUTE.USER, user);
    }

    /**
     * Get SysUser from Session
     * 
     * @param httpSession
     * @return
     */
    public static Users getSysUser(HttpSession httpSession) {
        return (Users) httpSession.getAttribute(AppConstants.SESSION_ATTRIBUTE.USER);
    }
}
