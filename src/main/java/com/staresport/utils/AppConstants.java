package com.staresport.utils;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 
 * @author ChuQuangVi
 * 
 */
public class AppConstants {

	public static class ACTION {
		public static final String ACTION_SUFFIX = "Action";
		public static final String LOGIN_ACTION = "LoginAction";
		public static final String MATCH_ACTION = "MatchAction";
	}

	public static class COMMON {
		public static final int CAPTCHA_LENGTH = 5;
		public static final int BATCH_SIZE = 100;
		public static final int MAX_LOGIN_FAILED_TIMES = 3;
	}

	public static class VIEW_STATE {
		public static final int SEARCH = 0;
		public static final int INSERT = 1;
		public static final int UPDATE = 2;
		public static final int INFO = 3;
	}

	public static class VISIBLE {
		public static final String SHOW = "show";
		public static final String HIDE = "hide";
	}

	public static class SPECIAL_CHARACTERS {
		public static final String SEPARATOR = File.separator;
		public static final String HYPHEN = "-";
		public static final String SLASH = "/";
		public static final String EMPTY = "";
		public static final String COLON = ":";
		public static final String COMMA = "\"";
		public static final String SEMICOLON = ";";
		public static final String POUND = "#";
	}

	public static class DATE_FORMAT {
		public static class SLASH_FORMAT {
			public static final String DDMMYYYY = "dd/MM/yyyy";
			public static final String YYYYMMDD = "yyyy/MM/dd";
			public static final String YYMMDD = "yy/MM/dd";
			public static final String DDMMYYYY_TIME = "dd/MM/yyyy HH:mm:ss";
			public static final String MMDDYYYY_TIME = "MM/dd/yyyy HH:mm:ss";
			public static final String YYYYMMDD_TIME = "yyyy/MM/dd HH:mm:ss";
			public static final String DDMMYYYY_TIME_SQL = "dd/mm/yyyy hh24:mi:ss";
		}

		public static class HYPHEN_FORMAT {
			public static final String DDMMYYYY = "dd-MM-yyyy";
			public static final String YYYYMMDD = "yyyy-MM-dd";
			public static final String YYMMDD = "yy-MM-dd";
			public static final String MMYYYY = "MM-yyyy";
			public static final String YYYYMM = "yyyy-MM";
			public static final String DDMMYYYY_TIME = "dd-MM-yyyy HH:mm:ss";
			public static final String YYYYMMDD_TIME = "yyyy-MM-dd HH:mm:ss";
			public static final String DDMMYYYY_TIME_SQL = "dd-mm-yyyy hh24:mi:ss";
		}

		public static class OTHER_FORMAT {
			public static final String YYYYMM = "yyyyMM";
			public static final String YYYYMMDD = "yyyyMMdd";
			public static final String DDMMYYYY = "ddMMyyyy";
			public static final String YYYYMMDD_TIME = "yyyyMMddHHmmss";
			public static final String YYYYMMDD_TIMEZONE = "yyyy MMMMM dd, EEEEE, HH:mm z";
		}
	}

	public static class SESSION_ATTRIBUTE {
		public static final String USER = "user";
		public static final String COUNT_REQUEST = "countRequest";
	}

	public static class GOSUGAMER {
		public static final String BASEURL = "http://www.gosugamers.net";
		public static final String GOSUBET = "gosubet";
		public static final int UPCOMING_RECORD_LIMIT = 15;
		public static final int RESULT_RECORD_LIMIT = 15;
		public static final int RESULT_NUM_PAGE_SYNC = 4;
		public static final String UPCOMING_PAGE = "?u-page=";
		public static final String RESULT_PAGE = "?r-page=";
		public static final int CONNECT_TIMOUT = 80000;
		public static final String TEAM1 = "team1";
		public static final String TEAM2 = "team2";

		public static class GAME_TYPE {
			public static final int DOTA = 1;
			public static final int LOL = 2;
			public static final int FIFA = 3;
			public static final int COUNTERSTRIKE = 4;
			public static final int OVERWATCH = 5;
			public static final int HEROESOFTHESTORM = 6;
			public static final int STARCRAFT2 = 7;

			public static Map<Integer, String> LINK_GAME_MAP = new LinkedHashMap<>();
			static {
				LINK_GAME_MAP.put(DOTA, "dota2");
				LINK_GAME_MAP.put(COUNTERSTRIKE, "counterstrike");
				LINK_GAME_MAP.put(HEROESOFTHESTORM, "heroesofthestorm");
				LINK_GAME_MAP.put(LOL, "lol");
				LINK_GAME_MAP.put(OVERWATCH, "overwatch");
				LINK_GAME_MAP.put(STARCRAFT2, "starcraft2");
			}

			public static Map<Integer, String> NAME_GAME_MAP = new LinkedHashMap<>();
			static {
				NAME_GAME_MAP.put(DOTA, "Dota 2");
				NAME_GAME_MAP.put(COUNTERSTRIKE, "CS-GO");
				NAME_GAME_MAP.put(FIFA, "Fifa online");
				NAME_GAME_MAP.put(HEROESOFTHESTORM, "Heroes of the Storm");
				NAME_GAME_MAP.put(LOL, "League of Legend");
				NAME_GAME_MAP.put(OVERWATCH, "Overwatch");
				NAME_GAME_MAP.put(STARCRAFT2, "StarCraft 2 ");
			}
		}

		public static class STATUS_MATCH {
			public static final String UPCOMING_MATCHES = "Upcoming Matches";
			public static final String RECENT_RESULTS = "Recent Results";
			public static final String LIVE_MATCHES = "Live Matches";
		}

		public static class HTML_TAG {
			public static final String TR = "tr";
			public static final String A = "a";
			public static final String H2 = "h2";
			public static final String H3toA = "h3 > a";
		}

		public static class HTML_ATTRIBUTE {
			public static final String HREF = "href";
			public static final String STYLE = "style";
		}

		public static class CLASS_TAG {
			public static final String MATCH_DETAIL = ".match-heading-overlay";
			public static final String MATCHES = ".simple";
			public static final String INFO_TEAM1 = ".opponent1";
			public static final String INFO_TEAM2 = ".opponent2";
			public static final String INFO_DATETIME = ".match-extras > div.vs p.datetime";
			public static final String RANKED = ".ranked";
			public static final String FLAG = ".flag";
			public static final String TEAM_PLAYER = ".team-player";
			public static final String DIV_BOX = "div.box";
			public static final String STREAM_TAB = ".matches-streams .match-stream-tab";
			public static final String BET_PERCENT = "span.bet-percentage";
			public static final String BET_TEAM1 = ".bet1";
			public static final String BET_TEAM2 = ".bet2";
			public static final String BESTOF = ".bestof";
			public static final String PAGINATOR = "div.paginator > div.viewing";
		}

		public static class STATUS_UPDATE_SCORE {
			public static final int NO = 0;
			public static final int YES = 1;
		}

		public static class FLAG {
			public static final String NO_FLAG = "Other";
		}
	}

	public static class RESULT_STATE {
		public static final int ERROR = -1;
		public static final int NO_ERROR = 0;
		public static final int COMPLETED = 1;
		public static Map<Integer, String> RESULT_STATE_MAP = new LinkedHashMap<>();
		static {
			RESULT_STATE_MAP.put(ERROR, "ERROR");
			RESULT_STATE_MAP.put(NO_ERROR, "NO_ERROR");
			RESULT_STATE_MAP.put(COMPLETED, "COMPLETED");
		}
	}

	public static final class DATABASE_CONNECTION {
		public static final String CONNECTED = "DATABASE CONNECTED ";
		public static final String LOST_CONNECTION = "LOST DATABASE CONNECTION";
	}
	
	public static final class MATCH{
		public static final int ALLOW_BET = 0;
		public static final int NOT_ALLOW_BET = 1;
		public static final class MATCH_STATUS {
			public static final int DEFAULT = 0;
			public static final int RUNNING = 1;
			public static final int CLOSE = 2;
			public static final int PENDING = 3;
			public static final int REJECT = 4;
		}
	}

	public static final class USER_TYPE {
		public static final int ADMIN = 1;
		public static final int PARTNER = 2;
		public static final int VIP = 3;
		public static final int NORMAL = 4;
	}

	public static final class HOST {
		public static final int NUM_HOST = 5;

		public static final class HOST_NOTIFY {
			public static final String HOST_OVER = "host-over";
			public static final String SAVE_FAILED = "save-failed";
			public static final String SAVE_SUCCESS = "save-success";
			public static final String EMPTY_LINK = "link-empty";
			public static final String NOT_LINK = "not-link";
			public static final String USER_HOSTED = "user-hosted";
		}
	}

	public static final class ACCOUNT_TYPE {
		public static final String ADMIN = "admin";
		public static final String PARTNER = "partner";
		public static final String VIP = "vip";
		public static final String NORMAL = "normal";
	}

	public static final class MATCH_LOAD_OPTION {
		public static final int HOUR_LOAD_COMING_TAB = 2;
		public static final int HOUR_LOAD_24H_TAB = 24;
		public static final int LOAD_NEXT_MATCH = 0;// Link ben canh Videostream
		public static final int LOAD_ALL_MATCH = -1; // Tab All
		public static final int LOAD_LIVE_MATCH = 1;
	}
	
	public static final class BET{
		public static final int ALL_MATCH = 0;
		public static final int BO_MATCH = 1;
	}
	
	public static final class TIME{
		public static final String SECOND = "SECOND";
		public static final String MINUTE = "MINUTE";
		public static final String HOUR = "HOUR";
		public static final String DAY = "DAY";
		public static final String WEEK = "WEEK";
		public static final String MONTH = "MONTH";
	}
	
	public static final class NUMBER_STRING{
		public static final String ZERO = "0";
		public static final String ONE = "1";
	}
}
