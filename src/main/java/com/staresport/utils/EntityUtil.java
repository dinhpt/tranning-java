package com.staresport.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.PropertyUtilsBean;

/**
 * 
 * @author ChuQuangVi
 *
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class EntityUtil {
	public static void copyProperties(Object dest, Object src) throws Exception {
        BeanUtilsBean beanUtilsBean = getUtilsBean();
        beanUtilsBean.copyProperties(dest, src);
    }
	
	public static void copyPropertyList(List<Object> destList, List<Object> srcList) throws Exception {
		if(CommonUtil.isEmpty(srcList)) return;
		for(int i=0; i< srcList.size(); i++){
			copyProperties(destList.get(i), srcList.get(i));
		}
    }
	
	
	public static BeanUtilsBean getUtilsBean() {
        ConvertUtilsBean convertUtilsBean = new ConvertUtilsBean();
        BeanUtilsBean beanUtilsBean = new BeanUtilsBean(convertUtilsBean,
                new PropertyUtilsBean());
        return beanUtilsBean;
    }
	
	public static <T> String getEntityName(Class<T> obj) {
        try {
            String name = new String();
            Table table = obj.getAnnotation(Table.class);
            if (table != null) {
                name = table.name();
            }
            return name;
        } catch (Exception e) {
        	e.getMessage();
        }
        return new String();
    }
	
	public static void setIdValue(Object dest, Object src){
		try {
            String name = "";
            Object value = null;
            Field[] fields = src.getClass().getDeclaredFields();
            for (Field f : fields) {
                Id a = f.getAnnotation(Id.class);
                if (a != null) {
                    name = f.getName();
                    value = getFieldValue(src, name);
                    break;
                }
            }
            if(CommonUtil.isEmpty(name)) return;
            setPropertyValue(dest, name, value);
        } catch (Exception e) {
            e.getMessage();
        }
	}
	
	public static String getIdName(Object o) {
        try {
            String name = "";
            o.getClass().getMethods();
            Field[] fields = o.getClass().getDeclaredFields();
            for (Field f : fields) {
                Id a = f.getAnnotation(Id.class);
                if (a != null) {
                    name = f.getName();
                    break;
                }
            }
            return name;
        } catch (Exception e) {
            e.getMessage();
        }
        return new String();
    }
	
	public static Object getFieldValue(Object o, String name) {
        try {
            Object value = null;
            Method[] methods = o.getClass().getMethods();
            for (Method m : methods) {
            	if(m.getName().equalsIgnoreCase("get" + name)){
            		value = (Object) m.invoke(o);
            		break;
            	}
            }
            return value;
        } catch (Exception e) {
            e.getMessage();
        }
        return null;
    }
	
	public static void setPropertyValue(Object o, String name, Object value) {
        try {
        	Method[] methods = o.getClass().getMethods();
            for (Method m : methods) {
                if (m.getName().equalsIgnoreCase("set" + name)) {
                    m.invoke(o, value);
                    break;
                }
            }
        } catch (Exception e) {
        	e.getMessage();
        }
    }
	
	public static Object getIdValue(Object o) {
        try {
        	 String name = "";
             Object value = null;
             Field[] fields = o.getClass().getDeclaredFields();
             for (Field f : fields) {
                 Id a = f.getAnnotation(Id.class);
                 if (a != null) {
                	 name = f.getName();
                     value = getFieldValue(o, name);
                     break;
                 }
             }
            return value;
        } catch (Exception e) {
            e.getMessage();
        }
        return null;
    }
	
	/**
     * Reset Object
     * 
     * @param obj
     */
    public static void reset(Object obj) {
    	try {
    		Map map1 = BeanUtils.describe(obj);
            BeanUtilsBean beanUtilsBean = getUtilsBean();
            Map<String, String> map2 = new HashMap<String, String>();
            Set<Entry<String, String>> entrySet = map1.entrySet();
            for (Entry<String, String> e : entrySet) {
                map2.put(e.getKey(), null);
            }
            beanUtilsBean.populate(obj, map2);
		} catch (Exception e) {
			e.getMessage();
		}
        
    }
    
    public static <T extends Object> List<T> cloneList(List<T> list) {
        return ((List<T>) ((ArrayList<T>) list).clone());
    }
    
	public static <T> T deepCloneObject(T source) {
		try {
			if (source == null) {
				return null;
			}
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream out = new ObjectOutputStream(bos);
			out.writeObject(source);
			out.flush();
			out.close();
			ObjectInputStream in = new ObjectInputStream(
					new ByteArrayInputStream(bos.toByteArray()));
			T dto = (T) in.readObject();
			in.close();
			return dto;
		} catch (IOException | ClassNotFoundException e) {
			return null;
		}
	}
	
	public static <T> List<T> deepCloneObjects(List<T> source) {
		if(CommonUtil.isEmpty(source)) return null;
		List<T> list = new ArrayList<>();
		try {
			for(T obj : source){
				list.add(deepCloneObject(obj));
			}
			return list;
		} catch (Exception e) {
			e.getMessage();
		}
		return null;
	}
}
