package com.staresport.utils;

import com.staresport.dto.PermissionDto;
import com.staresport.dto.UserDto;

public class PermissionUtil {
	
	public static void permission(UserDto user){
		if(user==null) return;
		String typeCode = user.getTypeCode();
		if(CommonUtil.isEmpty(typeCode)) return;
		PermissionDto permission = user.getPermission();
		if(typeCode.equalsIgnoreCase(AppConstants.ACCOUNT_TYPE.ADMIN)){
			setViewAdmin(permission);
		}else if(typeCode.equalsIgnoreCase(AppConstants.ACCOUNT_TYPE.PARTNER)){
			setViewPartner(permission);
		}else if(typeCode.equalsIgnoreCase(AppConstants.ACCOUNT_TYPE.VIP)){
			setViewVip(permission);
		}else if(typeCode.equalsIgnoreCase(AppConstants.ACCOUNT_TYPE.NORMAL)){
			setViewNormal(permission);
		}
	}
	
	private static void setViewAdmin(PermissionDto permission){
		permission.setViewHistory(true);
		permission.setViewMatch(true);
		permission.setViewNotiEndMatch(true);
	}
	
	private static void setViewPartner(PermissionDto permission){
		permission.setViewHistory(true);
		permission.setViewMatch(true);
		permission.setViewNotiEndMatch(false);
	}
	
	private static void setViewVip(PermissionDto permission){
		permission.setViewHistory(true);
		permission.setViewMatch(true);
		permission.setViewNotiEndMatch(false);
	}
	
	private static void setViewNormal(PermissionDto permission){
		permission.setViewHistory(true);
		permission.setViewMatch(false);
		permission.setViewNotiEndMatch(false);
	}
}
