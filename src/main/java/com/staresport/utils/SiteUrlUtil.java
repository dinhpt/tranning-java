package com.staresport.utils;


/**
 * URLユーティリティ
 *
 * @author dinhpt
 */
public class SiteUrlUtil {

	/** ログイン必要URLパターン */
	//private static final String LOGIN_REQUIRED = ".*/(mypage|order|cancel|favorite|enquete|inquiry).*";

	/**
	 * セキュアhttpプロトコルを返します。
	 *
	 * @return
	 */
	public static String getSecureProtocol() {
		return ResourcesUtil.getResource(ResourcesUtil.SYS, "secure.protocol");
	}

	/**
	 * サイトのドメインを返します。
	 *
	 * @return
	 */
	public static String getSiteDomain() {
		return ResourcesUtil.getResource(ResourcesUtil.SYS, "site.domain");
	}

	/**
	 * IndexのURL
	 *
	 * @return
	 */
	public static String getIndexUrl() {
		return ResourcesUtil.getResource(ResourcesUtil.SYS, "site.root.url");
	}

	/**
	 * LoginのURL
	 *
	 * @return
	 */
	public static String getLoginUrl() {
		return getSecureProtocol() + "://" + getSiteDomain() + "/login/";
	}

}
