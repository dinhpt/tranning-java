package com.staresport.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.seasar.struts.util.ResponseUtil;

/**
 * 
 * @author ChuQuangVi
 *
 */
public class FileUtil {
	public static final String UPLOAD_FOLDER_APP = ResourcesUtil.getResource(
			ResourcesUtil.APP_CONFIG, "folder.fileUpload");
	public static final String EXPORT_FOLDER_APP = ResourcesUtil.getResource(
			ResourcesUtil.APP_CONFIG, "folder.export");
	//public static final String EXPORT_FOLDER_REPORT = ResourcesUtil.getResource(ResourcesUtil.APP_CONFIG, "folder.fileUpload.report");
	private static Logger LOGGER = Logger.getLogger(FileUtil.class);

	/**
	 * Get name of file
	 * 
	 * @param filePath
	 * @return
	 */
	public static String getFileName(String filePath) {
		if (filePath == null) {
			return new String();
		}
		File file = new File(filePath);
		return file.getName();
	}

	/**
	 * Get path of file's folder
	 * 
	 * @param filePath
	 * @return
	 */
	public static String getFolderPath(String filePath) {
		if (filePath == null) {
			return new String();
		}
		return filePath.substring(0, filePath.length()
				- getFileName(filePath).length());
	}

	/**
	 * Get extension of file
	 * 
	 * @param fileName
	 * @return
	 */
	public static String getExtension(String fileName) {
		if (fileName == null) {
			return new String();
		}
		int dotIdx = fileName.lastIndexOf(".");
		if (dotIdx <= 0) {
			return new String();
		}
		return fileName.substring(dotIdx + 1, fileName.length()).toLowerCase();
	}

	/**
	 * Get file name without extension
	 * 
	 * @param fileName
	 * @return
	 */
	public static String getFileNameWithouExtension(String fileName) {
		if (fileName == null) {
			return new String();
		}
		int dotIdx = fileName.lastIndexOf(".");
		if (dotIdx <= 0) {
			return fileName;
		}
		return fileName.substring(0, dotIdx);
	}

	/**
	 * generate random file name
	 * 
	 * @param fileName
	 * @return
	 */
	public static String getRandomFileName(String fileName) {
		UUID uuid = UUID.randomUUID();
		return uuid.toString() + "." + getExtension(fileName);
	}

	/**
	 * 
	 * @param path
	 * @return
	 */
	public static String getSafePath(String path) {
		if (path == null) {
			return null;
		}
		return path.replace("\\", "/").replace("//", "/");
	}

	/**
	 * Store file in server APP
	 * 
	 * @param uploadFolder
	 * @param fileName
	 * @param inputBytes
	 */
	public static String uploadFile(String path, String fileName,
			byte[] inputBytes) throws Exception {
		OutputStream outputStream = null;
		String filePath = "";
		try {
			filePath = getSafePath(path + File.separator);
			File file = new File(filePath);
			if (!file.exists()) {
				file.mkdirs();
			}
			filePath = getSafePath(filePath + File.separator + fileName);
			file = new File(filePath);
			outputStream = new FileOutputStream(file);
			outputStream.write(inputBytes);
			outputStream.flush();
			return fileName;
		} catch (Exception e) {
			LOGGER.error("Error on uploading file to application server", e);
			throw e;
		} finally {
			try {
				if (outputStream != null) {
					outputStream.close();
				}
			} catch (Exception e2) {
				LOGGER.error("Error on closing output stream", e2);
			}
		}
	}
	
	/**
     * Upload file to application server with encrypted file name
     * 
     * @param uploadFolder
     * @param fileName
     * @param inputBytes
     * @throws Exception
     */
    public static String uploadFileNameEncrypted(String path, String fileName, byte[] inputBytes) throws Exception {
        String fileNameNoExtension = FileUtil.getFileNameWithouExtension(fileName);
        String fileExtension = FileUtil.getExtension(fileName);
        String fileNameEncrypted = ConverterUtil.toString(new Date(), AppConstants.DATE_FORMAT.OTHER_FORMAT.YYYYMMDD_TIME) 
        		+ "_" + fileNameNoExtension + (CommonUtil.isEmpty(fileExtension) ? "" : ("." + fileExtension));
        return uploadFile(path, fileNameEncrypted, inputBytes);
    }

	/**
	 * Delete file from server APP
	 * 
	 * @param filePath
	 * @return
	 * @throws Exception
	 */
	public static boolean deleteFile(String filePath) throws Exception {
		try {
			filePath = getSafePath(UPLOAD_FOLDER_APP + File.separator
					+ filePath);
			File file = new File(filePath);
			if (file != null && file.isFile() && file.exists() && file.delete()) {
				return true;
			}
			return false;
		} catch (Exception e) {
			LOGGER.error("Error on delete file", e);
			throw e;
		}
	}

	/**
	 * Delete file from server APP
	 * 
	 * @param absoluteFilePath
	 * @return
	 * @throws Exception
	 */
	public static boolean deleteFile_AbsolutePath(String absoluteFilePath)
			throws Exception {
		try {
			File file = new File(absoluteFilePath);
			if (file != null && file.isFile() && file.exists() && file.delete()) {
				return true;
			}
			return false;
		} catch (Exception e) {
			LOGGER.error("Error on delete file", e);
			throw e;
		}
	}

	/**
	 * 
	 * @param filePath
	 * @return
	 */
	public static byte[] readFile(String filePath) {
		try {
			InputStream inputStream = new FileInputStream(new File(
					UPLOAD_FOLDER_APP + filePath));
			return IOUtils.toByteArray(inputStream);
		} catch (IOException e) {
			LOGGER.error(null, e);
			return new byte[0];
		}
	}
	
	public static void download(String fileUrl, String fileName) throws Exception {
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(new File(fileUrl));
			fileName += "_" + new Date().getTime() + ".pdf";
			ResponseUtil.download(new String(fileName), IOUtils.toByteArray(inputStream));
			inputStream.close();
		} catch (Exception e) {
		}

	}
}
