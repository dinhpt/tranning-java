package com.staresport.service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.staresport.bean.UserBetJson;
import com.staresport.dto.BetDto;
import com.staresport.dto.MatchDto;
import com.staresport.dto.SubMatchsDto;
import com.staresport.dto.UserBetDetailDto;
import com.staresport.dto.UserBetDto;
import com.staresport.entity.UserBetDetail;
import com.staresport.entity.UserBets;
import com.staresport.utils.AppConstants;
import com.staresport.utils.CommonUtil;
import com.staresport.utils.DateUtil;
import com.staresport.utils.EntityUtil;
import com.staresport.utils.UserBetUtil;

public class BetService extends AbstractService<Object> {
	
	public MatchDto getMatchById(Long matchId) {
		if (matchId == null)
			return null;
		StringBuilder query = new StringBuilder();
		query.append(
				"SELECT m.*, t1.name team1Name, t1.avatar team1Avatar, m.team1_bet_per team2BetPer, t1.flag team1Flag, t1.ranked team1Ranked, "
						+ "   t2.name team2Name, t2.avatar team2Avatar, m.team2_bet_per team1BetPer, t2.flag team2Flag, t2.ranked team2Ranked,"
						+ "   uh.stream linkStream, "
						+ "   (CASE WHEN m.date_time > NOW() THEN true ELSE false END) AS isLive,"
						+ "   sm.sub_match_id, sm.no_match, sm.bet_type," + "   ub.user_bet_id, ub.all_bets "
						+ "  FROM matchs m  " + "    LEFT JOIN USER_HOSTEDS uh ON m.match_id = uh.match_id "
						+ "    JOIN TEAMS t1 ON t1.team_id = m.team1_id "
						+ "    JOIN TEAMS t2 ON t2.team_id = m.team2_id "
						+ "    JOIN SUB_MATCHS sm ON sm.match_id = m.match_id"
						+ "    LEFT JOIN (SELECT ub.user_bet_id, ub.match_id, ub.all_bets FROM USER_BETS ub WHERE ub.match_id = ?) ub "
						+ "     ON ub.match_id = m.match_id " + "  WHERE m.match_id = ? "
						+ "  ORDER BY sm.bet_type ASC");
		params = new ArrayList<Object>();
		params.add(matchId);
		params.add(matchId);
		List<MatchDto> matchs = jdbcManager.selectBySql(MatchDto.class, query.toString(), params.toArray())
				.getResultList();
		MatchDto match = new MatchDto();
		if (CommonUtil.isEmpty(matchs))
			return match;
		match = matchs.get(0);
		int size = matchs.size();
		List<SubMatchsDto> subMatchs = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			MatchDto dto = matchs.get(i);
			SubMatchsDto subMatch = new SubMatchsDto();
			subMatch.setSubMatchId(dto.getSubMatchId());
			subMatch.setNoMatch(dto.getNoMatch());
			subMatch.setBetType(dto.getBetType());
			subMatchs.add(subMatch);
		}
		match.setSubMatchs(subMatchs);
		return match;
	}
	
	public MatchDto getMatchUserBet(Integer matchId, Integer userHostedId, Integer userBetId){
		if(matchId == null || userBetId == null) return null;
		boolean isBeted = isUserBeted(matchId, userHostedId, userBetId);
		if(!isBeted){
			return getMatchUserNotBet(matchId, userHostedId);
		}else{
			return getMatchUserBeted(matchId, userHostedId, userBetId);
		}
	}

	public MatchDto getMatchUserNotBet(Integer matchId, Integer userHostedId) {
		StringBuilder query = new StringBuilder();
		params = new ArrayList<Object>();
		String selectUserHost = "";
		String queryUserHost = "";
		if (userHostedId != null) {
			selectUserHost = " uh.stream, uh.user_id uhosted_id,(SELECT u.name FROM USERS u WHERE u.user_id = uh.user_id) uhosted_name, ";
			queryUserHost = " LEFT JOIN (SELECT uh.user_id, uh.match_id, uh.stream FROM USER_HOSTEDS uh WHERE uh.user_id = ?) uh ON m.match_id = uh.match_id ";
			params.add(userHostedId);
		}
		query.append("SELECT m.*,t1.name team1Name,t1.avatar team1Avatar,t1.flag team1Flag,t1.ranked team1Ranked, "
				+ "      t2.name team2Name,t2.avatar team2Avatar,t2.flag team2Flag,t2.ranked team2Ranked, "
				+ "      uh.stream linkStream,(CASE WHEN m.date_time > NOW() THEN TRUE ELSE FALSE END) AS isLive, "
				+ 		  selectUserHost 
				+ "       sm.sub_match_id,sm.no_match,sm.bet_type, " 
				+"       (SELECT sm1.team1_bet_money "
				+"           FROM sub_matchs sm1 "
                +"           WHERE sm1.match_id = m.match_id  "
                +"             AND sm.sub_match_id = sm1.sub_match_id "
                +"             AND sm1.team1_Id = t1.team_id) AS money_team1_bo, "
                +"       (SELECT sm2.team2_bet_money "
                +"           FROM sub_matchs sm2 "
                +"           WHERE     sm2.match_id = m.match_id  "
                +"            AND sm.sub_match_id = sm2.sub_match_id "
                +"            AND sm2.team2_Id = t2.team_id) AS money_team2_bo"
				+ "     FROM matchs m " 
				+         queryUserHost
				+ "       JOIN TEAMS t1 ON t1.team_id = m.team1_id "
				+ "       JOIN TEAMS t2 ON t2.team_id = m.team2_id "
				+ "       JOIN SUB_MATCHS sm ON sm.match_id = m.match_id " 
				+ "     WHERE m.match_id = ? "
				+ "ORDER BY sm.bet_type ASC ");
		params.add(matchId);
		List<MatchDto> matchs = jdbcManager.selectBySql(MatchDto.class, query.toString(), params.toArray())
				.getResultList();
		MatchDto match = new MatchDto();
		if (CommonUtil.isEmpty(matchs))
			return match;
		match = matchs.get(0);
		int size = matchs.size();
		List<UserBetDetailDto> detailDtos = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			MatchDto dto = matchs.get(i);
			UserBetDetailDto detailDto = setDataBetDetail(dto);
			detailDtos.add(detailDto);
		}
		match.setBetDetailList(detailDtos);
		return match;
	}

	public MatchDto getMatchUserBeted(Integer matchId, Integer uhostedId, Integer userBetId) {
		if (matchId == null || userBetId == null || uhostedId == null)
			return null;
		StringBuilder query = new StringBuilder();
		params = new ArrayList<Object>();
		query.append("SELECT m.*, "
				+"       (CASE WHEN m.date_time > NOW() THEN TRUE ELSE FALSE END) AS isLive, "
				+"       t1.name team1Name,t1.avatar team1Avatar,t1.flag team1Flag, "
				+"       t1.ranked team1Ranked,t2.name team2Name,t2.avatar team2Avatar, "
				+"       t2.flag team2Flag,t2.ranked team2Ranked,uh.stream,uh.user_id uhosted_id, "
				+"       (SELECT u.name FROM USERS u WHERE u.user_id = uh.user_id) uhosted_name, "
				+"       ub.user_id user_bet_id,ub.result,ub.bet_bestof, "
				+"       (SELECT SUM(bet_money) "
				+"          FROM user_bet_detail ubd "
				+"         WHERE     ubd.user_id = ub.user_id "
				+"               AND ubd.match_id = m.match_id "
				+"               AND ubd.uhosted_id = uh.user_id "
				+"               AND ubd.bet_team_id = t1.team_id) "
				+"          AS ubet_money_team1, "
				+"       (SELECT SUM(sm.team1_bet_money) "
				+"          FROM sub_matchs sm "
				+"         WHERE sm.team1_Id = t1.team_id AND sm.match_id = m.match_id) "
				+"          AS total_money_team1, "
				+"       (SELECT SUM(bet_money) "
				+"          FROM user_bet_detail ubd "
				+"         WHERE     ubd.user_id = ub.user_id "
				+"               AND ubd.match_id = m.match_id "
				+"               AND ubd.uhosted_id = uh.user_id "
				+"               AND ubd.bet_team_id = t2.team_id) "
				+"          AS ubet_money_team2, "
				+"       (SELECT SUM(sm.team2_bet_money) "
				+"          FROM sub_matchs sm "
				+"         WHERE sm.team2_Id = t2.team_id AND sm.match_id = m.match_id) "
				+"          AS total_money_team2, "
				+"        ubd.*, "
				+"       (SELECT sm.team1_bet_money "
				+"           FROM sub_matchs sm "
                +"           WHERE sm.match_id = m.match_id  "
                +"             AND ubd.sub_match_id = sm.sub_match_id "
                +"             AND sm.team1_Id = t1.team_id) AS money_team1_bo, "
                +"       (SELECT sm.team2_bet_money "
                +"           FROM sub_matchs sm "
                +"           WHERE     sm.match_id = m.match_id  "
                +"            AND ubd.sub_match_id = sm.sub_match_id "
                +"            AND sm.team2_Id = t2.team_id) AS money_team2_bo,"
                +"       (SELECT ubd1.bet_money "
				+"          FROM user_bet_detail ubd1 "
				+"         WHERE     ubd1.user_id = ub.user_id "
				+"               AND ubd1.match_id = m.match_id "
				+"               AND ubd1.sub_match_id = ubd.sub_match_id "
				+"               AND ubd1.uhosted_id = uh.user_id "
				+"               AND ubd1.bet_team_id = t1.team_id) "
				+"          AS ubd_money_team1, "
				+"       (SELECT ubd2.bet_money "
				+"          FROM user_bet_detail ubd2 "
				+"         WHERE     ubd2.user_id = ub.user_id "
				+"               AND ubd2.match_id = m.match_id "
				+"               AND ubd2.uhosted_id = uh.user_id "
				+"               AND ubd2.sub_match_id = ubd.sub_match_id "
				+"               AND ubd2.bet_team_id = t2.team_id) "
				+"          AS ubd_money_team2 "
				+"  FROM matchs m "
				+"       LEFT JOIN (SELECT uh.user_id, uh.match_id, uh.stream "
				+"                    FROM USER_HOSTEDS uh "
				+"                   WHERE uh.user_id = ?) uh "
				+"          ON m.match_id = uh.match_id "
				+"       JOIN TEAMS t1 ON t1.team_id = m.team1_id "
				+"       JOIN TEAMS t2 ON t2.team_id = m.team2_id "
				+"       JOIN (SELECT * FROM user_bets ub "
				+"              WHERE ub.user_id = ? AND ub.uhosted_id = ?) ub ON ub.match_id = m.match_id "
				+"       JOIN (SELECT * FROM user_bet_detail ubd  "
				+"             WHERE ubd.match_id = ? "
				+"             AND ubd.uhosted_id = ? AND ubd.user_id = ?) ubd ON m.match_id = ubd.match_id"
				+" WHERE m.match_id = ? ");
		params.add(uhostedId);
		params.add(userBetId);
		params.add(uhostedId);
		params.add(matchId);
		params.add(uhostedId);
		params.add(userBetId);
		params.add(matchId);
		List<MatchDto> matchs = jdbcManager.selectBySql(MatchDto.class, query.toString(), params.toArray()).getResultList();
		if(CommonUtil.isEmpty(matchs)) return null;
		MatchDto matchDto = new MatchDto();
		List<UserBetDetailDto> detailDtos = new ArrayList<>();
		int size = matchs.size();
		try {
			for(int i=0; i < size; i++){
				MatchDto match = matchs.get(i);
				if(i==0) matchDto = match;
				UserBetDetailDto detailDto = setDataBetDetail(match);
				detailDtos.add(detailDto);
			}
			matchDto.setBetDetailList(detailDtos);
		} catch (Exception e) {
			e.getMessage();
		}
		statisticProfit(matchDto, detailDtos);
		return matchDto;
	}
	
	private UserBetDetailDto setDataBetDetail(MatchDto match){
		UserBetDetailDto detailDto = new UserBetDetailDto();
		try {
			EntityUtil.copyProperties(detailDto, match);
			Double ubet1 = match.getUbdMoneyTeam1();
			Double ubet2 = match.getUbdMoneyTeam2();
			Double totUbet1 = match.getMoneyTeam1Bo();
			Double totUbet2 = match.getMoneyTeam2Bo();
			BetDto betDto = UserBetUtil.caculateRate(ubet1, ubet2, totUbet1, totUbet2);
			detailDto.setPctgTeam1(betDto.getPctgTeam1());
			detailDto.setPctgTeam2(betDto.getPctgTeam2());
			detailDto.setRateTeam1(betDto.getRateTeam1());
			detailDto.setRateTeam2(betDto.getRateTeam2());
			detailDto.setProfitTeam1(betDto.getProfitTeam1());
			detailDto.setProfitTeam2(betDto.getProfitTeam2());
		} catch (Exception e) {
			e.getMessage();
		}
		return detailDto;
	}
	
	private void statisticProfit(MatchDto match, List<UserBetDetailDto> detailDtos){
		if(CommonUtil.isEmpty(detailDtos)) return;
		Integer profit1 = 0;
		Integer profit2 = 0;
		for(UserBetDetailDto dto : detailDtos){
			profit1 += dto.getProfitTeam1();
			profit2 += dto.getProfitTeam2();
		}
		match.setMoneyWinTeam1(String.valueOf(profit1));
		match.setMoneyWinTeam2(String.valueOf(profit2));
	}

	/**
	 * CHECK USER BETED THE MATCH
	 * 
	 * @param matchId
	 * @param userId
	 * @return
	 */
	public boolean isUserBeted(Integer matchId, Integer userHostedId, Integer userBetId) {
		if(matchId==null || userHostedId==null || userBetId==null) return false;
		params = new ArrayList<>();
		String query = "SELECT ub.user_id FROM USER_BETS ub "
				+ "	WHERE ub.user_id = ? AND ub.match_id = ? AND ub.uhosted_id = ?";
		params.add(userBetId);
		params.add(matchId);
		params.add(userHostedId);
		long count = jdbcManager.getCountBySql(query, params.toArray());
		return count > 0;
	}

	public boolean validMatch(Integer matchId, Integer subMatchId) {
		if (matchId == null || subMatchId == null)
			return false;
		String query = "SELECT sm.sub_match_id FROM SUB_MATCHS sm " + " WHERE sm.match_id = ? AND sm.sub_match_id = ? ";
		params = new ArrayList<>();
		params.add(matchId);
		params.add(subMatchId);
		long count = jdbcManager.getCountBySql(query, params.toArray());
		return count > 0;
	}

	public List<UserBetJson> getBetUser(Integer userId, Integer matchId, Integer uhostedId) {
		if (userId == null || matchId == null || uhostedId == null)
			return null;
		String query = "SELECT ub.bet_bestof FROM user_bets ub "
				+ "	WHERE ub.user_id = ? AND ub.match_id = ? AND ub.uhosted_id = ?";
		params = new ArrayList<>();
		params.add(userId);
		params.add(matchId);
		params.add(uhostedId);
		String histories = jdbcManager.selectBySql(String.class, query, params.toArray()).getSingleResult();
		if(CommonUtil.isEmpty(histories)) return null;
		return castJsonToObj(histories);
	}
	
	public UserBetDto getAllBetUser(Integer userId, Integer matchId, Integer uhostedId) {
		if (userId == null || matchId == null || uhostedId == null)
			return null;
		String query = "SELECT ub.bet_bestof, ub.all_bet_histories FROM user_bets ub "
				+ "	WHERE ub.user_id = ? AND ub.match_id = ? AND ub.uhosted_id = ?";
		params = new ArrayList<>();
		params.add(userId);
		params.add(matchId);
		params.add(uhostedId);
		UserBetDto userBetDto = jdbcManager.selectBySql(UserBetDto.class, query, params.toArray()).getSingleResult();
		if(userBetDto == null) return null;
		userBetDto.setUserBetJsons(castJsonToObj(userBetDto.getBetBestof()));
		userBetDto.setUbetHistrJsons(castJsonToObj(userBetDto.getAllBetHistories()));
		return userBetDto;
	}

	public List<UserBetDto> getUserBetHistory(Integer userId) {
		List<UserBetDto> lstUserBet = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		params = new ArrayList<>();
		query.append("select game.game_type_id gameId, game.name gameName, ubet.all_bets allBets, ubet.match_id matchId,"
				+ " t1.name team1Name, t1.avatar team1Avatar, t1.team_id team1Id, "
				+ " t2.name team2Name, t2.avatar team2Avatar, t2.team_id team2Id"
				+ " from users user join  (select * from user_bets UNION  select * from user_bet_histories ) ubet ON user.user_id = ubet.user_id "
				+ " join matchs m ON ubet.match_id = m.match_id "
				+ " join game_types game ON m.game_type = game.game_type_id"
				+ " join teams t1 ON t1.team_id = m.team1_id " + " join teams t2 ON t2.team_id = m.team2_id "
				+ " where user.user_id = ? ");
		 params.add(userId);
		lstUserBet = jdbcManager.selectBySql(UserBetDto.class, query.toString(),params.toArray()).getResultList();
		return lstUserBet;
	}
	
	public boolean isRemainTimeBet(Integer matchId){
		if(matchId == null) return false;
		params = new ArrayList<>();
		String query = "SELECT m.match_id FROM MATCHS m "
				+ " WHERE m.match_id = ? AND m.status = "+ AppConstants.MATCH.MATCH_STATUS.DEFAULT;
		query += " AND m.date_time > " + DateUtil.addTimeDateNowMySql(5, AppConstants.TIME.MINUTE);
		params.add(matchId);
		return jdbcManager.getCountBySql(query, params.toArray())>0;
	}
	
	public List<SubMatchsDto> getSubMatch(Integer matchId){
		if(matchId == null) return null;
		String query = "SELECT * FROM SUB_MATCHS sm WHERE sm.match_id = ?";
		params = new ArrayList<>();
		params.add(matchId);
		return jdbcManager.selectBySql(SubMatchsDto.class, query, params.toArray()).getResultList();
	}
	
	public MatchDto loadDataBet(UserBetDto userBet, Integer userId){
		if(userBet == null) return null;
		Integer matchId = userBet.getMatchId();
		Integer uhostId = userBet.getUhostedId();
		Integer team1Id = userBet.getTeam1Id();
		Integer team2Id = userBet.getTeam2Id();
		StringBuilder query = new StringBuilder();
		query.append("SELECT m.match_id,sm.sub_match_id, "
					+"       (SELECT SUM(bet_money) "
					+"          FROM user_bet_detail ubd "
					+"         WHERE     ubd.user_id = ? "
					+"               AND ubd.match_id = m.match_id "
					+"               AND ubd.uhosted_id = ? "
					+"               AND ubd.bet_team_id = ?) "
					+"          AS ubet_money_team1, "
					+"       (SELECT SUM(bet_money) "
					+"          FROM user_bet_detail ubd "
					+"         WHERE     ubd.user_id = ? "
					+"               AND ubd.match_id = m.match_id "
					+"               AND ubd.uhosted_id = ? "
					+"               AND ubd.bet_team_id = ?) "
					+"          AS ubet_money_team2, "
					+"       (SELECT SUM(sm1.team1_bet_money) "
					+"          FROM sub_matchs sm1 "
					+"         WHERE sm1.team1_Id = ? AND sm1.match_id = m.match_id) "
					+"          AS total_money_team1, "
					+"       (SELECT SUM(sm2.team2_bet_money) "
					+"          FROM sub_matchs sm2 "
					+"         WHERE sm2.team2_Id = ? AND sm2.match_id = m.match_id) "
					+"          AS total_money_team2, "
					+"       (SELECT sm1.team1_bet_money "
					+"          FROM sub_matchs sm1 "
					+"         WHERE     sm1.match_id = m.match_id "
					+"               AND sm1.sub_match_id = sm.sub_match_id "
					+"               AND sm1.team1_Id = ?) "
					+"          AS money_team1_bo, "
					+"       (SELECT sm2.team2_bet_money "
					+"          FROM sub_matchs sm2 "
					+"         WHERE     sm2.match_id = m.match_id "
					+"               AND sm2.sub_match_id = sm.sub_match_id "
					+"               AND sm.team2_Id = ?) "
					+"          AS money_team2_bo, "
					+"       (SELECT ubd1.bet_money "
					+"          FROM user_bet_detail ubd1 "
					+"         WHERE     ubd1.user_id = ? "
					+"               AND ubd1.match_id = m.match_id "
					+"               AND ubd1.sub_match_id = sm.sub_match_id "
					+"               AND ubd1.uhosted_id = ? "
					+"               AND ubd1.bet_team_id = ?) "
					+"          AS ubd_money_team1, "
					+"       (SELECT ubd2.bet_money "
					+"          FROM user_bet_detail ubd2 "
					+"         WHERE     ubd2.user_id = ? "
					+"               AND ubd2.match_id = m.match_id "
					+"               AND ubd2.uhosted_id = ? "
					+"               AND ubd2.sub_match_id = sm.sub_match_id "
					+"               AND ubd2.bet_team_id = ?) "
					+"          AS ubd_money_team2 "
					+"  FROM matchs m "
					+"     JOIN (SELECT * FROM sub_matchs sm where sm.match_id = ?) as sm ON sm.match_id = m.match_id "
					+" WHERE m.match_id = ? ");
		params = new ArrayList<>();
		
		params.add(userId);
		params.add(uhostId);
		params.add(team1Id);
		params.add(userId);
		params.add(uhostId);
		params.add(team2Id);
		params.add(team1Id);
		params.add(team2Id);
		params.add(team1Id);
		params.add(team2Id);
		params.add(userId);
		params.add(uhostId);
		params.add(team1Id);
		params.add(userId);
		params.add(uhostId);
		params.add(team2Id);
		params.add(matchId);
		params.add(matchId);
		List<MatchDto> matchs = jdbcManager.selectBySql(MatchDto.class, query.toString(), params.toArray()).getResultList();
		if(CommonUtil.isEmpty(matchs)) return null;
		MatchDto matchDto = new MatchDto();
		List<UserBetDetailDto> detailDtos = new ArrayList<>();
		int size = matchs.size();
		try {
			for(int i=0; i < size; i++){
				MatchDto match = matchs.get(i);
				if(i==0) matchDto = match;
				UserBetDetailDto detailDto = setDataBetDetail(match);
				detailDtos.add(detailDto);
			}
			matchDto.setBetDetailList(detailDtos);
		} catch (Exception e) {
			e.getMessage();
		}
		statisticProfit(matchDto, detailDtos);
		return matchDto;
		
	}
	
	public boolean insert(UserBets userBets, List<UserBetDetail> userBetDetails){
		if(userBets == null && CommonUtil.isEmpty(userBetDetails)) return false;
		try {
			jdbcManager.insert(userBets).execute();
			jdbcManager.insertBatch(userBetDetails).execute();
			return true;
		} catch (Exception e) {
			e.getMessage();
		}
		return false;
	}
	
	public boolean update(UserBets userBets, List<UserBetDetail> userBetDetails){
		if(userBets == null && CommonUtil.isEmpty(userBetDetails)) return false;
		try {
			jdbcManager.update(userBets).execute();
			jdbcManager.updateBatch(userBetDetails).execute();
			return true;
		} catch (Exception e) {
			e.getMessage();
		}
		return false;
	}
	
	
	private List<UserBetJson> castJsonToObj(String json){
		if(CommonUtil.isEmpty(json)) return null;
		Gson gson = new Gson();
		Type listType = new TypeToken<List<UserBetJson>>() {}.getType();
        return  gson.fromJson(json, listType);
	}
}
