package com.staresport.service;

import java.util.ArrayList;
import java.util.List;

import com.staresport.dto.MatchDto;
import com.staresport.entity.Matchs;
import com.staresport.entity.Streams;
import com.staresport.utils.AppConstants;
import com.staresport.utils.CommonUtil;
import com.staresport.utils.DateUtil;

public class MatchService extends AbstractService<Matchs> {
	public List<MatchDto> getMatchList(int gameType, Integer userId) {
		List<MatchDto> matchDtos = null;
		try {
			params = new ArrayList<>();
			StringBuilder query = new StringBuilder();
			String sqlViewHost = "";
			if (userId != null) {
				sqlViewHost = ", (case WHEN (SELECT count(uh.user_id) FROM user_hosteds uh WHERE uh.user_id = ? AND uh.match_id = m.match_id) > 0 "
							+ "	  THEN true ELSE false END) as viewHost,"
							+ " (SELECT uh.user_id FROM USER_HOSTEDS uh WHERE uh.user_id = ? AND uh.match_id = m.match_id) as uhosted_id ";
				params.add(userId);
				params.add(userId);
			}
			query.append("select m.match_id matchId, DATE_FORMAT(m.date_time, '%H:%i') startTime, "
					+ " t1.name team1Name, t1.avatar team1Avatar, m.team1_bet_per team2BetPer, t1.flag team1Flag, "
					+ " t2.avatar team2Avatar, m.team2_bet_per team1BetPer, t2.flag team2Flag, "
					+ "( case when LENGTH(t2.name) > 100 then CONCAT(SUBSTRING(t2.name, 1, 100), '...') "
					+ " else t2.name end) as team2Name " + sqlViewHost 
					+ " from matchs m "
					+ " 	join teams t1 ON m.team1_id = t1.team_id " 
					+ " 	JOIN teams t2 ON m.team2_id = t2.team_id "
					+ " WHERE m.game_type = ? AND m.status = " + AppConstants.MATCH.MATCH_STATUS.DEFAULT
					+ "		AND (m.date_time > " + DateUtil.addTimeDateNowMySql(5, AppConstants.TIME.MINUTE)
					+ "		AND m.date_time <= " + DateUtil.addTimeDateNowMySql(24, AppConstants.TIME.HOUR) + ")"
					+ " ORDER BY m.date_time ASC ");
			params.add(gameType);
			matchDtos = jdbcManager.selectBySql(MatchDto.class, query.toString(), params.toArray()).getResultList();
		} catch (Exception e) {
			e.getMessage();
		}
		return matchDtos;
	}

	public boolean getNumHostedOfMatch(Long matchId) {
		if (matchId == null)
			return false;
		params = new ArrayList<>();
		String query = "SELECT num_hosted FROM MATCHS WHERE match_id = ?";
		params.add(matchId);
		Long numHosted = jdbcManager.selectBySql(Long.class, query, params.toArray()).getSingleResult();
		return numHosted == null || numHosted <= AppConstants.HOST.NUM_HOST;
	}

	public boolean updateNumHostedOfMatch(Long matchId) {
		if (matchId == null)
			return false;
		params = new ArrayList<>();
		String query = "UPDATE MATCHS m SET "
				+ " m.NUM_HOSTED = ( case when m.NUM_HOSTED is null then 0 else m.NUM_HOSTED end) + 1 "
				+ " WHERE m.MATCH_ID = ?";
		params.add(matchId);
		int result = jdbcManager.updateBySql(query, Long.class).params(params.toArray()).execute();
		return result > 0;
	}

	/**
	 * NamLX : Live Tab
	 * 
	 * @param gameType
	 * @param matchStatus
	 * @return
	 */
	public List<MatchDto> getMatchListLive(int gameType, int matchStatus, String searchKey) {
		List<MatchDto> matchs = new ArrayList<>();
		params = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		query.append(
				"select m.match_id matchId, DATE_FORMAT(m.date_time, '%H:%i') startTime, m.status, uh.user_hosted_id as userHostedsId,  "
						+ " t1.name team1Name, t1.avatar team1Avatar, m.team1_bet_per team2BetPer, t1.flag team1Flag, "
						+ " t2.name team2Name, t2.avatar team2Avatar, m.team2_bet_per team1BetPer, t1.flag team2Flag,"
						+ " user.name as userHosted, user.avatar_url as hostedAvar,user.user_type_id as userType, gtype.name as gameName, "
						+ " gtype.short_name as gameShortName, gtype.img_url as gameAvar, m.total_user_show as totalShowing  "
						+ " from matchs m join user_hosteds uh ON m.match_id = uh.match_id join users user ON uh.user_id = user.user_id "
						+ " join game_types gtype ON m.game_type = gtype.game_type_id  "
						+ " join teams t1 ON m.team1_id = t1.team_id " + " JOIN teams t2 ON m.team2_id = t2.team_id "
						+ " where m.game_type = ? AND m.status = ? ");
		params.add(gameType);
		params.add(matchStatus);
		if (!CommonUtil.isEmpty(searchKey)) {
			query.append(" AND (t1.name like ? OR t2.name like ? OR user.name like  ? )");
			searchKey = "%" + searchKey + "%";
			params.add(searchKey);
			params.add(searchKey);
			params.add(searchKey);
		}

		try {
			matchs = jdbcManager.selectBySql(MatchDto.class, query.toString(), params.toArray()).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return matchs;
	}

	/**
	 * NamLX :aside-left
	 * 
	 * @return
	 */
	public List<MatchDto> getMostViewingMatch() {
		List<MatchDto> matchs = new ArrayList<>();
		params = new ArrayList<>();
		StringBuilder query = new StringBuilder();

		query.append("select m.match_id matchId, DATE_FORMAT(m.date_time, '%H:%i') startTime, uh.user_hosted_id as userHostedsId,"
				+ " t1.name team1Name, t1.avatar team1Avatar, m.team1_bet_per team2BetPer, t1.flag team1Flag, "
				+ " t2.name team2Name, t2.avatar team2Avatar, m.team2_bet_per team1BetPer, t1.flag team2Flag,"
				+ " user.name as userHosted, user.avatar_url as hostedAvar,user.user_type_id as userType, gtype.name as gameName, "
				+ " gtype.short_name as gameShortName, gtype.img_url as gameAvar, m.total_user_show as totalShowing  "
				+ " from matchs m join user_hosteds uh ON m.match_id = uh.match_id join users user ON uh.user_id = user.user_id "
				+ " join game_types gtype ON m.game_type = gtype.game_type_id  "
				+ " join teams t1 ON m.team1_id = t1.team_id " + " JOIN teams t2 ON m.team2_id = t2.team_id "
				+ " where  m.status = ? ORDER BY m.total_user_show DESC LIMIT 6");
		params.add(AppConstants.MATCH.MATCH_STATUS.RUNNING);
		try {
			matchs = jdbcManager.selectBySql(MatchDto.class, query.toString(), params.toArray()).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return matchs;
	}

	public List<MatchDto> getAdminStream() {
		List<MatchDto> matchs = new ArrayList<>();
		params = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		query.append("select m.match_id matchId, DATE_FORMAT(m.date_time, '%Y/%m/%d %T') startTime, m.status,  "
				+ " gtype.short_name as gameShortName, gtype.img_url as gameAvar, m.name, gtype.video_img_url as gameImgStream "
				+ " from matchs m join user_hosteds uh ON m.match_id = uh.match_id join users user ON uh.user_id = user.user_id "
				+ " join game_types gtype ON m.game_type = gtype.game_type_id  "
				+ " where user.user_type_id = ? AND m.date_time > NOW() limit 4 ");
		params.add(AppConstants.USER_TYPE.ADMIN);
		try {
			matchs = jdbcManager.selectBySql(MatchDto.class, query.toString(), params.toArray()).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return matchs;
	}

	/**
	 * NamLX : Match Comming, next to streaming video, search by Teamname or
	 * Userhosted name
	 * 
	 * @param hours
	 * @param gameType
	 * @param searchMatch
	 * @return
	 */
	public List<MatchDto> getMatchComing(int hours, int gameType, String searchMatch) {
		List<MatchDto> lstMatchComming = new ArrayList<>();
		params = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		query.append(
				"select m.match_id matchId, DATE_FORMAT(m.date_time, '%Y/%m/%d %T') startTime,m.date_time as dateTime, "
						+ " t1.name team1Name, t1.avatar team1Avatar, m.team1_bet_per team2BetPer, t1.flag team1Flag, "
						+ " t2.name team2Name, t2.avatar team2Avatar, m.team2_bet_per team1BetPer, t1.flag team2Flag, "
						+ " m.status, uh.user_hosted_id as userHostedsId, user.user_id uhostedId, "
						+ " user.name as userHosted, user.avatar_url as hostedAvar,user.user_type_id as userType, m.total_user_show as totalShowing, "
						+ " gtype.short_name as gameShortName, gtype.img_url as gameAvar, m.name, gtype.video_img_url as gameImgStream "
						+ " from matchs m " + " join user_hosteds uh ON m.match_id = uh.match_id "
						+ " join users user ON uh.user_id = user.user_id "
						+ " join teams t1 ON m.team1_id = t1.team_id " + " JOIN teams t2 ON m.team2_id = t2.team_id "
						+ " join game_types gtype ON m.game_type = gtype.game_type_id where 1 = 1 ");

		if (gameType != 0) {
			query.append("  AND m.game_type = ? ");
			params.add(gameType);
		}
		if (searchMatch != null && searchMatch.length() > 0) {
			query.append(" AND (user.name like ? OR  t1.name like ? OR t2.name like ? )");
			searchMatch = "%" + searchMatch + "%";
			params.add(searchMatch);
			params.add(searchMatch);
			params.add(searchMatch);
		}
		if (hours == AppConstants.MATCH_LOAD_OPTION.LOAD_NEXT_MATCH) {
			query.append(" AND  (m.status = 1 OR m.date_time > NOW() ) ORDER BY m.date_time ASC limit 10 ");
		} else if (hours == AppConstants.MATCH_LOAD_OPTION.LOAD_ALL_MATCH) {
			query.append(" AND (m.status = 1 OR ( m.status = 0  AND m.date_time > NOW() ) ) ORDER BY m.date_time ASC ");
		} else {
			query.append(" AND m.status = 0 AND  ( m.date_time > NOW() AND m.date_time < date_add(NOW(), INTERVAL "
					+ hours + " HOUR) )" + " ORDER BY m.date_time ASC ");
		}
		try {
			lstMatchComming = jdbcManager.selectBySql(MatchDto.class, query.toString(), params.toArray())
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return lstMatchComming;
	}

	public boolean isUserHosted(Integer userId, Long matchId) {
		if (userId == null || matchId == null)
			return true;
		String query = "SELECT uh.user_hosted_id FROM USER_HOSTEDS uh WHERE uh.USER_ID = ? AND uh.MATCH_ID = ?";
		params = new ArrayList<>();
		params.add(userId);
		params.add(matchId);
		return jdbcManager.getCountBySql(query, params.toArray()) > 0;
	}

	public MatchDto getInfoMatch(Long matchId) {
		if (matchId == null)
			return null;
		StringBuilder query = new StringBuilder();
		query.append("select m.match_id as matchId, m.name, "
				+ " (SELECT gt.name FROM game_types gt WHERE gt.game_type_id = m.game_type) as gameName, "
				+ " (SELECT name FROM teams t WHERE t.team_id = m.team1_id) as team1Name, "
				+ " (SELECT name FROM teams t WHERE t.team_id = m.team2_id) as team2Name,"
				+ " s.stream_id, s.lang, s.url "
				+ " FROM MATCHS m LEFT JOIN STREAMS s ON m.match_id = s.match_id AND s.status = 0"
				+ " WHERE m.match_id = ? ");
		params = new ArrayList<>();
		params.add(matchId);
		List<MatchDto> matchs = jdbcManager.selectBySql(MatchDto.class, query.toString(), params.toArray())
				.getResultList();
		if (CommonUtil.isEmpty(matchs))
			return null;
		MatchDto match = new MatchDto();
		List<Streams> streams = new ArrayList<>();
		Streams stream = null;
		int size = matchs.size();
		for (int i = 0; i < size; i++) {
			MatchDto dto = matchs.get(i);
			stream = new Streams();
			if (i == 0) {
				match.setMatchId(dto.getMatchId());
				match.setName(dto.getName());
				match.setGameName(dto.getName());
				match.setTeam1Name(dto.getTeam1Name());
				match.setTeam2Name(dto.getTeam2Name());
			}
			Long streamId = dto.getStreamId();
			if (streamId == null)
				return match;
			stream.setStreamId(dto.getStreamId());
			stream.setLang(dto.getLang());
			stream.setUrl(dto.getUrl());
			streams = match.getStreams();
			if (!streams.contains(stream)) {
				streams.add(stream);
				match.setStreams(streams);
			}
		}
		return match;
	}
}
