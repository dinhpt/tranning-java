package com.staresport.service;

import java.util.ArrayList;
import java.util.List;

import com.staresport.entity.GameTypes;

public class GameTypesService extends AbstractService<GameTypes> {
	public int insert(GameTypes gtype) {
		return jdbcManager.insert(gtype).execute();
	}

	public List<GameTypes> getAllTypes() {
		List<GameTypes> lstGameTypes = new ArrayList<>();
		lstGameTypes = jdbcManager.from(GameTypes.class).getResultList();
		return lstGameTypes;
	}
	
	public GameTypes getById(int gameId) {
		String where = " game_type_id = ? ";
		return jdbcManager.from(GameTypes.class).where(where, gameId).getSingleResult();
	}

}
