package com.staresport.service;

import java.util.ArrayList;

import com.staresport.dto.SubMatchsDto;
import com.staresport.entity.SubMatchs;

public class SubMatchsService extends AbstractService<SubMatchs> {
	public SubMatchsDto getById(Integer subMatchId, Integer matchId) {
		params = new ArrayList<>();
		String query = "SELECT * FROM sub_matchs WHERE  sub_match_id = ? AND match_id = ? ";
		params.add(subMatchId);
		params.add(matchId);
		return jdbcManager.selectBySql(SubMatchsDto.class, query, params.toArray()).getSingleResult();
	}
}
