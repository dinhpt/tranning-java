package com.staresport.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.seasar.extension.jdbc.AutoSelect;
import org.seasar.extension.jdbc.JdbcManager;
import org.seasar.extension.jdbc.SqlFileSelect;
import org.seasar.extension.jdbc.SqlFileUpdate;
import org.seasar.extension.jdbc.SqlSelect;
import org.seasar.extension.jdbc.service.S2AbstractService;
import org.seasar.framework.beans.util.BeanMap;

/**
 * Base service class for master database.
 *
 * @author GMO-SC horiuchi
 *
 * @param <T>
 */
public abstract class AbstractService<T> extends S2AbstractService<T> {

	@Resource(name = "jdbcManager")
	protected JdbcManager jdbcManager;

	protected List<Object> params = new ArrayList<>();
	/**
	 * Change the sqlFilePath to "sql/simpleClassName" ex.
	 * "sql/SomeEntity/selectAll.sql"<br>
	 * (Overrides superclass method.)<br>
	 *
	 * @param entityClass
	 *            type of entity
	 */
	@Override
	protected void setEntityClass(final Class<T> entityClass) {
		this.entityClass = entityClass;
		sqlFilePathPrefix = "sql/";
	}

	/**
	 * Return the select query to master database.<br>
	 *
	 * @return AutoSelect<T> select query
	 */
	@Override
	protected AutoSelect<T> select() {
		return selectFromMaster();
	}

	/**
	 * Return the select query to master database.<br>
	 *
	 * @return AutoSelect<T> select query
	 */
	protected AutoSelect<T> selectFromMaster() {
		return jdbcManager.from(entityClass);
	}

	/**
	 * Return the sqlfile select query to master database.<br>
	 *
	 * 　@param <T2> type of query result class
	 * 
	 * @param baseClass
	 *            query result class
	 * @param path
	 *            sqlfile path
	 * @return SqlFileSelect<T2> select query
	 */
	@Override
	protected <T2> SqlFileSelect<T2> selectBySqlFile(final Class<T2> baseClass,
			final String path) {
		return selectBySqlFileFromMaster(baseClass, path);
	}

	/**
	 * Return the sqlfile select query to master database.<br>
	 *
	 * 　@param <T2> type of query result class
	 * 
	 * @param baseClass
	 *            query result class
	 * @param path
	 *            sqlfile path
	 * @param parameter
	 *            query parameters
	 * @return SqlFileSelect<T2> select query
	 */
	@Override
	protected <T2> SqlFileSelect<T2> selectBySqlFile(final Class<T2> baseClass,
			final String path, final Object parameter) {
		return selectBySqlFileFromMaster(baseClass, path, parameter);
	}

	/**
	 * Return the sqlfile select query to master database.<br>
	 *
	 * 　@param <T2> type of query result class
	 * 
	 * @param baseClass
	 *            query result class
	 * @param path
	 *            sqlfile path
	 * @return SqlFileSelect<T2> select query
	 */
	protected <T2> SqlFileSelect<T2> selectBySqlFileFromMaster(
			final Class<T2> baseClass, final String path) {
		return jdbcManager.selectBySqlFile(baseClass, sqlFilePathPrefix + path);
	}

	/**
	 * Return the sqlfile select query to master database.<br>
	 *
	 * 　@param <T2> type of query result class
	 * 
	 * @param baseClass
	 *            query result class
	 * @param path
	 *            sqlfile path
	 * @param parameter
	 *            query parameters
	 * @return SqlFileSelect<T2> select query
	 */
	protected <T2> SqlFileSelect<T2> selectBySqlFileFromMaster(
			final Class<T2> baseClass, final String path, final Object parameter) {
		return jdbcManager.selectBySqlFile(baseClass, sqlFilePathPrefix + path,
				parameter);
	}

	/**
	 * Return the record count by sqlfile count query.<br>
	 *
	 * @param path
	 *            sqlfile path
	 * @param parameter
	 *            query parameters
	 * @return long record count
	 */
	protected long countBySqlFile(final String path, final Object parameter) {
		return countBySqlFileFromMaster(path, parameter);
	}

	/**
	 * Return the record count by sqlfile count query.<br>
	 *
	 * @param path
	 *            sqlfile path
	 * @param parameter
	 *            query parameters
	 * @return long record count
	 */
	protected long countBySqlFileFromMaster(final String path,
			final Object parameter) {
		return jdbcManager.getCountBySqlFile(sqlFilePathPrefix + path,
				parameter);
	}


	/**
	 * updateBySqlFile
	 *
	 * @param path
	 */
	@Override
	protected SqlFileUpdate updateBySqlFile(final String path) {
		return jdbcManager.updateBySqlFile(sqlFilePathPrefix + path);
	}

	/**
	 * updateBySqlFile
	 *
	 * @param path
	 * @param parameter
	 */
	@Override
	protected SqlFileUpdate updateBySqlFile(String path, Object parameter) {
		return jdbcManager.updateBySqlFile(sqlFilePathPrefix + path, parameter);
	}
	
	protected  <T2> SqlSelect<T2>  selectBySql(final Class<T2> baseClass, final String sql, final Object parameter){
		return jdbcManager.selectBySql(baseClass, sql, parameter);
	}


	/**
	 * Delete (physical)
	 */
	@Override
	public int delete(T entity) {
		return jdbcManager.delete(entity).execute();
	}

	public List<T> findAll() {
		return select().getResultList();
	}

	public List<T> findByCondition(BeanMap conditions) {
		return select().where(conditions).getResultList();
	}

	public long getCount() {
		return select().getCount();
	}

	public int insert(T entity) {
		return jdbcManager.insert(entity).execute();
	}
	
	public int insert(List<T> entities) {
		return jdbcManager.insert(entities).execute();
		//return super.insert(entity);
	}

	public int update(T entity) {
		return jdbcManager.update(entity).execute();
	}
	
	public int update(List<T> entities) {
		return jdbcManager.update(entities).execute();
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}
}
