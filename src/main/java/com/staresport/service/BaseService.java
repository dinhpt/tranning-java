package com.staresport.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.staresport.entity.CommonEntity;
import com.staresport.utils.CommonUtil;
import com.staresport.utils.DateUtil;
import com.staresport.utils.EntityUtil;

/**
 * 
 * @author ChuQuangVi
 *
 * @param <T>
 */
@SuppressWarnings("hiding")
public class BaseService<T> extends AbstractService<T> {
	private Logger logger = Logger.getLogger(BaseService.class); 
	
	public <T> T findById(Class<T> c, Object key) {
		if(key == null) return null;
		return jdbcManager.from(c).id(key).getSingleResult();
	}

	public T insert(T obj, Integer createdBy){
		try {
			if (obj instanceof CommonEntity) {
				((CommonEntity) obj).setCreatedBy(createdBy);;
				((CommonEntity) obj).setCreatedAt(DateUtil.getSystemDate());
			}
			jdbcManager.insert(obj).execute();
			return obj;
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return null;
	}

	public List<T> insert(List<T> objList, Integer createdBy){
		try {
			List<T> insertedList = new ArrayList<T>();
			T insertedObj;
			for (T obj : objList) {
				insertedObj = insert(obj, createdBy);
				if (insertedObj != null) {
					insertedList.add(obj);
				}
			}
			return insertedList;
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return null;
	}
	
	public boolean insertBatch(List<T> objList){
		try {
			int[] record = jdbcManager.insertBatch(objList).execute();
			return record.length > 0;
		} catch (Exception e) {
			e.getMessage();
		}
		return false;
	}

	public <T> T update(T obj, Integer updatedBy) {
		try {
			if (obj instanceof CommonEntity) {
				((CommonEntity) obj).setUpdatedBy(updatedBy);
				((CommonEntity) obj).setUpdateAt(DateUtil.getSystemDate());
			}
			int result = jdbcManager.update(obj).execute();
			if (result > 0)	return obj;
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return null;
	}

	public List<T> update(List<T> objList, Integer updatedBy) {
		try {
			List<T> updatedList = new ArrayList<T>();
			T updatedObj;
			for (T obj : objList) {
				updatedObj = update(obj, updatedBy);
				if (updatedObj != null) {
					updatedList.add(updatedObj);
				}
			}
			return updatedList;
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return null;
	}
	
	public boolean updateBatch(List<T> objList){
		try {
			int[] record = jdbcManager.updateBatch(objList).execute();
			return record.length > 0;
		} catch (Exception e) {
			e.getMessage();
		}
		return false;
	}

	public <T> int delete(Class<T> c, Object key) {
		if(key == null) return 0;
		try {
			T obj = findById(c, key);
			if (obj != null) {
				return jdbcManager.delete(obj).execute();
			}
		} catch (Exception e) {
			return 0;
		}
		return 0;
	}

	public void delete(List<T> objList) {
		for (T obj : objList) {
			delete(obj);
		}
	}

	/**
	 * DELETE BY IDS
	 * 
	 * @param c
	 * @param idField
	 * @param ids
	 * @return
	 * @author ChuQuangVi
	 */
	public int deleteByIds(Class<T> c, String idField, String[] ids) {
		if (CommonUtil.isEmpty(ids))
			return 0;
		String table = EntityUtil.getEntityName(c);
		if (CommonUtil.isEmpty(table))
			return 0;
		String id = "";
		int size = ids.length;
		for (int i = 0; i < size; i++) {
			if (i == 0) {
				id = ids[i];
				continue;
			}
			id += "," + ids[i];
		}
		String query = "DELETE FROM " + table + " a " + " WHERE a." + idField + " IN (" + id + ")";
		int result = jdbcManager.updateBySql(query).execute();
		return result;
	}

	public List<T> findByProperty(Class<T> c, String idField, Object value, String orderBy) {
		if(value == null) return null;
		String where = idField + " = ? ";
		if (CommonUtil.isEmpty(orderBy))
			orderBy = idField;
		return jdbcManager.from(c).where(where, value).orderBy(orderBy).getResultList();
	}

	public List<T> findAll(Class<T> c, String... orderByArgs) {
		String orderBy = "";
		if (!CommonUtil.isEmpty(orderByArgs)) {
			orderBy = StringUtils.join(orderByArgs, ",");
		}
		return jdbcManager.from(c).orderBy(orderBy).getResultList();
	}
}
