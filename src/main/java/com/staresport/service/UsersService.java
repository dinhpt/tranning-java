package com.staresport.service;

import java.util.ArrayList;

import com.staresport.dto.UserDto;
import com.staresport.entity.Users;
import com.staresport.utils.CommonUtil;
import com.staresport.utils.PermissionUtil;

public class UsersService extends AbstractService<Users>{
	
	public UserDto authen(String username, String password){
		if(CommonUtil.isEmpty(username) || CommonUtil.isEmpty(password))
			return null;
		String query = "SELECT u.user_id, u.user_name, u.name,"
				+ " (SELECT ut.code FROM user_types ut WHERE u.user_type_id = ut.user_type_id) as typeCode"
				+ " FROM USERS u WHERE u.user_name = ? AND u.password = ?";
		params = new ArrayList<Object>();
		params.add(username);
		params.add(password);
		UserDto userDto = jdbcManager.selectBySql(UserDto.class, query, params.toArray()).getSingleResult();
		PermissionUtil.permission(userDto);
		return userDto;
	}
}
