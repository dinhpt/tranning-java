package com.staresport.service;

import java.util.ArrayList;

import com.staresport.dto.UserHostedDto;
import com.staresport.entity.UserHosteds;

public class UserHostedsService extends AbstractService<UserHosteds> {
	public UserHosteds getById(Long userHostedsId) {
		String where = " user_hosted_id = ? ";
		return jdbcManager.from(UserHosteds.class).where(where, userHostedsId).getSingleResult();
	}

	public UserHostedDto getUserHostedInfo(Long userHostedId) {
		UserHostedDto userHostedDto = new UserHostedDto();
		StringBuilder query = new StringBuilder();
		params = new ArrayList<>();
		query.append(
				"select uh.user_hosted_id  userHostedId, uh.user_id as userId, uh.match_id  matchId , uh.stream  stream, "
						+ " uh.code  code, uh.total_user_bet  totalUserBet, uh.total_fee  totalFee, m.best_of  bestOf, "
						+ " m.name  matchName, m.tournament  tournament,  m.total_user_show  totalShowing,"
						+ " uh.total_money_bet  totalMoneyBet, uh.time_hosted   timeHosted, m.status  matchStatus, DATE_FORMAT(m.date_time, '%Y/%m/%d %T') dateTime "
						+ " from user_hosteds uh join matchs m on uh.match_id = m.match_id where uh.user_hosted_id = ? ");
		params.add(userHostedId);
		userHostedDto = jdbcManager.selectBySql(UserHostedDto.class, query.toString(), params.toArray())
				.getSingleResult();
		return userHostedDto;
	}

}
