/*
 * Copyright 2004-2008 the Seasar Foundation and the Others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
package com.staresport.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletContext;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;
import org.seasar.struts.util.ResponseUtil;

import com.staresport.dto.MatchDto;
import com.staresport.dto.MatchsDto;
import com.staresport.dto.UserHostedDto;
import com.staresport.entity.GameTypes;
import com.staresport.form.HomeForm;
import com.staresport.utils.AppConstants;
import com.staresport.utils.CommonUtil;
import com.staresport.utils.Notification;
import com.staresport.utils.SiteUrlUtil;

import net.arnx.jsonic.JSON;

public class IndexAction extends BaseAction {

	@Resource
	protected ServletContext application;

	@Resource
	protected SessionAction sessionAction;

	// private final String LOGIN = "/login/login.jsp";
	private final String INDEX = "/index/index.jsp";

	@ActionForm
	public HomeForm homeForm;
	protected List<MatchDto> lstLolLive = new ArrayList<>();
	protected List<MatchDto> lstDotaLive = new ArrayList<>();
	protected List<MatchDto> lstFifaLive = new ArrayList<>();
	protected List<MatchDto> lstCsLive = new ArrayList<>();
	protected List<MatchDto> lstOverwatchLive = new ArrayList<>();

	@Execute(validator = false)
	public String index() {
		try {
			// call auto load common
			Notification.showNotificationError(request, "name", "name");
			initPage();
			return INDEX;
		} catch (Exception e) {
			return null;
		}
	}

	private void initPage() {
		initLstGame(homeForm);
		loadMatchStreamComing();
		loadLiveTab();
	}

	private void loadMatchStreamComing() {
		initLstGame(homeForm);
		List<MatchDto> lstMatchComming = matchLogic.getMatchComing(AppConstants.MATCH_LOAD_OPTION.LOAD_NEXT_MATCH,
				AppConstants.MATCH_LOAD_OPTION.LOAD_NEXT_MATCH, "");
		homeForm.setLstMatchComming(lstMatchComming);
		if (lstMatchComming != null && lstMatchComming.size() > 0) {
			UserHostedDto userHostedDto = userHostedsLogic.getUserHostedInfo(lstMatchComming.get(0).getUserHostedsId());
			homeForm.setUserHostedDto(userHostedDto);
		}
	}

	private void loadLiveTab() {
		List<MatchsDto> lstMatchsDtos = new ArrayList<>();
		loadLiveMatch("");
		convertList(lstMatchsDtos);
		homeForm.setLstMatchsDtos(lstMatchsDtos);
	}

	protected void loadLiveMatch(String searchKey) {
		lstLolLive = matchLogic.getMatchListLive(AppConstants.GOSUGAMER.GAME_TYPE.LOL,

				AppConstants.MATCH.MATCH_STATUS.RUNNING, searchKey);
		lstDotaLive = matchLogic.getMatchListLive(AppConstants.GOSUGAMER.GAME_TYPE.DOTA,
				AppConstants.MATCH.MATCH_STATUS.RUNNING, searchKey);
		lstFifaLive = matchLogic.getMatchListLive(AppConstants.GOSUGAMER.GAME_TYPE.FIFA,
				AppConstants.MATCH.MATCH_STATUS.RUNNING, searchKey);
		lstCsLive = matchLogic.getMatchListLive(AppConstants.GOSUGAMER.GAME_TYPE.COUNTERSTRIKE,
				AppConstants.MATCH.MATCH_STATUS.RUNNING, searchKey);
		lstOverwatchLive = matchLogic.getMatchListLive(AppConstants.GOSUGAMER.GAME_TYPE.OVERWATCH,
				AppConstants.MATCH.MATCH_STATUS.RUNNING, searchKey);
	}

	protected void convertList(List<MatchsDto> lstMatchsDtos) {
		if (lstLolLive.size() > 0 || lstCsLive.size() > 0 || lstOverwatchLive.size() > 0 || lstDotaLive.size() > 0
				|| lstFifaLive.size() > 0) {
			converToLstMatchsDto(lstMatchsDtos, lstLolLive, AppConstants.GOSUGAMER.GAME_TYPE.LOL);
			converToLstMatchsDto(lstMatchsDtos, lstCsLive, AppConstants.GOSUGAMER.GAME_TYPE.COUNTERSTRIKE);
			converToLstMatchsDto(lstMatchsDtos, lstOverwatchLive, AppConstants.GOSUGAMER.GAME_TYPE.OVERWATCH);
			converToLstMatchsDto(lstMatchsDtos, lstDotaLive, AppConstants.GOSUGAMER.GAME_TYPE.DOTA);
			converToLstMatchsDto(lstMatchsDtos, lstFifaLive, AppConstants.GOSUGAMER.GAME_TYPE.FIFA);
		}
	}

	protected void converToLstMatchsDto(List<MatchsDto> lstMatchsDto, List<MatchDto> lstMatch, int gameType) {
		MatchsDto matchsDto = new MatchsDto();
		String name = AppConstants.GOSUGAMER.GAME_TYPE.NAME_GAME_MAP.get(gameType);
		matchsDto.setGameType(gameType);
		matchsDto.setNameGame(name);
		matchsDto.setMatchDtos(lstMatch);
		lstMatchsDto.add(matchsDto);
	}

	@Execute(validator = false)
	public String loadTabMatch() {
		Integer loadOption = homeForm.getLoadOption();
		String searchMatch = homeForm.getSearchMatch().trim();
		Integer selectedGame = homeForm.getGameSelect();
		if (loadOption == null)
			return INDEX;
		List<MatchsDto> lstMatchsDtos = new ArrayList<>();
		if (loadOption == AppConstants.MATCH_LOAD_OPTION.LOAD_LIVE_MATCH) {
			if (selectedGame == null || selectedGame == -1) {
				loadLiveMatch(searchMatch);
				convertList(lstMatchsDtos);
			} else {
				List<MatchDto> lstMatchLive = matchLogic.getMatchListLive(selectedGame,
						AppConstants.MATCH.MATCH_STATUS.RUNNING, searchMatch);

				converToLstMatchsDto(lstMatchsDtos, lstMatchLive, selectedGame);
			}
		} else {
			try {
				// Load Match by game type
				if (selectedGame == null || selectedGame == -1) {
					getMatchComingGameType(lstMatchsDtos, AppConstants.GOSUGAMER.GAME_TYPE.LOL, loadOption,
							searchMatch);
					getMatchComingGameType(lstMatchsDtos, AppConstants.GOSUGAMER.GAME_TYPE.COUNTERSTRIKE, loadOption,
							searchMatch);
					getMatchComingGameType(lstMatchsDtos, AppConstants.GOSUGAMER.GAME_TYPE.OVERWATCH, loadOption,
							searchMatch);
					getMatchComingGameType(lstMatchsDtos, AppConstants.GOSUGAMER.GAME_TYPE.DOTA, loadOption,
							searchMatch);
					getMatchComingGameType(lstMatchsDtos, AppConstants.GOSUGAMER.GAME_TYPE.FIFA, loadOption,
							searchMatch);
				} else {
					getMatchComingGameType(lstMatchsDtos, selectedGame, loadOption, searchMatch);
				}
			} catch (Exception e) {
				lstMatchsDtos = new ArrayList<>();
				return null;
			}
		}
		Map<String, Object> outPut = new HashMap<String, Object>();
		if (lstMatchsDtos != null && lstMatchsDtos.size() > 0) {
			outPut.put("lstComingMatchs", lstMatchsDtos);
		}
		String json = JSON.encode(outPut);
		ResponseUtil.write(json, "application/json", "UTF-8");
		return null;
	}

	private void getMatchComingGameType(List<MatchsDto> lstMatchsDtos, int gameType, int loadOption,
			String searchMatch) {
		List<MatchDto> lstMatchDtos = new ArrayList<>();
		lstMatchDtos = matchLogic.getMatchComing(loadOption, gameType, searchMatch);
		boolean gameExist = false;
		if (lstMatchDtos != null && lstMatchsDtos.size() > 0) {
			for (MatchsDto matchs : lstMatchsDtos) {
				if (matchs.getGameType() == gameType) {
					gameExist = true;
					matchs.getMatchDtos().addAll(lstMatchDtos);
					break;
				}
			}
		}
		if (!gameExist) {
			MatchsDto matchsDto = new MatchsDto();
			String name = AppConstants.GOSUGAMER.GAME_TYPE.NAME_GAME_MAP.get(gameType);
			matchsDto.setNameGame(name);
			matchsDto.setMatchDtos(lstMatchDtos);
			matchsDto.setGameType(gameType);
			lstMatchsDtos.add(matchsDto);
		}
	}

	@Execute(validator = false, urlPattern = "{gameId}")
	public String game() {
		Integer gameId;
		try {
			gameId = Integer.parseInt((String) request.getParameter("gameId"));
		} catch (Exception e) {
			return INDEX;
		}
		homeForm.setGameSelect(gameId);
		loadMatchStreamComing();
		GameTypes game = gamesLogic.getById(gameId);
		if (game != null) {
			try {
				List<MatchsDto> lstMatchsDtos = new ArrayList<>();
				List<MatchDto> lstGame = matchLogic.getMatchListLive(gameId, AppConstants.MATCH.MATCH_STATUS.RUNNING,
						"");
				if (lstGame != null & lstGame.size() > 0) {
					converToLstMatchsDto(lstMatchsDtos, lstGame, gameId);
				}
				homeForm.setLstMatchsDtos(lstMatchsDtos);
			} catch (Exception e) {
				e.getMessage();
			}
		}
		return INDEX;
	}

	@Execute(validator = false, urlPattern = "stream/{userHostId}")
	public String stream() {
		Long userHostId;
		try {
			userHostId = Long.parseLong((String) request.getParameter("userHostId"));
		} catch (Exception e) {
			return INDEX;
		}
		loadLiveTab();
		loadMatchStreamComing();

		UserHostedDto userHostedDto = userHostedsLogic.getUserHostedInfo(userHostId);
		homeForm.setUserHostedDto(userHostedDto);
		return INDEX;
	}

	@Execute(validator = false)
	public String doLogout() {
		sessionDto.clear();
		String redirectTo = SiteUrlUtil.getIndexUrl() + "?redirect=true";
		return redirectTo;
	}

	/**
	 * Redirect from VQS
	 * @return
	 */
	@Execute(validator = false, urlPattern = "{token}/{userName}/{password}/{moneyStar}")
	public String login() {
		String userName = (String) request.getParameter("userName");
		String password = (String) request.getParameter("userName");
		String moneyStar = (String) request.getParameter("userName");
		if (CommonUtil.isEmpty(userName) || CommonUtil.isEmpty(password) || CommonUtil.isEmpty(moneyStar)) {
			return INDEX;
		}
		return INDEX;
	}

}
