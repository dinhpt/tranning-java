package com.staresport.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.seasar.framework.container.SingletonS2Container;

import com.staresport.dto.MatchDto;
import com.staresport.dto.SessionDto;
import com.staresport.dto.UserDto;
import com.staresport.entity.GameTypes;
import com.staresport.form.BaseForm;
import com.staresport.logic.BaseLogic;
import com.staresport.logic.GameTypesLogic;
import com.staresport.logic.MatchLogic;
import com.staresport.logic.UserHostedsLogic;
import com.staresport.utils.AppConstants;
import com.staresport.utils.EntityUtil;
import com.staresport.utils.SiteUrlUtil;

public class BaseAction {
	public static final String INDEX = SiteUrlUtil.getIndexUrl() + "?redirect=true";
	/**
	 * LoginSessionDto
	 */
	@Resource
	protected SessionDto sessionDto;

	@Resource
	protected HttpServletRequest request;

	@Resource
	protected HttpServletResponse response;

	@Resource
	protected HttpSession session;

	@Resource
	protected BaseLogic baseLogic;

	protected int viewState;
	public String viewInsert;
	public String viewUpdate;

	// INFO USER
	public UserDto user = new UserDto();

	@Resource
	protected GameTypesLogic gamesLogic;

	@Resource
	protected UserHostedsLogic userHostedsLogic;

	@Resource
	protected MatchLogic matchLogic;

	protected List<GameTypes> lstGameType = new ArrayList<>();
	protected List<MatchDto> lstMostViewing = new ArrayList<>();

	/**
	 * Initial
	 */
	public BaseAction() {
		sessionDto = SingletonS2Container.getComponent(SessionDto.class);
		if (sessionDto != null) {
			user = sessionDto.user;
		}
	}

	/**
	 * @author NamLX
	 * @param baseForm
	 *            Cac action khac phai goi ham nay o trong ham index
	 */
	protected void initLstGame(BaseForm baseForm) {
		try {
			lstGameType = gamesLogic.getAllTypes();
			baseForm.setLstGameType(lstGameType);
			lstMostViewing = matchLogic.getMostViewingMatch();
			baseForm.setLstMostViewing(lstMostViewing);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void reset(Object o) throws Exception {
		EntityUtil.reset(o);
		doInsert();
	}

	// PROCESSING SAVE DATA
	public <T> boolean doSave(T obj) throws Exception {
		boolean result = false;
		try {
			if (!validateDoSave())
				return false;
			if (!validateBussiness())
				return false;
			onDoSave();
			result = save(obj, user.getUserId()) != null;
			doSaveCallback();
		} catch (Exception e) {
			result = false;
		}
		notifyResult(result);
		return result;
	}

	protected void onDoSave() throws Exception {
	}

	protected boolean validateDoSave() throws Exception {
		return validateRequired();
	}

	protected boolean validateRequired() throws Exception {
		return true;
	}

	protected boolean validateBussiness() throws Exception {
		return true;
	}

	protected void copyProperties(Object dest, Object src) throws Exception {
		EntityUtil.copyProperties(dest, src);
	}

	protected void setView() {
		setViewState();
		setViewButon();
	}

	protected void setViewButon() {
		if (viewState == AppConstants.VIEW_STATE.INSERT) {
			viewInsert = AppConstants.VISIBLE.SHOW;
			viewUpdate = AppConstants.VISIBLE.HIDE;
		} else if (viewState == AppConstants.VIEW_STATE.UPDATE) {
			viewInsert = AppConstants.VISIBLE.HIDE;
			viewUpdate = AppConstants.VISIBLE.SHOW;
		}
	}

	protected void setViewState() {
	}

	protected <T> T save(T obj, Integer userId) {
		T objectRet = null;
		setViewState();
		if (viewState == AppConstants.VIEW_STATE.INSERT) {
			objectRet = baseLogic.insert(obj, userId);
		} else if (viewState == AppConstants.VIEW_STATE.UPDATE) {
			objectRet = baseLogic.update(obj, userId);
		}
		return objectRet;
	}

	protected void doSaveCallback() {

	}

	protected void notifyResult(boolean result) {
		if (result) {

		} else {

		}
	}
	// END PROCESSING SAVE DATA

	public void doSearch() throws Exception {
		// setView();
		if (!validateDoSearch())
			return;
		viewState = AppConstants.VIEW_STATE.SEARCH;
		onDoSearch();
		processSearch();
	}

	protected boolean validateDoSearch() throws Exception {
		return true;
	}

	protected void onDoSearch() throws Exception {
	}

	protected void processSearch() throws Exception {

	}

	public boolean doDelete() throws Exception {
		setView();
		if (!validateDoDelete())
			return false;
		onDoDelete();
		return delete();
	}

	protected boolean validateDoDelete() {
		return true;
	}

	protected void onDoDelete() {
	}

	protected boolean delete() {
		return true;
	}

	public void doInsert() throws Exception {
		onDoInsert();
		viewState = AppConstants.VIEW_STATE.INSERT;
		setViewButon();
	}

	protected void onDoInsert() throws Exception {
	}

	public void doUpdate() throws Exception {
		onDoUpdate();
		viewState = AppConstants.VIEW_STATE.UPDATE;
		setViewButon();
	}

	protected void onDoUpdate() throws Exception {

	}

	public void doExport() throws Exception {
		setView();
		if (!validateDoExport())
			return;
		onDoExport();
		export();
	}

	protected boolean validateDoExport() {
		return true;
	}

	protected void onDoExport() {

	}

	protected void export() throws Exception {

	}

	protected void setSessionUser(UserDto user) {
		sessionDto.user = user;
	}

}
