package com.staresport.action;

import javax.annotation.Resource;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

import com.staresport.dto.SessionDto;
import com.staresport.form.SessionForm;
import com.staresport.logic.BaseLogic;

public class SessionAction {

	@ActionForm
	@Resource
	protected SessionForm sessionForm;

	@Resource
	protected SessionDto sessionDto;

	@Resource
	private BaseLogic baseLogic;

	// function auto load data common
	public void autoload() {
		
	}

	// ajax load and call back
	@Execute(validator = false)
	public String index() {
		return "index.jsp";
	}

}