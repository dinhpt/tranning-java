package com.staresport.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;
import org.seasar.struts.util.ResponseUtil;

import com.staresport.dto.MatchDto;
import com.staresport.dto.MatchsDto;
import com.staresport.entity.UserHosteds;
import com.staresport.form.MatchForm;
import com.staresport.utils.AppConstants;
import com.staresport.utils.CommonUtil;

import net.arnx.jsonic.JSON;

public class MatchAction extends BaseAction{
	
	private final String MATCHS = "index.jsp";
	
	@ActionForm
	public MatchForm matchForm;
	
	//--------- FUNCTIONS ------
	@Execute(validator = false)
	public String index() {
		initLstGame(matchForm);
		loadGameList();
		return MATCHS;
	}
	
	private void loadGameList() {
		List<MatchsDto> matchsDtos = matchForm.matchsDtos;
		loadMatchsByGame(AppConstants.GOSUGAMER.GAME_TYPE.DOTA, matchsDtos);
		loadMatchsByGame(AppConstants.GOSUGAMER.GAME_TYPE.LOL, matchsDtos);
		loadMatchsByGame(AppConstants.GOSUGAMER.GAME_TYPE.FIFA, matchsDtos);
		loadMatchsByGame(AppConstants.GOSUGAMER.GAME_TYPE.COUNTERSTRIKE, matchsDtos);
		loadMatchsByGame(AppConstants.GOSUGAMER.GAME_TYPE.OVERWATCH, matchsDtos);
	}
	
	private void loadMatchsByGame(int gameType, List<MatchsDto> matchsDtos){
		if(matchsDtos == null) return;
		List<MatchDto> matchDtos = matchLogic.getMatchList(gameType, user.getUserId());
		if(CommonUtil.isEmpty(matchDtos)) return;
		MatchsDto matchsDto = new MatchsDto();
		String name = AppConstants.GOSUGAMER.GAME_TYPE.NAME_GAME_MAP.get(gameType);
		matchsDto.setNameGame(name);
		matchsDto.setMatchDtos(matchDtos);
		matchsDtos.add(matchsDto);
	}
	
	@Execute(validator = false)
	public String doHost() {
		Long matchId = matchForm.getMatchId();
		if(matchId == null) return MATCHS;
		MatchDto match = matchLogic.getInfoMatch(matchId);
		if(match == null) return MATCHS;
		Map<String, Object> outPut = new HashMap<String, Object>();
		outPut.put("gameName", match.getGameName());
		outPut.put("team1", match.getTeam1Name());
		outPut.put("team2", match.getTeam2Name());
		outPut.put("matchName", match.getName());
		outPut.put("matchId", match.getMatchId());
		outPut.put("streams", match.getStreams());
		String json = JSON.encode(outPut);
		ResponseUtil.write(json, "application/json", "UTF-8");
		return null;
	}
	
	@Execute(validator = false)
	public String approveHost() {
		Map<String, Object> outPut = new HashMap<String, Object>();
		String result = AppConstants.HOST.HOST_NOTIFY.SAVE_SUCCESS;
		Long matchId = matchForm.getMatchId();
		String link = matchForm.getLink();
		Integer userId = user.getUserId();
		if(CommonUtil.isEmpty(link)){
			result = AppConstants.HOST.HOST_NOTIFY.EMPTY_LINK;
		}else if(!CommonUtil.isUrl(link)){
			result = AppConstants.HOST.HOST_NOTIFY.NOT_LINK;
		}else if(!validAppHost(matchId)){
			result = AppConstants.HOST.HOST_NOTIFY.HOST_OVER;
		}else if(isUserHosted(userId, matchId)){
			result = AppConstants.HOST.HOST_NOTIFY.USER_HOSTED;
		}else if(!doSaveAppHost(userId, matchId)){
			result = AppConstants.HOST.HOST_NOTIFY.SAVE_FAILED;
		}
		if(result.equalsIgnoreCase(AppConstants.HOST.HOST_NOTIFY.SAVE_SUCCESS)){
			outPut.put("uhostedId", user.getUserId());
		}
		outPut.put("result", result);
		String json = JSON.encode(outPut);
		ResponseUtil.write(json, "application/json", "UTF-8");
		return null;
	}
	
	private boolean doSaveAppHost(Integer userId, Long matchId) {
		// SAVE INTO TABLE USERS_HOSTED
		UserHosteds hosted = new UserHosteds();
		hosted.setMatchId(matchId);
		hosted.setUserId(userId);
		hosted.setStream(matchForm.getLink());
		hosted = baseLogic.insert(hosted, userId);
		return hosted != null ? matchLogic.updateNumHostedOfMatch(matchId) : false;
	}
	
	/**
	 * VALIDATE BUSSINESS APPROVE HOST
	 * @return
	 */
	private boolean validAppHost(Long matchId){
		if(matchId == null) return false;
		return matchLogic.getNumHostedOfMatch(matchId);
	}
	
	private boolean isUserHosted(Integer userId, Long matchId){
		return matchLogic.isUserHosted(userId, matchId);
	}
}
