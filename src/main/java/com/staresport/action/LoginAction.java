package com.staresport.action;

import javax.annotation.Resource;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

import com.staresport.dto.UserDto;
import com.staresport.form.LoginForm;
import com.staresport.logic.UsersLogic;
import com.staresport.utils.CommonUtil;
import com.staresport.utils.PassWordUtil;

public class LoginAction extends BaseAction {
	private static final String LOGIN = "login.jsp";

	@ActionForm
	public LoginForm loginForm;
	@Resource
	private UsersLogic usersLogic;

	@Execute(validator = false)
	public String index() {
		try {
			initLstGame(loginForm);
			return LOGIN;
		} catch (Exception e) {
			return INDEX;
		}
	}

	@Execute(validator = false)
	public String authenticate() {
		String username = loginForm.getUserName();
		String password = loginForm.getPassword();
		UserDto user = null;
		if (!CommonUtil.isEmpty(username) && !CommonUtil.isEmpty(password)) {
			try {
				password = PassWordUtil.getInstance().encrypt(password);
				user = usersLogic.authen(username, password);
			} catch (Exception e) {
				e.getMessage();
			}
		}
		if (user == null) {
			return LOGIN;
		}
		setSessionUser(user);
		return INDEX;
	}
}
