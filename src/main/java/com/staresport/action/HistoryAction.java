package com.staresport.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Resource;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.staresport.bean.UserBetInfo;
import com.staresport.dto.SubMatchsDto;
import com.staresport.dto.UserBetDto;
import com.staresport.form.HistoryForm;
import com.staresport.logic.BetLogic;
import com.staresport.logic.HistoryLogic;
import com.staresport.logic.SubMatchsLogic;
import com.staresport.utils.CommonUtil;

public class HistoryAction extends BaseAction {
	private final String HISTORY = "index.jsp";

	@Resource
	private HistoryLogic historyLogic;

	@Resource
	private BetLogic betLogic;

	@Resource
	private SubMatchsLogic subMatchsLogic;

	@ActionForm
	public HistoryForm historyForm;

	@Execute(validator = false)
	public String index() {
		initLstGame(historyForm);
		List<UserBetDto> lstUserBetDtos = new ArrayList<>();
		if (user != null) {
			Integer userId = user.getUserId();
			lstUserBetDtos = betLogic.getUserBetHistory(userId);
			Gson gson = new Gson();
			if (lstUserBetDtos != null && lstUserBetDtos.size() > 0) {
				for (UserBetDto userBet : lstUserBetDtos) {
					if (userBet.getAllBets() != null && userBet.getAllBets().length() > 0) {
						List<UserBetInfo> list = gson.fromJson(userBet.getAllBets(),
								new TypeToken<List<UserBetInfo>>() {
								}.getType());
						List<UserBetInfo> lstConvert = setSubMatchInfoToBet(list, userBet.getMatchId());
						userBet.setLstUserBetJson(lstConvert);
					}
				}
			}
		} else {
			return INDEX;
		}
		historyForm.setLstUserBetDtos(lstUserBetDtos);
		return HISTORY;
	}

	/**
	 * lay thong tin sub match
	 * 
	 * @param lstUserBetInfo
	 */
	private List<UserBetInfo> setSubMatchInfoToBet(List<UserBetInfo> lstUserBetInfo, Integer matchId) {
		List<UserBetInfo> lstReturn = new ArrayList<>();
		if (lstUserBetInfo != null && lstUserBetInfo.size() > 0) {
			for (UserBetInfo betInfo : lstUserBetInfo) {

				if (betInfo.getBetTeamId() != null) {
					betInfo.setMatchId(String.valueOf(matchId));
					SubMatchsDto subMatchDto = subMatchsLogic.getById(Integer.valueOf(betInfo.getSubMatchId()),
							Integer.valueOf(betInfo.getMatchId()));
					if (subMatchDto != null) {
						betInfo.setWinResult(subMatchDto.getWinResults());
						betInfo.setMatchStatus(subMatchDto.getStatus());
					}
					lstReturn.add(betInfo);
				}
			}
		}
		if (lstReturn != null && lstReturn.size() > 2) {
			Collections.sort(lstReturn, new Comparator<UserBetInfo>() {
				 @Override
			      public int compare(final UserBetInfo object1, final UserBetInfo object2) {
			          return object1.getSubMatchId().compareTo(object2.getSubMatchId());
			      }
			});
		}
		return lstReturn;
	}

	@Execute(validator = false, urlPattern = "{userId}")
	public String history() {
		String userId = (String) request.getParameter("userId");
		if (CommonUtil.isEmpty(userId))
			return INDEX;
		initLstGame(historyForm);
		if (user != null) {

		}

		return HISTORY;
	}
}
