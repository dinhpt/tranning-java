package com.staresport.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.arnx.jsonic.JSON;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;
import org.seasar.struts.util.ResponseUtil;

import com.staresport.bean.UserBetJson;
import com.staresport.dto.BetParamDto;
import com.staresport.dto.MatchDto;
import com.staresport.dto.SubMatchsDto;
import com.staresport.dto.UserBetDto;
import com.staresport.entity.UserBetDetail;
import com.staresport.entity.UserBets;
import com.staresport.form.BetForm;
import com.staresport.logic.BetLogic;
import com.staresport.utils.AppConstants;
import com.staresport.utils.CommonUtil;
import com.staresport.utils.ConverterUtil;
import com.staresport.utils.EntityUtil;
import com.staresport.utils.ResourcesUtil;

public class BetAction extends BaseAction {
	private final String BET = "index.jsp";

	@Resource
	private BetLogic betLogic;

	@ActionForm
	public BetForm betForm;
	
	private boolean success=true;
	private String message;
	// GET INFO USER BET
	private UserBetDto userBet = new UserBetDto();
	// OBJECT TO SAVE
	private UserBets userBets = new UserBets();
	// OBJECT TO SAVE
	private List<UserBetDetail> userBetDetails = new ArrayList<>();
	// FOR AFTER DOSAVE DATA
	private UserBetJson userBetJson = new UserBetJson();
	// INCLUDE PARAMS GET FROM REQUEST
	private BetParamDto paramDto = new BetParamDto();;

	// --------- FUNCTIONS ------
	@Execute(validator = false)
	public String index() {
		initLstGame(betForm);
		return INDEX;
	}

	@Execute(validator = false, urlPattern = "{matchId}/uhid/{uhostedId}")
	public String match() {
		initLstGame(betForm);
		String matchid = (String) request.getParameter("matchId");
		String uhostedid = (String) request.getParameter("uhostedId");
		if (!CommonUtil.isNumber(matchid) || !CommonUtil.isNumber(uhostedid))
			return INDEX;
		Integer uhostedId = Integer.valueOf(uhostedid);
		Integer matchId = Integer.valueOf(matchid);
		if(user==null) return BET;
		MatchDto match = betLogic.getMatchUserBet(matchId, uhostedId, user.getUserId());
		if (match != null) {
			try {
				EntityUtil.copyProperties(betForm, match);
			} catch (Exception e) {
				e.getMessage();
				return INDEX;
			}
		}
		return BET;
	}
	
	@Execute(validator = false, urlPattern = "refresh")
	public String doRefreshData(){
		doRefresh();
		return null;
	}
	
	private void doRefresh(){
		try {
			if(!validateParams())
				return;
			setInfoBet(paramDto);
			refresh();
		} catch (Exception e) {
			e.getMessage();
		}
	}
	
	private void refresh(){
		MatchDto matchDto = betLogic.loadDataBet(userBet, user.getUserId());
		if(matchDto == null) success = false;
		Map<String, Object> outPut = new HashMap<String, Object>();
		outPut.put("code", success ? "success":"failed");
		outPut.put("statistic", matchDto);
		outPut.put("infoBet", matchDto.getBetDetailList());
		String json = JSON.encode(outPut);
		ResponseUtil.write(json, "application/json", "UTF-8");
	}
	
	@Execute(validator = false, urlPattern = "dochangebet")
	public String doChangeBet() throws Exception {
		doSaveChangeBet();
		Map<String, Object> outPut = new HashMap<String, Object>();
		outPut.put("code", success ? "success":"failed");
		outPut.put("message", message);
		outPut.put("totalBet", userBetJson.getBetMoney());
		String json = JSON.encode(outPut);
		ResponseUtil.write(json, "application/json", "UTF-8");
		return null;
	}
	
	private void doSaveChangeBet() {
		try {
			if(!validateParams())
				return;
			if(!validDoSaveChangeBet())
				return;
			//NOT ENOUGH setViewState. IF REMOVE THEN SET VIEW_STATE = UPDATE
			setViewState();
			if(!validBussinessChangeBet())
				return;
			onDoSaveChangeBet();
			if(!save())
				return;
			saveCallBack();
			notifyChangeBet();
		} catch (Exception e) {
			e.getMessage();
		}
	}
	
	private void getParameters(){
		String matchId = (String) request.getParameter("matchId");
		String betTeamId = (String) request.getParameter("teamId");
		String uhostedId = (String) request.getParameter("uhostedId");
		String smatchId = (String) request.getParameter("smatchId");
		String team1Id = (String) request.getParameter("team1Id");
		String team2Id = (String) request.getParameter("team2Id");
		String betMoney = (String) request.getParameter("betMoney");
		setParameters(matchId, betTeamId, uhostedId, smatchId, betMoney, team1Id, team2Id);
	}
	
	private void setParameters(String matchId, String betTeamId, String uhostedId,
			String smatchId, String betMoney, String team1Id, String team2Id){
		paramDto.setMatchId(matchId);
		paramDto.setBetTeamId(betTeamId);
		paramDto.setUhostedId(uhostedId);
		paramDto.setSmatchId(smatchId);
		paramDto.setBetMoney(betMoney);
		paramDto.setTeam1Id(team1Id);
		paramDto.setTeam2Id(team2Id);
		
	} 
	
	private boolean validateParams(){
		getParameters();
		if(!CommonUtil.isNumber(paramDto.getMatchId()) 
				|| !CommonUtil.isNumber(paramDto.getUhostedId())
				|| !CommonUtil.isNumber(paramDto.getTeam1Id())
				|| !CommonUtil.isNumber(paramDto.getTeam2Id())){
			success = false;
			message = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "input.invalid");
			return false;
		}
		return true;
	}
	
	private boolean validDoSaveChangeBet(){
		if(!CommonUtil.isNumber(paramDto.getSmatchId())
				|| !CommonUtil.isNumber(paramDto.getBetTeamId())){
			success = false;
			message = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "input.invalid");
			return false;
		}
		setInfoBet(paramDto);
		return true;
	}
	
	private boolean validBussinessChangeBet(){
		//1. CHECK MATCH VALID OR INVALID
		if (success) {
			isValidMatch(userBet);
		}
		//2. CHECK TIME TO BET MATCH
		if (success) {
			isRemainTimeBet(userBet);
		}
		// CHECK USER WAS BET NOT YET ?
		if(success){
			isUserBet();
		}
		// ALLOW CHANGE BET
		if(success){
			isAllowsChange(userBet.getUserBetJsons(), userBet.getSubMatchId());
		}
		return success;
	}
	
	private boolean isAllowsChange(List<UserBetJson> betJsons, Integer smatchId) {
		success = validAllowsChange(betJsons, smatchId);
		if(success) return true;
		String bestOf = "";
		if(smatchId > 1){
			bestOf = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "bet.each.bestof", String.valueOf((smatchId-1)));
		}
		else if(smatchId == 1){
			bestOf = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "bet.all.bestof");
		}
		message = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "bet.change.lost.turns", bestOf);
		return success;

	}
	
	private boolean validAllowsChange(List<UserBetJson> betJsons, Integer smatchId){
		String subId = String.valueOf(smatchId);
		for(UserBetJson json : betJsons){
			if(subId.equalsIgnoreCase(json.getSubMatchId())){
				int num = Integer.parseInt(json.getNumChange());
				return num < 1;
			}
		}
		return false;
	}
	
	private boolean isUserBet(){
		UserBetDto userBetDto = betLogic.getAllBetUser(user.getUserId(), userBet.getMatchId(), userBet.getUhostedId());
		if(userBetDto == null){
			success = false;
			message = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "error.no.find.data");
			return false;
		}
		userBet.setUserBetJsons(userBetDto.getUserBetJsons());
		userBet.setUbetHistrJsons(userBetDto.getUbetHistrJsons());
		return true;
	}
	
	private void onDoSaveChangeBet() throws CloneNotSupportedException{
		userBets.setUserId(user.getUserId());
		userBets.setMatchId(userBet.getMatchId());
		userBets.setUhostedId(userBet.getUhostedId());
		
		Integer smatchId = userBet.getSubMatchId();
		Integer teamId = userBet.getBetTeamId();
		// COLUM BET_BESTOF
		 List<UserBetJson> userBetJsons = updateChangeBetBestOf(userBet.getUserBetJsons(), smatchId, teamId);
		userBets.setBetBestof(ConverterUtil.toJsonString(userBetJsons));
		userBetDetails = convertToUbetDetail(userBetJsons);
		// COLUM ALL_BET_HISTORIES
		 List<UserBetJson> ubetHistrJsons = updateAbetHistr(userBet.getUbetHistrJsons(), smatchId, teamId);
		userBets.setAllBetHistories(ConverterUtil.toJsonString(ubetHistrJsons));
	}
	
	
	
	private void notifyChangeBet(){
		if(success){
			message = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "bet.change.success");
		}else{
			message = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "bet.change.failed");
		}
		
	}
	
	private List<UserBetJson> updateChangeBetBestOf(List<UserBetJson> betJsons, Integer smatchId, Integer teamId){
		if(CommonUtil.isEmpty(betJsons) || smatchId == null || teamId == null) return null;
		List<UserBetJson> userBetJsons = EntityUtil.deepCloneObjects(betJsons);
		String smatchid = String.valueOf(smatchId);
		String teamid = String.valueOf(teamId);
		for(UserBetJson betJson : userBetJsons){
			String subId = betJson.getSubMatchId();
			int numChange = Integer.parseInt(betJson.getNumChange());
			if(subId.equalsIgnoreCase(smatchid)){
				betJson.setBetTeamId(teamid);
				betJson.setChanged(AppConstants.NUMBER_STRING.ONE);
				betJson.setNumChange(String.valueOf(numChange+1));
			}
		}
		return userBetJsons;
	}
	
	private List<UserBetJson> updateAbetHistr(List<UserBetJson> betJsons, Integer smatchId, Integer teamId) 
			throws CloneNotSupportedException{
		if(CommonUtil.isEmpty(betJsons) || smatchId == null || teamId == null) return null;
		List<UserBetJson> jsons = EntityUtil.deepCloneObjects(betJsons);
		List<UserBetJson> jsonsUpdate = new ArrayList<>();
		UserBetJson json = null;
		String smatchid = String.valueOf(smatchId);
		String teamid = String.valueOf(teamId);
		for(UserBetJson betJson : jsons){
			String subId = betJson.getSubMatchId();
			if(subId.equalsIgnoreCase(smatchid)){
				json = betJson.clone();
				json.setBetTime(getDateNow());
				json.setBetTeamId(teamid);
				json.setChanged(AppConstants.NUMBER_STRING.ONE);
				jsonsUpdate.add(json);
			}
			jsonsUpdate.add(betJson);
		}
		return jsonsUpdate;
	}

	@Execute(validator = false, urlPattern = "dobet")
	public String doBet() throws Exception {
		doSaveBet();
		Map<String, Object> outPut = new HashMap<String, Object>();
		outPut.put("code", success ? "success":"failed");
		outPut.put("message", message);
		outPut.put("totalBet", userBetJson.getBetMoney());
		outPut.put("firstBet", AppConstants.NUMBER_STRING.ONE.equalsIgnoreCase(userBetJson.getNumBet()) ? true:false);
		String json = JSON.encode(outPut);
		ResponseUtil.write(json, "application/json", "UTF-8");
		return null;
	}
	
	private void doSaveBet() {
		try {
			if(!validateParams())
				return;
			if(!validDoSaveBet())
				return;
			setViewState();
			if(!validBussinessBet())
				return;
			onDoSaveBet();
			if(!save())
				return;
			saveCallBack();
			notifyBet();
		} catch (Exception e) {
			e.getMessage();
		}

	}
	
	private void setInfoBet(BetParamDto paramDto){
		userBet.setMatchId(paramDto.getMatchId() != null ? Integer.valueOf(paramDto.getMatchId()):null);
		userBet.setBetTeamId(paramDto.getBetTeamId() != null ? Integer.valueOf(paramDto.getBetTeamId()):null);
		userBet.setUhostedId(paramDto.getUhostedId() != null ? Integer.valueOf(paramDto.getUhostedId()):null);
		userBet.setSubMatchId(paramDto.getSmatchId() != null ? Integer.valueOf(paramDto.getSmatchId()):null);
		userBet.setBetMoney(paramDto.getBetMoney());
		userBet.setUserBetId(user.getUserId());
		userBet.setTeam1Id(paramDto.getTeam1Id() != null ? Integer.valueOf(paramDto.getTeam1Id()):null);
		userBet.setTeam2Id(paramDto.getTeam2Id() != null ? Integer.valueOf(paramDto.getTeam2Id()):null);
	}
	
	//-------------------------- VALIDATE BET ------------------------------
	private void isValidMatch(UserBetDto userBet) {
		Integer matchId = userBet.getMatchId();
		Integer subMatchId = userBet.getSubMatchId();
		success = betLogic.validMatch(matchId, subMatchId);
		if(!success) message = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "bet.match.invalid");
		
	}
	
	private void isRemainTimeBet(UserBetDto userBet) {
		success = betLogic.isRemainTimeBet(userBet.getMatchId());
		if(!success) message = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "bet.match.endtime");
		
	}
	
	private boolean validBetMore(List<UserBetJson> infoUserBets, Integer smatchId){
		if(CommonUtil.isEmpty(infoUserBets) || smatchId == null) return false;
		try {
			for(UserBetJson json : infoUserBets){
				String subMatchId = json.getSubMatchId();
				if(String.valueOf(smatchId).equals(subMatchId)){
					return Integer.parseInt(json.getNumBet()) < 3;
				}
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return false;
	}
	
	private void isAllowsBetMore(List<UserBetJson> infoUserBets, Integer smatchId){
		success = validBetMore(infoUserBets, smatchId);
		if(success) return;
		String bestOf = "";
		if(smatchId > 1){
			bestOf = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "bet.each.bestof", String.valueOf((smatchId-1)));
		}
		else if(smatchId == 1){
			bestOf = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "bet.all.bestof");
		}
		message = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "bet.match.not.bet.more", bestOf);
	}
	
	private boolean validDoSaveBet() throws Exception {
		if(!CommonUtil.isNumber(paramDto.getSmatchId()) 
				|| !CommonUtil.isNumber(paramDto.getBetTeamId()) ){
			success = false;
			message = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "input.invalid");
			return false;
		}
		String betMoney = paramDto.getBetMoney();
		if(CommonUtil.isEmpty(betMoney)){
			success = false;
			message = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "money.bet.empty");
			return false;
		}
		if(!CommonUtil.isNumber(betMoney)){
			success = false;
			message = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "money.bet.not.number");
			return false;
		}
		setInfoBet(paramDto);
		return true;
	}
	
	private boolean validBussinessBet() throws Exception{
		if(!success) return false;
		
		//1. CHECK MATCH VALID OR INVALID
		if (success) {
			isValidMatch(userBet);
		}
		//2. CHECK TIME TO BET MATCH
		if (success) {
			isRemainTimeBet(userBet);
		}
		//3. CHECK QUANTITY STAR OF USER
		
		if(success){
			hasDataBet();
		}
		
		//4. CHECK ALLOWS BET MORE
		if(success && viewState == AppConstants.VIEW_STATE.UPDATE){
			isAllowsBetMore(userBet.getUserBetJsons(), userBet.getSubMatchId());
		}
		return success;
	}
	
	private boolean hasDataBet(){
		if(viewState == AppConstants.VIEW_STATE.INSERT){
			List<UserBetJson> betJsons = getInfoBetFirst(userBet);
			if(CommonUtil.isEmpty(betJsons)){
				success = false;
				message = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "error.no.find.data");
				return false;
			}
			userBet.setUserBetJsons(betJsons);
			userBet.setUbetHistrJsons(betJsons);
		}else if(viewState == AppConstants.VIEW_STATE.UPDATE){
			UserBetDto userBetDto = betLogic.getAllBetUser(user.getUserId(), userBet.getMatchId(), userBet.getUhostedId());
			if(userBetDto == null){
				success = false;
				message = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "error.no.find.data");
				return false;
			}
			userBet.setUserBetJsons(userBetDto.getUserBetJsons());
			userBet.setUbetHistrJsons(userBetDto.getUbetHistrJsons());
		}
		return true;
	}
	//-------------------------- END VALIDATE BET ------------------------------
	
	
	private void onDoSaveBet() throws CloneNotSupportedException {
		if(!success) return;
		List<UserBetJson> betJsons = userBet.getUserBetJsons();
		List<UserBetJson> betHstrJsons = userBet.getUbetHistrJsons();
		if(viewState == AppConstants.VIEW_STATE.UPDATE){
			betJsons = updateBetBestof(betJsons, userBet);
			betHstrJsons = updateBetHstr(betHstrJsons, userBet);
		}
		userBet.setUserBetJsons(betJsons);
		userBets.setUserId(user.getUserId());
		userBets.setMatchId(userBet.getMatchId());
		userBets.setUhostedId(userBet.getUhostedId());
		String betBestOf = ConverterUtil.toJsonString(betJsons);
		userBets.setBetBestof(betBestOf);
		userBets.setAllBetHistories(ConverterUtil.toJsonString(betHstrJsons));
		// PUT DATA INTO user bet detail
		userBetDetails = convertToUbetDetail(betJsons);
	}
	
	private boolean save(){
		if(!success) return false;
		if(success){
			if(viewState == AppConstants.VIEW_STATE.INSERT){
				success = betLogic.insert(userBets, userBetDetails);
			}else if(viewState == AppConstants.VIEW_STATE.UPDATE){
				success = betLogic.update(userBets, userBetDetails);
			}
			
		}
		return success;
	}
	
	private void saveCallBack() {
		List<UserBetJson> groups =  userBet.getUserBetJsons();
		Integer subId = userBet.getSubMatchId();
		getInfoBoBet(userBetJson, groups, subId);
		//BetDto betDto = betLogic.statisticUserBet(userBet, user.getUserId());
		
	}
	
	private void notifyBet(){
		if(success){
			message = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "bet.match.success");
		}else{
			message = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "bet.match.failed");
		}
	}
	
	/**
	 * GET INFO BEST OF WAS BET
	 * @param userBetJson
	 * @param groupUbetJsons
	 * @param subMatchId
	 */
	private void getInfoBoBet(UserBetJson userBetJson, List<UserBetJson> groupUbetJsons, Integer subMatchId){
		if(CommonUtil.isEmpty(groupUbetJsons) || subMatchId == null) return;
		String subId = String.valueOf(subMatchId);
		for(UserBetJson json : groupUbetJsons){
			if(json.getSubMatchId().equals(subId)){ 
				userBetJson.setNumBet(json.getNumBet());
				userBetJson.setBetMoney(json.getBetMoney());
			}
		}
	}
	
	/**
	 * GET DATA FOR THE FIRST BET
	 * @param userBet
	 * @return
	 */
	private List<UserBetJson> getInfoBetFirst(UserBetDto userBet){
		List<UserBetJson> infoUserBets = new ArrayList<>();
		Integer matchId = userBet.getMatchId();
		List<SubMatchsDto> matchsDtos = betLogic.getSubMatch(matchId);
		if(CommonUtil.isEmpty(matchsDtos)){
			success = false;
			message = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "match.not.existed");
			return null;
		}
		UserBetJson betJson = null;
		if(!CommonUtil.isEmpty(matchsDtos)){
			for(SubMatchsDto dto : matchsDtos){
				betJson = new UserBetJson();
				Integer subId = dto.getSubMatchId();
				betJson.setBetMoney(AppConstants.NUMBER_STRING.ZERO);
				betJson.setBetType(String.valueOf(dto.getBetType()));
				betJson.setNumBet(AppConstants.NUMBER_STRING.ZERO);
				betJson.setNoBet(AppConstants.NUMBER_STRING.ZERO);
				betJson.setSubMatchId(String.valueOf(subId));
				betJson.setUserId(String.valueOf(user.getUserId()));
				betJson.setStatus(AppConstants.NUMBER_STRING.ZERO);
				if(userBet.getSubMatchId().equals(subId)){
					betJson.setBetMoney(userBet.getBetMoney());
					betJson.setBetTeamId(String.valueOf(userBet.getBetTeamId()));
					betJson.setNumBet(AppConstants.NUMBER_STRING.ONE);
					betJson.setBetTime(getDateNow());
					betJson.setNoBet(AppConstants.NUMBER_STRING.ONE);
				}
				betJson.setNumChange(AppConstants.NUMBER_STRING.ZERO);
				betJson.setNoMatch(String.valueOf(dto.getNoMatch()));
				betJson.setMatchId(String.valueOf(matchId));
				infoUserBets.add(betJson);
			}
		}
		if(CommonUtil.isEmpty(infoUserBets)){
			success = false;
			message = ResourcesUtil.getResource(ResourcesUtil.MASSAGE, "bestof.not.existed");
			return null;
		}
		return infoUserBets;
	}
	
	private List<UserBetJson> updateBetBestof(List<UserBetJson> userBetJsons, UserBetDto userBet){
		if(CommonUtil.isEmpty(userBetJsons) || userBet == null) return null;
		String smatchId = String.valueOf(userBet.getSubMatchId());
		List<UserBetJson> betJsons = EntityUtil.deepCloneObjects(userBetJsons);
		for(UserBetJson json : betJsons){
			String subId = json.getSubMatchId();
			Integer money = Integer.valueOf(json.getBetMoney());
			if(smatchId.equals(subId)){
				money += Integer.valueOf(userBet.getBetMoney());
				int numbet = Integer.parseInt(json.getNumBet());
				json.setBetMoney(String.valueOf(money));
				json.setNumBet(String.valueOf(numbet+1));
				json.setBetTeamId(String.valueOf(userBet.getBetTeamId()));
				json.setBetTime(getDateNow());
				break;
			}
		}
		return betJsons;
	}
	
	/**
	 * UPDATE INFO USER BET HISTORY
	 * @param userBetJsons
	 * @param userBet
	 * @throws CloneNotSupportedException 
	 */
	private List<UserBetJson> updateBetHstr(List<UserBetJson> userBetJsons, UserBetDto userBet) throws CloneNotSupportedException{
		if(CommonUtil.isEmpty(userBetJsons) || userBet == null) return null;
		String smatchId = String.valueOf(userBet.getSubMatchId());
		String betMoney = userBet.getBetMoney();
		// INCREMENT A RECORD INTO JSONARRAY
		UserBetJson userBetJson = new UserBetJson();
		List<UserBetJson> betJsons = new ArrayList<>();
		int loop = 0;
		int noBetEnd = 0;
		boolean isFirst=false;
		for(UserBetJson betJson : userBetJsons){
			String subId = betJson.getSubMatchId();
			int numBet = Integer.parseInt(betJson.getNumBet());
			if(smatchId.equalsIgnoreCase(subId)){
				betJson.setNumBet(String.valueOf(numBet+1));
				// BET THE FIRST
				if(numBet == 0){
					betJson.setBetTime(getDateNow());
					betJson.setBetTeamId(String.valueOf(userBet.getBetTeamId()));
					betJson.setBetMoney(betMoney);
					betJson.setNoBet(AppConstants.NUMBER_STRING.ONE);
					betJson.setNumChange(AppConstants.NUMBER_STRING.ZERO);
					isFirst = true;
				}else{
					int noBet = Integer.parseInt(betJson.getNoBet());
					noBetEnd = (noBetEnd < noBet) ? noBet:noBetEnd;
					if(loop == 0){
						userBetJson = betJson.clone();
						userBetJson.setUserId(String.valueOf(user.getUserId()));
						userBetJson.setBetMoney(betMoney);
						userBetJson.setBetTime(getDateNow());
						userBetJson.setNumBet(String.valueOf(numBet+1));
						userBetJson.setStatus(AppConstants.NUMBER_STRING.ZERO);
					}
				}
				loop++;
			}
			betJsons.add(betJson);
		}
		if(!isFirst){
			userBetJson.setNoBet(String.valueOf(++noBetEnd));
			betJsons.add(userBetJson);
		}
		return betJsons;
	}
	
	@Override
	protected void setViewState() {
		Integer matchId = betForm.getMatchId();
		Integer uhostedId = betForm.getUhostedId();
		Integer userId = user.getUserId();
		boolean isbeted = betLogic.isUserBeted(matchId, uhostedId, userId);
		if(isbeted){
			viewState = AppConstants.VIEW_STATE.UPDATE;
		}else{
			viewState = AppConstants.VIEW_STATE.INSERT;
		}
	}
	
	private String getDateNow(){
		return ConverterUtil.toString(new Date(), AppConstants.DATE_FORMAT.SLASH_FORMAT.YYYYMMDD_TIME);
	}
	
	private List<UserBetDetail> convertToUbetDetail(List<UserBetJson> betJsons){
		if(CommonUtil.isEmpty(betJsons)) return null;
		List<UserBetDetail> details = new ArrayList<>(); 
		try {
			for(UserBetJson json : betJsons){
				UserBetDetail detail = new UserBetDetail();
				detail.setUserId(Integer.valueOf(user.getUserId()));
				detail.setMatchId(Integer.valueOf(userBet.getMatchId()));
				detail.setUhostedId(Integer.valueOf(userBet.getUhostedId()));
				detail.setBetTime(ConverterUtil.toDate(json.getBetTime(), AppConstants.DATE_FORMAT.SLASH_FORMAT.YYYYMMDD_TIME));
				detail.setSubMatchId(Integer.valueOf(json.getSubMatchId()));
				detail.setBetTeamId(!CommonUtil.isEmpty(json.getBetTeamId()) ? Integer.parseInt(json.getBetTeamId()) : null);
				detail.setBetMoney(Double.parseDouble(json.getBetMoney()));
				detail.setNumBet(Integer.valueOf(json.getNumBet()));
				detail.setNumChange(Integer.valueOf(json.getNumChange()));
				detail.setNoMatch(Integer.valueOf(json.getNoMatch()));
				details.add(detail);
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return details;
	}
}
