/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.staresport.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author NamLX
 */
@Entity
@Table(name = "bet_configs")
public class BetConfigs implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	@Column(name = "bet_config_id")
	private Integer betConfigId;

	@Column(name = "times_per_match")
	private int timesPerMatch;

	@Column(name = "max_money_bet")
	private float maxMoneyBet;

	public BetConfigs() {
	}

	public Integer getBetConfigId() {
		return betConfigId;
	}

	public void setBetConfigId(Integer betConfigId) {
		this.betConfigId = betConfigId;
	}

	public int getTimesPerMatch() {
		return timesPerMatch;
	}

	public void setTimesPerMatch(int timesPerMatch) {
		this.timesPerMatch = timesPerMatch;
	}

	public float getMaxMoneyBet() {
		return maxMoneyBet;
	}

	public void setMaxMoneyBet(float maxMoneyBet) {
		this.maxMoneyBet = maxMoneyBet;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof BetConfigs)) {
			return false;
		}
		BetConfigs other = (BetConfigs) object;
		if ((this.betConfigId == null && other.betConfigId != null) || (this.betConfigId != null && !this.betConfigId.equals(other.betConfigId))) {
			return false;
		}
		return true;
	}

}
