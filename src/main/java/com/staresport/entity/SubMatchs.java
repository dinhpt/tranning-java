/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.staresport.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author NamLX
 */
@Entity
@Table(name = "SUB_MATCHS")
public class SubMatchs extends BaseEntity<SubMatchs> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sub_match_id")
	private Integer subMatchId;
	@Column(name = "match_id")
	private Integer matchId;
	@Column(name = "score1_team")
	private Float score1Team;
	@Column(name = "win_results")
	private Integer winResults;
	@Column(name = "score2_team")
	private Float score2Team;
	@Column(name = "total_user_bet")
	private Integer totalUserBet;
	@Column(name = "total_fee")
	private Float totalFee;
	@Column(name = "total_money_bet")
	private Float totalMoneyBet;
	@Column(name = "team1_bet_per")
	private Integer team1BetPer;
	@Column(name = "team2_bet_per")
	private Integer team2BetPer;
	@Column(name = "status")
	private Integer status;
	@Column(name = "bet_type")
	private Integer betType;
	@Column(name = "no_match")
	private Integer noMatch;

	public SubMatchs() {
	}

	public Integer getSubMatchId() {
		return subMatchId;
	}

	public void setSubMatchId(Integer subMatchId) {
		this.subMatchId = subMatchId;
	}

	public Integer getMatchId() {
		return matchId;
	}

	public void setMatchId(Integer matchId) {
		this.matchId = matchId;
	}

	public Float getScore1Team() {
		return score1Team;
	}

	public void setScore1Team(Float score1Team) {
		this.score1Team = score1Team;
	}

	public Float getScore2Team() {
		return score2Team;
	}

	public void setScore2Team(Float score2Team) {
		this.score2Team = score2Team;
	}

	public Integer getTotalUserBet() {
		return totalUserBet;
	}

	public void setTotalUserBet(Integer totalUserBet) {
		this.totalUserBet = totalUserBet;
	}

	public Float getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(Float totalFee) {
		this.totalFee = totalFee;
	}

	public Float getTotalMoneyBet() {
		return totalMoneyBet;
	}

	public void setTotalMoneyBet(Float totalMoneyBet) {
		this.totalMoneyBet = totalMoneyBet;
	}

	public Integer getTeam1BetPer() {
		return team1BetPer;
	}

	public void setTeam1BetPer(Integer team1BetPer) {
		this.team1BetPer = team1BetPer;
	}

	public Integer getTeam2BetPer() {
		return team2BetPer;
	}

	public void setTeam2BetPer(Integer team2BetPer) {
		this.team2BetPer = team2BetPer;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getNoMatch() {
		return noMatch;
	}

	public void setNoMatch(Integer noMatch) {
		this.noMatch = noMatch;
	}

	public Integer getBetType() {
		return betType;
	}

	public void setBetType(Integer betType) {
		this.betType = betType;
	}

	public Integer getWinResults() {
		return winResults;
	}

	public void setWinResults(Integer winResults) {
		this.winResults = winResults;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (matchId != null ? matchId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof SubMatchs)) {
			return false;
		}
		SubMatchs other = (SubMatchs) object;
		if ((this.subMatchId == null && other.subMatchId != null)
				|| (this.subMatchId != null && !this.subMatchId.equals(other.subMatchId))) {
			return false;
		}
		return true;
	}

}
