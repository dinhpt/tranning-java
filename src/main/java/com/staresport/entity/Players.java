/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.staresport.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author NamLX
 */
@Entity
@Table(name = "PLAYERS")
public class Players implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "player_id")
	private Integer playerId;
	@Column(name = "name")
	private String name;
	@Column(name = "team_id")
	private Integer teamId;
	@Column(name = "position")
	private String position;
	@Column(name = "winrate")
	private Float winrate;
	@Column(name = "avatar")
	private String avatar;

	public Integer getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Integer playerId) {
		this.playerId = playerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Float getWinrate() {
		return winrate;
	}

	public void setWinrate(Float winrate) {
		this.winrate = winrate;
	}
	
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Players)) {
			return false;
		}
		Players other = (Players) object;
		if ((this.playerId == null && other.playerId != null)
				|| (this.playerId != null && !this.playerId.equals(other.playerId))) {
			return false;
		}
		return true;
	}
}
