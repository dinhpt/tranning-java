/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.staresport.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author NamLX
 */
@Entity
@Table(name = "match_types")
@NamedQueries({ @NamedQuery(name = "MatchTypes.findAll", query = "SELECT m FROM MatchTypes m") })
public class MatchTypes implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	@Column(name = "mtype_id")
	private Integer mtypeId;

	@Column(name = "name")
	private String name;
	@Column(name = "gtype_id")
	private Integer gtypeId;

	public MatchTypes() {
	}

	public MatchTypes(Integer mtypeId) {
		this.mtypeId = mtypeId;
	}

	public MatchTypes(Integer mtypeId, String name) {
		this.mtypeId = mtypeId;
		this.name = name;
	}

	public Integer getMtypeId() {
		return mtypeId;
	}

	public void setMtypeId(Integer mtypeId) {
		this.mtypeId = mtypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getGtypeId() {
		return gtypeId;
	}

	public void setGtypeId(Integer gtypeId) {
		this.gtypeId = gtypeId;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (mtypeId != null ? mtypeId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof MatchTypes)) {
			return false;
		}
		MatchTypes other = (MatchTypes) object;
		if ((this.mtypeId == null && other.mtypeId != null)
				|| (this.mtypeId != null && !this.mtypeId.equals(other.mtypeId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.staresport.entity.MatchTypes[ mtypeId=" + mtypeId + " ]";
	}

}
