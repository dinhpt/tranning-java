/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.staresport.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author NamLX
 */
@Entity
@Table(name = "game_types")
public class GameTypes implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "game_type_id")
	private Integer gameTypeId;
	@Column(name = "name")
	private String name;
	@Column(name = "short_name")
	private String shortName;
	@Column(name = "img_url")
	private String imgUrl;

	public Integer getGameTypeId() {
		return gameTypeId;
	}

	public void setGameTypeId(Integer gameTypeId) {
		this.gameTypeId = gameTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof GameTypes)) {
			return false;
		}
		GameTypes other = (GameTypes) object;
		if ((this.gameTypeId == null && other.gameTypeId != null)
				|| (this.gameTypeId != null && !this.gameTypeId.equals(other.gameTypeId))) {
			return false;
		}
		return true;
	}

}
