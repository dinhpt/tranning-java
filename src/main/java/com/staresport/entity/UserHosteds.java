/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.staresport.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author NamLX
 */
@Entity
@Table(name = "user_hosteds")
public class UserHosteds implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "user_hosted_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userHostedId;
	@Column(name = "user_id")
	private Integer userId;
	@Column(name = "match_id")
	private Long matchId;
	@Column(name = "stream")
	private String stream;
	@Column(name = "code")
	private String code;
	@Column(name = "total_user_bet")
	private Integer totalUserBet;
	@Column(name = "total_fee")
	private Float totalFee;
	@Column(name = "total_money_bet")
	private Float totalMoneyBet;
	@Column(name = "time_hosted")
	private Integer timeHosted;
	
	public Long getUserHostedId() {
		return userHostedId;
	}
	public void setUserHostedId(Long userHostedId) {
		this.userHostedId = userHostedId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Long getMatchId() {
		return matchId;
	}
	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}
	public String getStream() {
		return stream;
	}
	public void setStream(String stream) {
		this.stream = stream;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Integer getTotalUserBet() {
		return totalUserBet;
	}
	public void setTotalUserBet(Integer totalUserBet) {
		this.totalUserBet = totalUserBet;
	}
	public Float getTotalFee() {
		return totalFee;
	}
	public void setTotalFee(Float totalFee) {
		this.totalFee = totalFee;
	}
	public Float getTotalMoneyBet() {
		return totalMoneyBet;
	}
	public void setTotalMoneyBet(Float totalMoneyBet) {
		this.totalMoneyBet = totalMoneyBet;
	}
	public Integer getTimeHosted() {
		return timeHosted;
	}
	public void setTimeHosted(Integer timeHosted) {
		this.timeHosted = timeHosted;
	}
}
