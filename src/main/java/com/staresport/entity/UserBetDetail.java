package com.staresport.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "USER_BET_DETAIL")
public class UserBetDetail implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "user_id")
	private Integer userId;
	@Id
	@Column(name = "match_id")
	private Integer matchId;
	@Id
	@Column(name = "uhosted_id")
	private Integer uhostedId;
	@Id
	@Column(name = "sub_match_id")
	private Integer subMatchId;
	@Column(name = "bet_team_id")
	private Integer betTeamId;
	@Column(name = "bet_time", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date betTime;
	@Column(name = "bet_money")
	private Double betMoney;
	@Column(name = "profit")
	private Double profit;
	@Column(name = "num_bet")
	private Integer numBet;
	@Column(name = "num_change")
	private Integer numChange;
	@Column(name = "no_match")
	private Integer noMatch;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getMatchId() {
		return matchId;
	}
	public void setMatchId(Integer matchId) {
		this.matchId = matchId;
	}
	public Integer getUhostedId() {
		return uhostedId;
	}
	public void setUhostedId(Integer uhostedId) {
		this.uhostedId = uhostedId;
	}
	public Integer getSubMatchId() {
		return subMatchId;
	}
	public void setSubMatchId(Integer subMatchId) {
		this.subMatchId = subMatchId;
	}
	public Integer getBetTeamId() {
		return betTeamId;
	}
	public void setBetTeamId(Integer betTeamId) {
		this.betTeamId = betTeamId;
	}
	public Date getBetTime() {
		return betTime;
	}
	public void setBetTime(Date betTime) {
		this.betTime = betTime;
	}
	public Double getBetMoney() {
		return betMoney;
	}
	public void setBetMoney(Double betMoney) {
		this.betMoney = betMoney;
	}
	public Double getProfit() {
		return profit;
	}
	public void setProfit(Double profit) {
		this.profit = profit;
	}
	public Integer getNumBet() {
		return numBet;
	}
	public void setNumBet(Integer numBet) {
		this.numBet = numBet;
	}
	public Integer getNumChange() {
		return numChange;
	}
	public void setNumChange(Integer numChange) {
		this.numChange = numChange;
	}
	public Integer getNoMatch() {
		return noMatch;
	}
	public void setNoMatch(Integer noMatch) {
		this.noMatch = noMatch;
	}
	
	
	
}
