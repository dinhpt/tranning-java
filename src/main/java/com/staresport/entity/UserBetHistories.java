/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.staresport.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author NamLX
 */
@Entity
@Table(name = "user_bet_histories")
public class UserBetHistories implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_bet_history_id")
	private Integer userBetHistoryId;
	@Column(name = "user_id")
	private Integer userId;
	@Column(name = "match_id")
	private Integer matchId;
	@Column(name = "result")
	private short result;
	@Column(name = "profit")
	private Float profit;
	@Column(name = "first_bet_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date firstBetTime;
	@Column(name = "last_bet_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastBetTime;
	@Column(name = "all_bets")
	private String allBets;
	
	public Integer getUserBetHistoryId() {
		return userBetHistoryId;
	}
	public void setUserBetHistoryId(Integer userBetHistoryId) {
		this.userBetHistoryId = userBetHistoryId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getMatchId() {
		return matchId;
	}
	public void setMatchId(Integer matchId) {
		this.matchId = matchId;
	}
	public short getResult() {
		return result;
	}
	public void setResult(short result) {
		this.result = result;
	}
	public Float getProfit() {
		return profit;
	}
	public void setProfit(Float profit) {
		this.profit = profit;
	}
	public Date getFirstBetTime() {
		return firstBetTime;
	}
	public void setFirstBetTime(Date firstBetTime) {
		this.firstBetTime = firstBetTime;
	}
	public Date getLastBetTime() {
		return lastBetTime;
	}
	public void setLastBetTime(Date lastBetTime) {
		this.lastBetTime = lastBetTime;
	}
	public String getAllBets() {
		return allBets;
	}
	public void setAllBets(String allBets) {
		this.allBets = allBets;
	}
}
