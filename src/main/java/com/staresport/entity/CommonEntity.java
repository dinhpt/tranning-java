package com.staresport.entity;

import java.util.Date;

/**
 * 
 * @author ChuQuangVi
 *
 */
public interface CommonEntity {
	public Date getCreatedAt();
	
	public void setCreatedAt(Date createdAt);
	
	public Date getUpdateAt();
	
	public void setUpdateAt(Date updateAt);
	
	public Integer getCreatedBy();
	
	public void setCreatedBy(Integer createdBy);
	
	public Integer getUpdatedBy();
	
	public void setUpdatedBy(Integer updatedBy);
	
}
