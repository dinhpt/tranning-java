/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.staresport.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author NamLX
 */
@Entity
@Table(name = "teams")
public class Teams implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "team_id")
	private Integer teamId;
	@Column(name = "name")
	private String name;
	@Column(name = "ranked")
	private Integer ranked;
	@Column(name = "avatar")
	private String avatar;
	@Column(name = "flag")
	private String flag;
	@Column(name = "link")
	private String link;
	@Column(name = "game_id")
	private Integer gameId;
	@Column(name = "winrate")
	private Float winrate;

	public Teams() {
	}

	public Teams(Integer teamId) {
		this.teamId = teamId;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getRanked() {
		return ranked;
	}

	public void setRanked(Integer ranked) {
		this.ranked = ranked;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Integer getGameId() {
		return gameId;
	}

	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}

	public Float getWinrate() {
		return winrate;
	}

	public void setWinrate(Float winrate) {
		this.winrate = winrate;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (teamId != null ? teamId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Teams)) {
			return false;
		}
		Teams other = (Teams) object;
		if ((this.teamId == null && other.teamId != null)
				|| (this.teamId != null && !this.teamId.equals(other.teamId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.staresport.entity.Teams[ teamId=" + teamId + " ]";
	}

}
