/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.staresport.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author NamLX
 */
@Entity
@Table(name = "MATCHS")
public class Matchs implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "match_id")
	private Long matchId;
	@Column(name = "name")
	private String name;
	@Column(name = "team1_id")
	private Long team1Id;
	@Column(name = "team2_id")
	private Long team2Id;
	@Column(name = "date_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateTime;
	@Column(name = "score1_team")
	private Float score1Team;
	@Column(name = "score2_team")
	private Float score2Team;
	@Column(name = "total_user_bet")
	private Integer totalUserBet;
	@Column(name = "total_fee")
	private Float totalFee;
	@Column(name = "total_money_bet")
	private Float totalMoneyBet;
	@Column(name = "link")
	private String link;
	@Column(name = "total_user_show")
	private Integer totalUserShow;
	@Column(name = "team1_bet_per")
	private Integer team1BetPer;
	@Column(name = "team2_bet_per")
	private Integer team2BetPer;
	@Column(name = "win_results")
	private Integer winResults;
	@Column(name = "status")
	private Integer status;
	@Column(name = "best_of")
	private Integer bestOf;
	@Column(name = "game_type")
	private Integer gameType;
	@Column(name = "match_type")
	private Integer matchType;
	@Column(name = "tournament")
	private String tournament;
	@Column(name = "num_hosted")
	private Integer numHosted;
	@Column(name = "created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;
	@Column(name = "update_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateAt;
	@Column(name = "created_by")
	private Integer createdBy;
	@Column(name = "updated_by")
	private Integer updatedBy;

	public Matchs() {
	}

	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getTeam1Id() {
		return team1Id;
	}

	public void setTeam1Id(Long team1Id) {
		this.team1Id = team1Id;
	}

	public Long getTeam2Id() {
		return team2Id;
	}

	public void setTeam2Id(Long team2Id) {
		this.team2Id = team2Id;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public Float getScore1Team() {
		return score1Team;
	}

	public void setScore1Team(Float score1Team) {
		this.score1Team = score1Team;
	}

	public Float getScore2Team() {
		return score2Team;
	}

	public void setScore2Team(Float score2Team) {
		this.score2Team = score2Team;
	}

	public Integer getTotalUserBet() {
		return totalUserBet;
	}

	public void setTotalUserBet(Integer totalUserBet) {
		this.totalUserBet = totalUserBet;
	}

	public Float getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(Float totalFee) {
		this.totalFee = totalFee;
	}

	public Float getTotalMoneyBet() {
		return totalMoneyBet;
	}

	public void setTotalMoneyBet(Float totalMoneyBet) {
		this.totalMoneyBet = totalMoneyBet;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Integer getTotalUserShow() {
		return totalUserShow;
	}

	public void setTotalUserShow(Integer totalUserShow) {
		this.totalUserShow = totalUserShow;
	}

	public Integer getTeam1BetPer() {
		return team1BetPer;
	}

	public void setTeam1BetPer(Integer team1BetPer) {
		this.team1BetPer = team1BetPer;
	}

	public Integer getTeam2BetPer() {
		return team2BetPer;
	}

	public void setTeam2BetPer(Integer team2BetPer) {
		this.team2BetPer = team2BetPer;
	}

	public Integer getWinResults() {
		return winResults;
	}

	public void setWinResults(Integer winResults) {
		this.winResults = winResults;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getBestOf() {
		return bestOf;
	}

	public void setBestOf(Integer bestOf) {
		this.bestOf = bestOf;
	}

	public Integer getGameType() {
		return gameType;
	}

	public void setGameType(Integer gameType) {
		this.gameType = gameType;
	}

	public Integer getMatchType() {
		return matchType;
	}

	public void setMatchType(Integer matchType) {
		this.matchType = matchType;
	}

	public String getTournament() {
		return tournament;
	}

	public void setTournament(String tournament) {
		this.tournament = tournament;
	}

	public Integer getNumHosted() {
		return numHosted;
	}

	public void setNumHosted(Integer numHosted) {
		this.numHosted = numHosted;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (matchId != null ? matchId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Matchs)) {
			return false;
		}
		Matchs other = (Matchs) object;
		if ((this.matchId == null && other.matchId != null)
				|| (this.matchId != null && !this.matchId.equals(other.matchId))) {
			return false;
		}
		return true;
	}

}
